"""
Module used to generate the tables and figures related to the descriptors' analysis.
"""

import os
from functools import partial
from typing import Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
from metrics.common import friedman_test, nemenyi_multitest
from metrics.descriptive import normalize_ranks
from skimage.io import imread
from utils.general_utils import available
from utils.helpers_utils import generate_dir_if_missing
from viz.colormaps import generate_cmap, generate_statistical_test_color_palette
from viz.wsi import (
    draw_boxplot,
    draw_heatmap,
    draw_matching_landmarks,
    generate_standalone_figure,
)

from benchmark.analysis_utils import (
    compute_dstrb_descriptive_statistics,
    create_parser,
    extract_exp_detr_desc_step_execution_time,
    extract_metric,
    get_experiments_from_pair,
    save_statistical_tests,
    sort_dict,
)
from benchmark.benchmark_utils import (
    dict_to_pattern,
    generate_exp_list_per_method,
    get_all_experiments_list,
    get_all_labels_dict,
    match_pattern,
    un_negate_methods,
)
from benchmark.extend_descriptors import (
    descriptors_to_patterns,
    extend_descriptor_exp_supv_outputs,
    extract_coord_desc,
)

DESCRIPTORS = {}


def _extract_exp_normalized_ranks(exp_path: str) -> np.ndarray:
    """Extract the landmarks normalized matching ranks for a given experiment.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :return: The landmarks normalized matching ranks for the given experiment.
    :rtype: np.ndarray
    """

    ranks_supv_path = os.path.join(exp_path, "supv/ranks_lndmrks_desc.csv")
    df = pd.read_csv(ranks_supv_path, index_col=0)

    return normalize_ranks(np.array(df["dst_lndmrks_matching_rank"]))


def _extract_coords_ranks_lndmrks(exp_path: str, file: str = "supv/desc_src.csv") -> Tuple[np.ndarray, np.ndarray]:
    """Extract the coordinates of a given set of landmarks along with their normalized matching ranks.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :param file: The set of landmarks (either from src or dst), defaults to "supv/desc_src.csv".
    :type file: str, optional
    :return: The coordinates of the landmarks and their normalized matching ranks.
    :rtype: Tuple[np.ndarray, np.ndarray]
    """

    coords, _ = extract_coord_desc(pd.read_csv(os.path.join(exp_path, file)))
    mask = np.load(os.path.join(exp_path, "supv/lndmrks_mask.npy"))
    ranks = _extract_exp_normalized_ranks(exp_path)

    return coords[mask, :], ranks


@available(DESCRIPTORS, newname="descriptors")
def generate_descriptors_analysis(
    exps: List[str],
    methods: Dict[str, List[str]],
    outpath: str,
    analysis_dirname: str = "descriptors",
    standard_results: bool = True,
    representative_example: bool = True,
    exec_times: bool = True,
) -> None:
    """Generate the tables and figures for the descriptors analysis.

    :param exps: The list of experiments to analyse.
    :type exps: List[str]
    :param methods: The collection of methods for each step that this analysis concerns.
    :type methods: Dict[str, List[str]]
    :param outpath: The path to the directory where the analysis will be saved, it will be
    saved under a subdirectory named 'experiments_analysis'.
    :type outpath: str
    :param analysis_dirname: The name to give to the analysis folder, defaults to descriptors.
    :type analysis_dirname: str, optional
    :param standard_results: Whether to generate the standard analysis or not, defaults to True.
    :type standard_results: bool, optional
    :param representative_example: Whether to generate the representative example or not,
    defaults to True.
    :type representative_example: bool, optional
    :param exec_times: Whether to generate the execution times results or not, defaults to True.
    :type exec_times: bool, optional
    """

    def _generate_boxplot_table_heatmap_for_metric(
        dstrbs: Dict,
        outpath: str,
        metric: str,
        ylabel: Optional[str] = None,
        prefix: Optional[str] = None,
    ) -> None:
        if not ylabel:
            ylabel = " ".join((f"{elem[0].upper()}{elem[1:]}" for elem in metric.split("_")))

        if not prefix:
            prefix = f"{'-'.join(metric.split('_'))}_"

        # Save the distributions boxplot
        generate_standalone_figure(
            draw_boxplot,
            out_img=os.path.join(outpath, f"descriptors_{metric}.png"),
            distributions=list(dstrbs.values()),
            distrib_labels=dstrbs.keys(),
            xlabel="Descriptors",
            ylabel=ylabel,
        )

        # Save Table containing the descriptive statistics per descriptor for the distributions
        df = pd.DataFrame(
            compute_dstrb_descriptive_statistics(dstrbs.copy(), prefix=prefix),
        ).T
        df = df.reindex(methods_ordering)
        df.to_csv(os.path.join(outpath, f"matching_{metric}_summary.csv"))

        # Save Table and heatmap with the results of the Friedman and Nemenyi tests
        metric_fried = friedman_test(*dstrbs.values())
        metric_nemenyi = nemenyi_multitest(metric_fried[-1])
        save_statistical_tests(
            filepath=os.path.join(outpath, f"matching_{metric}_statistical_tests.csv"),
            friedman_pval=metric_fried[1],
            dstrb_labels=list(dstrbs.keys()),
            nemenyi_pvals=metric_nemenyi,
        )
        named_metric_nemenyi = {desc: p_val for desc, p_val in zip(dstrbs.keys(), metric_nemenyi)}
        generate_standalone_figure(
            draw_heatmap,
            out_img=os.path.join(outpath, f"nemenyi_{metric}_heatmap.png"),
            distributions=named_metric_nemenyi,
            x_distribution_labels=list(named_metric_nemenyi.keys()),
            fig_title=f"{ylabel.split(' [')[0]} (Friedman's p-value: {metric_fried[1]:.2e})\n Nemenyi p-values",
            displayed_precision="scientific",
            cmap=generate_cmap(generate_statistical_test_color_palette(np.array(list(named_metric_nemenyi.values())))),
            adaptive_color=False,
        )

    methods_ordering = methods["descriptors"]

    # Extend the experiments targeted for the descriptors analysis
    extend_descriptor_exp_supv_outputs(exps, methods_ordering)

    desc_analysis_path = os.path.join(outpath, "experiments_analysis", analysis_dirname)
    generate_dir_if_missing(desc_analysis_path, recursive=True)

    desc_dict = generate_exp_list_per_method(exps, methods_ordering, descriptors_to_patterns)

    # Generate normalized_ranks boxplot, table and heatmap
    if standard_results:
        normalized_rank_dstrbs = extract_metric(desc_dict, methods_ordering, _extract_exp_normalized_ranks)
        _generate_boxplot_table_heatmap_for_metric(
            dstrbs=normalized_rank_dstrbs,
            outpath=desc_analysis_path,
            metric="normalized_ranks",
        )

    # Save a representative example of the normalized matching ranks
    if representative_example:
        # Get for each descriptor the experiment path that concerns the data pair
        tmp = sort_dict(
            get_experiments_from_pair(
                [
                    "/data/dataset_ANHIR/images/COAD_19/scale-10pc/S6.png",
                    "/data/dataset_ANHIR/images/COAD_19/scale-10pc/S5.png",
                ],
                desc_dict,
            ),
            methods_ordering,
        )

        # Get the landmarks coordinates and their normalized matching ranks for each experiment
        # Additionally, the target landmarks coordinates and normalized matching ranks are extracted
        ref_path = tmp[list(tmp.keys())[0]]
        coords_ranks = {"Target": _extract_coords_ranks_lndmrks(ref_path, "supv/desc_dst.csv")}
        for k, v in tmp.items():
            coords_ranks[k] = _extract_coords_ranks_lndmrks(v)

        generate_standalone_figure(
            draw_matching_landmarks,
            nrows=1,
            ncols=len(coords_ranks),
            out_img=os.path.join(desc_analysis_path, "matching_landmarks_comparison.png"),
            images=(
                imread(os.path.join(ref_path, "init/init_src.png")),
                imread(os.path.join(ref_path, "init/init_dst.png")),
            ),
            ranks_distributions=coords_ranks,
        )

    # Generate execution_times boxplot, table and heatmap
    if exec_times:
        exec_time_dstrbs = extract_metric(
            desc_dict, methods_ordering, partial(extract_exp_detr_desc_step_execution_time, steps=["descriptors"])
        )
        _generate_boxplot_table_heatmap_for_metric(
            dstrbs=exec_time_dstrbs,
            outpath=desc_analysis_path,
            metric="execution_times",
            ylabel="Execution Times [s]",
        )


if __name__ == "__main__":
    import tqdm

    from configs.config import Config

    parser = create_parser()
    args = parser.parse_args()

    run_config = os.path.join(
        args.run_path,
        *[elem for elem in os.listdir(args.run_path) if elem.startswith("run") and elem.endswith(".json")],
    )
    analysis_config = (
        args.constraints
        if args.constraints
        else os.path.join(
            args.run_path,
            *[elem for elem in os.listdir(args.run_path) if elem.startswith("analysis") and elem.endswith(".json")],
        )
    )
    cfg = Config(run_config, analysis_config)

    if not (outpath := args.outpath):
        outpath = args.run_path

    all_labels = get_all_labels_dict(os.path.join(args.run_path, "labels"))
    descriptors_analysis = [
        (label, constraints) for label, constraints in cfg.analysis.items() if constraints["type"] == "descriptors"
    ]
    for analysis_label, constraints in tqdm.tqdm(descriptors_analysis, desc="Descriptors analysis"):
        constraints = un_negate_methods(constraints=constraints, all_labels=all_labels)
        pattern = dict_to_pattern(constraints)
        exps_list = get_all_experiments_list(os.path.join(args.run_path, "experiments"))
        exps = [path for path in exps_list if match_pattern(os.path.basename(path), pattern)]

        generate_descriptors_analysis(
            exps=exps,
            methods=constraints,
            outpath=outpath,
            analysis_dirname=analysis_label if not args.dirname_prefix else f"{args.dirname_prefix}_{analysis_label}",
            standard_results=args.standard_results,
            representative_example=args.representative_example,
            exec_times=args.execution_times,
        )
