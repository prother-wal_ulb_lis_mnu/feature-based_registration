"""
This module is used to run the experiments, to extend them and optionally to generate the tables and figures
useful for the analysis of the results.
"""

import argparse
import os
import shutil
from collections import defaultdict
from typing import Any, Dict, List

import ray_ease as rez
import tqdm
from datasets.anhir import ANHIRDataset
from datasets.generic import CompleteDataset, GenericRegistrationDataset
from datasets.marzahl import ISCDataset, MSSCDataset
from experiment import FeatureBasedExperiment
from helpers.logger import ExperimentLogger, logger
from helpers.outputs import OutputsHelper
from processing.chaining import AVAILABLE_PREPROCESSING, PreProcessingLink
from psutil import Process
from registration.feature_matching import AVAILABLE_PIPELINES
from utils.errors import FailedExperiments
from utils.helpers_utils import generate_dir_if_missing

from benchmark.analysis import AVAILABLE_ANALYSIS
from benchmark.benchmark_utils import (
    dict_to_pattern,
    get_all_experiments_list,
    match_pattern,
    un_negate_methods,
)
from configs.config import CONFIG_DEFAULTS, Config, ExperimentConfig


def create_parser() -> argparse.ArgumentParser:
    """Create the arg parser.
    For addition information: https://docs.python.org/3/library/argparse.html

    :return parser: The arg parser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--outpath",
        type=str,
        required=False,
        default="/results/SIPAIM",
        help="The existing directory where the outputs of each experiment should be saved.",
    )
    parser.add_argument(
        "-e",
        "--experiments_config",
        type=str,
        required=True,
        help="The path to the JSON configuration file that explicits the comparison performed during "
        "the experiments.",
    )
    parser.add_argument(
        "-l",
        "--output-level",
        type=str,
        nargs="+",
        required=False,
        default=["NOT_LOGS"],
        help="The list of outputs to save for each experiment.",
    )
    parser.add_argument(
        "--no-cache",
        dest="no_cache",
        action="store_true",
        help="Whether to use the previous run results (cache) or not. If not, the previous `experiments` folder "
        "is renamed by adding `previous_` as a prefix.",
    )
    parser.add_argument(
        "-r",
        "--run_config",
        type=str,
        required=False,
        default="ray",
        help="The configuration which will be used to run the script. It can either be `ray` to run "
        "the application in parallel or `serial`.",
    )

    return parser


# Gray: 5 * 1024 * 1024 * 1024
# Haematoxylin: 6 * 1024 * 1024 * 1024
def run_experiment(cfg: ExperimentConfig, pair: Dict[str, Any], output_level: List[str]) -> None:
    """Run an experiment.

    :param cfg: A configuration object that contains all the settings to run a given experiment.
    :type cfg: ExperimentConfig
    :param pair: The path to the data (both src and dst).
    :type pair: Dict[str, Any]
    :param output_level: The list of outputs to save for the experiment.
    :type output_level: List[str]
    """

    # Get WholeSlide from path
    slide_pair = pair["ds_cls"].get_slides(pair)

    try:
        # Instantiate pipeline object from config
        pipeline = AVAILABLE_PIPELINES[cfg.pipeline].from_config(cfg)

        # Get pre-processing from config
        preprocessing_label, preprocessing_steps, preprocessing_kwargs = (
            cfg.extract_preprocessing_label_steps_N_kwargs(cfg.preprocessing)
        )

        # Add more general pre-processing settings to the global config level
        cfg.add_attributes_from_dict(preprocessing_kwargs)

        # Build {steps:labels} dictionary
        exp_steps = {"preprocessing": preprocessing_label}
        exp_steps.update(pipeline.labels)

        # Get the experiment name based on the steps
        prefix = "_".join(exp_steps[s[:-1]] for s in CONFIG_DEFAULTS["levels"] if exp_steps.get(s[:-1]))
        exp_basename = f"{prefix}_{FeatureBasedExperiment.__name__}"

        # Instantiate the experiment object
        exp = FeatureBasedExperiment(
            src_wsi=slide_pair["src"],
            dst_wsi=slide_pair["dst"],
            outputs_helper=OutputsHelper(
                output_level_specs=output_level,
                parent_dir=cfg.rootpath,
                basename=exp_basename,
            ),
            src_gt_path=slide_pair["src_gt"],
            dst_gt_path=slide_pair["dst_gt"],
            resolution=10.0,
            config=cfg,
            preprocessing_kwargs={
                "links": [
                    PreProcessingLink(AVAILABLE_PREPROCESSING[method], **kwargs)
                    for method, kwargs in preprocessing_steps
                ],
                "pad_value": getattr(cfg, "pad_value", 0.0),
            },
            feature_matching_pipeline=pipeline,
        )

        # Run the experiment and perform the evaluation
        exp.execute()
        exp.evaluate(extract_gt=slide_pair["gt_extractor"])

    except Exception:
        rootpath = cfg.rootpath
        cpu_id = Process().cpu_num()

        log = ExperimentLogger.get_logger(
            name=str(cpu_id),
            to_handle=["LOGFILE"],
            clear=False,
            output_dir=rootpath,
            log_filename=f"{cpu_id}_failed_exp.log",
        )
        log.info(f"{exp.dir_path} failed.")
        log.info(f"For pair: {os.path.basename(pair['src'])} - {os.path.basename(pair['dst'])}.")
        log.exception("An Error has occured, see stack trace hereunder.")


def main(rootpath: str, config_filename: str, output_level: List[str]) -> None:
    """Run the experiments, that are not already computed, and generate their analysis.

    :param rootpath: The path to the root folder where the run's outputs will be saved. If the path already
    exists, compute the difference between the previous run's configuration file and the new one.
    :type rootpath: str
    :param config_filename: The name to the .json file that will determine the run's parameters. It should be the
    name of file existing in the config folder or the name of the configuration files present in the rootpath. In
    the second scenario, the configuration files that will be taken into account are those present in the given
    rootpath.
    :type config_filename: str
    :param output_level: The list of outputs to save for each experiment.
    :type output_level: List[str]
    """

    previous_cfg = None
    if os.path.exists((history_dirpath := os.path.join(rootpath, ".history"))):
        run_config_file = os.path.join(rootpath, f"run_{config_filename}")
        analysis_config_file = os.path.join(rootpath, f"analysis_{config_filename}")

        # Get previous configuration files from .history
        previous_cfg = Config(
            os.path.join(history_dirpath, f"run_{config_filename}"),
            p if os.path.exists((p := os.path.join(history_dirpath, f"analysis_{config_filename}"))) else None,
        )
    else:
        run_config_file, analysis_config_file = config_filename, config_filename

    # Create if necessary the root folder for the run
    generate_dir_if_missing(rootpath)

    # Instantiate a configuration object from the configuration file
    cfg = Config(run_config_file=run_config_file, analysis_config_file=analysis_config_file)
    cfg.rootpath = rootpath

    # Copy configuration files in root folder if new run
    if not previous_cfg:
        shutil.copy2(cfg.run_config_filepath, os.path.join(rootpath, f"run_{config_filename}"))

        assert cfg.analysis_config_filepath is not None
        shutil.copy2(cfg.analysis_config_filepath, os.path.join(rootpath, f"analysis_{config_filename}"))

    # Create labels directory that will be gradually filled by the experiments
    labels_dir = os.path.join(rootpath, "labels")
    generate_dir_if_missing(labels_dir)
    cfg.labels_dir = labels_dir

    # Build the dataset which will be used for the run
    ds = CompleteDataset(
        datasets=[
            ANHIRDataset("/data/dataset_ANHIR"),
            MSSCDataset("/data/dataset_Marzahl/MSSC"),
            ISCDataset("/data/dataset_Marzahl/ISC"),
            # GenericRegistrationDataset("/data/cmmi/PTL01/Round_C1/3_1C1G1D")
        ]
    )

    print("Start the experiments.")

    # Get the "pseudo grid search" of ExperimentConfig. To avoid computing again experiments that are already
    # computed, only the difference with the previous run is considered. If this is the first run, the complete
    # specified grid search is taken into account.
    if previous_cfg:
        previous_cfg.rootpath, previous_cfg.labels_dir = rootpath, labels_dir
        pseudo_grid_search = [config for config in cfg.grid_search if config not in previous_cfg.grid_search]
    else:
        pseudo_grid_search = cfg.grid_search

    progress = tqdm.tqdm(
        range(len(ds.dev_set) * len(pseudo_grid_search)),
        desc="Experiments",
        disable=os.getenv("RAY_EASE") == "ray",
    )

    run_func_dict = {
        "low_consumption": rez.parallelize(memory=4 * 1024 * 1024 * 1024)(run_experiment),
        "high_consumption": rez.parallelize(memory=10 * 1024 * 1024 * 1024)(run_experiment),
    }

    futures = []
    for pair in ds.dev_set.values():
        if pair["origin"] == "ANHIRDataset":
            run_func = run_func_dict["low_consumption"]
        else:
            run_func = run_func_dict["high_consumption"]

        for config in pseudo_grid_search:
            futures.append(run_func(cfg=config, pair=pair, output_level=output_level))

            # Update progress bar for serial computing
            if os.getenv("RAY_EASE") != "ray":
                progress.update()

    rez.retrieve(futures, parallel_progress=True, parallel_progress_kwargs={"desc": "Experiments"})
    del futures

    # Clear logger to avoid writing in the last logfile
    ExperimentLogger.clear_handlers(logger=logger)

    if os.path.exists(os.path.join(rootpath, "experiments", "logs")):
        raise FailedExperiments

    all_labels = defaultdict(list)
    exps_current_N_expected_locations = {}
    for exp_path in get_all_experiments_list(cfg.experiments_path):
        exp = os.path.basename(exp_path)
        if exp == "logs":
            continue

        steps_labels = {
            step: label
            for step, label in zip(CONFIG_DEFAULTS["levels"], exp.split("_")[: len(CONFIG_DEFAULTS["levels"])])
        }

        # Retrieve the labels from the experiment
        # Compute the destination location of the experiment
        expected_dirpath = cfg.experiments_path
        for step in cfg.levels:
            label = steps_labels[step]
            if label not in all_labels[step]:
                all_labels[step].append(label)

            expected_dirpath = os.path.join(expected_dirpath, label)

            # Store current and expected location to move the experiments once the post-processing is done
            exps_current_N_expected_locations[exp_path] = os.path.join(expected_dirpath, exp)

    # Write labels files
    for step in all_labels:
        # Sort step labels based on the order they are given in the configuration file, with {labels:index}
        # If a specific label is not in the dictionary, then sort it at the end
        labels = sorted(
            all_labels[step], key=lambda x: cfg.get_target_step_methods_ordering(step).get(x, len(all_labels[step]))
        )
        all_labels[step] = labels

        assert cfg.labels_dir is not None
        path = os.path.join(cfg.labels_dir, f"{step}.txt")
        with open(path, "w", encoding="utf-8") as fp:
            fp.write("\n".join(labels))

    output_level = OutputsHelper.convert_output_level(output_level)

    # Copy/replace `run_<config>.json` in .history
    generate_dir_if_missing(os.path.join(rootpath, ".history"))
    if os.path.exists((dest_path := os.path.join(rootpath, ".history", f"run_{config_filename}"))):
        os.remove(dest_path)
    shutil.copy2(os.path.join(rootpath, f"run_{config_filename}"), dest_path)

    if "SUPV_QUANT" in output_level and "LOGFILE" in output_level:
        if cfg.analysis:
            print("Generate tables and figures.")

            for analysis_label, constraints in cfg.analysis.items():
                analysis = constraints.get("type")
                print(f"Generate analysis for: {analysis_label} ({analysis}).")

                # Translate methods from `not_<step>` into methods for step
                constraints = un_negate_methods(constraints=constraints, all_labels=all_labels)

                pattern = dict_to_pattern(constraints)

                exps = [
                    path
                    for path in exps_current_N_expected_locations
                    if match_pattern(os.path.basename(path), pattern)
                ]

                AVAILABLE_ANALYSIS[analysis](
                    exps=exps, methods=constraints, outpath=cfg.rootpath, analysis_dirname=analysis_label
                )

    # Copy/replace `analysis_<config>.json` in .history
    generate_dir_if_missing(os.path.join(rootpath, ".history"))
    if os.path.exists((dest_path := os.path.join(rootpath, ".history", f"analysis_{config_filename}"))):
        os.remove(dest_path)
    shutil.copy2(os.path.join(rootpath, f"analysis_{config_filename}"), dest_path)

    # Move experiments to their destination locations
    for current, expected in exps_current_N_expected_locations.items():
        generate_dir_if_missing(os.path.dirname(expected), recursive=True)
        shutil.move(current, expected)


if __name__ == "__main__":
    import time

    start = time.time()
    parser = create_parser()
    args = parser.parse_args()

    rez.init(args.run_config, log_to_driver=False)
    print(f"Running using configuration: {args.run_config}")

    # If `no-cache` and already an "experiments" subfolder, then move all the files in "previous_experiments"
    # subfolder and use "experiments" for the new run
    if args.no_cache and os.path.exists((previous_exp_dirpath := os.path.join(args.outpath, "experiments"))):
        generate_dir_if_missing(os.path.join(args.outpath, "previous_experiments"))
        for current in get_all_experiments_list(previous_exp_dirpath):
            shutil.move(current, current.replace("experiments", "previous_experiments"))

        # Remove "experiments" and ".history" directories
        shutil.rmtree(previous_exp_dirpath)
        shutil.rmtree(os.path.join(args.outpath, ".history"))

    main(rootpath=args.outpath, config_filename=args.experiments_config, output_level=args.output_level)
    total = time.time() - start
    print(f"duration = {int((total//60)//60)}h {int((total//60)%60)}min {int(total%60)}s")
