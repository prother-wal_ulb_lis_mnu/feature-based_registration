"""
Utils to generate the tables and figures.
"""

import argparse
import csv
import os
from numbers import Number
from typing import Any, Callable, Dict, List, Mapping, Optional, Tuple, TypeVar, Union

import numpy as np
import pandas as pd
from metrics.common import compute_descriptive_statistics, compute_interquartile

T = TypeVar("T", bound=Number)


def create_parser() -> argparse.ArgumentParser:
    """Create the arg parser.
    For addition information: https://docs.python.org/3/library/argparse.html

    :return parser: The arg parser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--run_path",
        type=str,
        required=True,
        help="The path to the root folder that contains everything related to a specific run.",
    )
    parser.add_argument(
        "-o",
        "--outpath",
        type=str,
        required=False,
        default=None,
        help="The path to the directory where the analysis should be saved. "
        "Only needed if the analysis should not be saved at the experiments root directory.",
    )
    parser.add_argument(
        "-n",
        "--dirname_prefix",
        type=str,
        required=False,
        default=None,
        help="A prefix to place before the analysis directories' names.",
    )
    parser.add_argument(
        "-c",
        "--constraints",
        type=str,
        required=False,
        default=None,
        help="The name of the configuration file to use for the analysis of the experiments.",
    )
    parser.add_argument(
        "--standard_results",
        dest="standard_results",
        action="store_true",
        help="Whether to generate the standard results or not.",
    )
    parser.add_argument(
        "--representative_example",
        dest="representative_example",
        action="store_true",
        help="Whether to generate the representative example or not.",
    )
    parser.add_argument(
        "--execution_times",
        dest="execution_times",
        action="store_true",
        help="Whether to generate the execution times results or not.",
    )

    return parser


def extract_exp_registration_metric(exp_path: str, metric: str = "rmse") -> np.ndarray:
    """Extract a specified metric for a given experiment.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :param metric: The metric to extract, defaults to "rmse".
    :type metric: str, optional
    :return: The desired metric for the given experiment.
    :rtype: np.ndarray
    """

    quant_supv_path = os.path.join(exp_path, "supv", "quant_metrics_global.csv")
    df = pd.read_csv(quant_supv_path, index_col=0)

    return np.array(df[metric])


def extract_exp_detr_desc_step_execution_time(
    exp_path: str,
    steps: List[str] = ["detectors", "descriptors"],
    merged: bool = False,
) -> np.ndarray:
    """Extract a the execution time of at least one step of the pipeline for a given experiment.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :param steps: The steps that should be included in the final execution time, defaults to
    ["detectors", "descriptors"].
    :type steps: List[str], optional
    :param merged: Whether the execution times of both image should be combined or not, defaults to False.
    :type merged: bool, optional
    :return: The execution times of both image or their sum.
    :rtype: np.ndarray
    """

    prefixes = ("src", "dst")

    exec_times_path_path = os.path.join(exp_path, "logs", "logs_exec_times.csv")
    df = pd.read_csv(exec_times_path_path)

    exec_times = {p: 0 for p in prefixes}
    for step in steps:
        for prefix in prefixes:
            exec_times[prefix] += df[f"{prefix}_{step[:-1]}"][0]

    if merged:
        return np.array([exec_times["src"] + exec_times["dst"]])

    return np.array([t for t in exec_times.values()])


def extract_metric(
    aggregated_exps_dict: Dict[str, List[str]],
    aggregations_ordering: List[str],
    extraction_method: Callable[..., np.ndarray],
    **extraction_method_kwargs: Any,
) -> Dict:
    """Extract, for all aggregated lists of experiments, a given metric.

    :param aggregated_exps_dict: The collection of lists of experiments aggregated by a pattern (either per
    step or combination of steps).
    :type aggregated_exps_dict: Dict[str, List[str]]
    :param aggregations_ordering: The order in which the aggregations should be displayed.
    :type aggregations_ordering: List[str]
    :param extraction_method: The method used to extract a given metric from the experiments outputs.
    :type extraction_method: Callable[..., np.ndarray]
    :return out: The collection of extracted metrics for all the aggregated lists of experiments.
    :rtype out: Dict
    """

    # Not dict comprehension because it gives a mutable dict and the type hinting fails
    out = {}
    for i in aggregations_ordering:
        out[i] = np.nan

    for key, value in aggregated_exps_dict.items():
        tmp: List[float] = []
        for exp_path in value:
            metric = extraction_method(exp_path, **extraction_method_kwargs)
            tmp += list(metric)

        out[key] = tmp

    return out


def compute_dstrb_descriptive_statistics(
    distributions: Dict[str, List[float]], prefix: str = "", string_format: str = "float_2"
) -> Dict[str, Dict[str, str]]:
    """Compute the distributions descriptive statistics.

    :param distributions: The distributions which will be described.
    :type distributions: Dict[str, List[float]]
    :param prefix: The prefix for the distributions, defaults to "".
    :type prefix: str, optional
    :param string_format: To which precision the descriptive statistics should be displayed in.
    It can either be ``int`` or ``float_x`` where x is the number of decimals, defaults to float_2.
    :type string_format: str, optional
    :return desc_stats_dstrbs: The distributions descriptive statistics.
    :rtype: Dict[str, Dict[str, str]]
    """

    def __format_median(m: int | float, string_format: str) -> str:
        to_display = ""
        if string_format == "int":
            to_display = str(round(m))
        elif string_format == "float":
            to_display = str(round(m, 2))
        elif "float" in string_format:
            ndecimals = string_format.split("_")[-1]

            try:
                ndecimals = int(ndecimals)
            except ValueError:
                ndecimals = 2

            to_display = str(round(m, ndecimals))
        else:
            raise ValueError(
                f"Bad format ({string_format}), the possible values are: int, float, float_x (where x "
                "is the precision to which the median will be rounded)."
            )

        return to_display

    desc_stats_dstrbs = {}
    for k in distributions:
        to_compute = [np.median, compute_interquartile]
        desc_stats = compute_descriptive_statistics(distributions[k], to_compute)
        desc_stats_dstrbs[k] = {
            f"{prefix}median": __format_median(desc_stats[0], string_format),
            f"{prefix}interquartile_range": f"[{__format_median(desc_stats[1][0], string_format)}-"
            f"{__format_median(desc_stats[1][1], string_format)}]",
        }

    return desc_stats_dstrbs


def save_statistical_tests(
    filepath: str,
    friedman_pval: float,
    dstrb_labels: List[str],
    nemenyi_pvals: np.ndarray,
    metadata: Optional[str] = None,
) -> None:
    """Save the statistical tests results in a .csv file.

    :param filepath: The path to the .csv file.
    :type filepath: str
    :param friedman_pval: The Friedman's test p-value.
    :type friedman_pval: float
    :param dstrb_labels: The labels of the different distributions.
    :type dstrb_labels: List[str]
    :param nemenyi_pvals: The pairwise Nemenyi post-hoc test p-values.
    :type nemenyi_pvals: np.ndarray
    :param metadata: Additional metadata that should be present in the file (e.g. the methods for the other steps),
    defaults to None.
    :type metadata: Optional[str], optional
    """

    with open(filepath, mode="w", encoding="utf-8") as fp:
        writer = csv.writer(fp, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

        if metadata:
            writer.writerow([f"Metadata: {metadata}"])
            writer.writerow([])

        writer.writerow(["Friedman's test p-value"])
        writer.writerow([friedman_pval])

        for _ in range(2):
            writer.writerow([])

        writer.writerow(["Nemenyi post-hoc test", *dstrb_labels])
        for ind, nem_pval in enumerate(nemenyi_pvals):
            writer.writerow([dstrb_labels[ind], *nem_pval])


def get_experiments_from_pair(
    pair: List[str], aggregated_exps_dict: Dict[str, List[str]], ordered: bool = False
) -> Dict[str, str]:
    """Get the path of each experiment that is based on a given image pair.

    :param pair: The given image pair ["src", "dst"].
    :type pair: List[str]
    :param aggregated_exps_dict: The collection of lists of experiments aggregated by a pattern (either per
    step or combination of steps).
    :type aggregated_exps_dict: Dict[str, List[str]]
    :param ordered: Whether the experiments are already ordered in which case the index from the matching
    experiment for the first aggregated list is used as a shortcut to find the other experiments, defaults
    to False.
    :type ordered: bool, optional
    :return matching_exps: The collection of experiments that satisfy the condition.
    :rtype matching_exps: Dict[str, str]
    """

    matching_exps = {}
    index = None
    for aggregation_name, exps in aggregated_exps_dict.items():
        # Shortcut if ordered and found for the aggregated list
        if index is not None:
            matching_exps[aggregation_name] = exps[index]
            continue

        for ind, exp in enumerate(exps):
            tmp = pd.read_csv(os.path.join(exp, "supv/quant_metrics_global.csv"))
            if pair[0] in tmp["src_path"][0] and pair[1] in tmp["dst_path"][0]:
                if ordered:
                    index = ind

                matching_exps[aggregation_name] = exp
                break

    return matching_exps


def sort_dict(d: Dict[str, str], sorting_order: List[str]) -> Dict[str, str]:
    """Sort the given dictionary based on a given list of its keys.

    :param d: The dictionary to sort.
    :type d: Dict[str, str]
    :param sorting_order: The sorting order of the dictionary keys.
    :type sorting_order: List[str]
    :return: The sorted dictionary.
    :rtype: Dict[str, str]
    """

    return {o: d[o] for o in sorting_order}
