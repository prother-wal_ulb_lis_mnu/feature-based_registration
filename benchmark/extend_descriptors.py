"""
Module used to extend the experiments for the descriptors analysis later on:
(i) The mask to filter the landmarks common to all descriptors (missing landmarks in some cases due to the image edges)
(ii) The matching ranks of all dst landmarks
"""

import os
from collections import defaultdict
from typing import Callable, Dict, List, Tuple

import cv2
import numpy as np
import pandas as pd
from metrics.descriptive import compute_lndmrks_ranks

from benchmark.benchmark_utils import (
    dict_to_pattern,
    generate_exp_list_per_method,
    generate_exp_zip,
)

DESC_DIST_N_TYPE = {
    "Random": ["norm_l2", np.float32],
    "Neighborhood": ["norm_l2", np.float32],
    "SIFT": ["norm_l2", np.float32],
    "SURF": ["norm_l2", np.float32],
    "BRISK": ["norm_hamming", np.uint8],
    "FREAK": ["norm_hamming", np.uint8],
    "BRIEF": ["norm_hamming", np.uint8],
    "ORB": ["norm_hamming", np.uint8],
    "VGG": ["norm_l2", np.float32],
    "DAISY": ["norm_l2", np.float32],
    "LATCH": ["norm_hamming", np.uint8],
    "Boost": ["norm_hamming", np.uint8],
}

CV_DISTANCES = {
    "norm_l2": cv2.NORM_L2,
    "norm_l1": cv2.NORM_L1,
    "norm_hamming": cv2.NORM_HAMMING,
    "norm_hamming2": cv2.NORM_HAMMING2,
}


def extract_coord_desc(df: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray]:
    """Extract the coordinates and descriptors.

    :param df: The dataframe from which the coordinates and the descriptors should be extracted.
    :type df: pd.DataFrame
    :return: The coordinates and the descriptors.
    :rtype: Tuple[np.ndarray, np.ndarray]
    """

    coords = []
    desc = []
    for c in df.columns:
        if not c.startswith("desc"):
            if c.startswith("x") or c.startswith("y"):
                coords.append(df[c])
            continue

        desc.append(df[c])

    return np.array(coords).T, np.array(desc).T


def _multiply_masks(masks: List[np.ndarray]) -> np.ndarray:
    """Get a boolean mask that results from the combination of multiple masks.

    :param masks: The different masks to combine.
    :type masks: List[np.ndarray]
    :return mask: The resulting mask.
    :rtype: np.ndarray
    """

    mask = np.ones((masks[0].shape[0],), dtype=bool)
    for m in masks:
        mask *= m

    return mask


def _extract_src_lndmrks_coords(corresponding_exps: Tuple[str, ...], descriptors: List[str]) -> Dict[str, np.ndarray]:
    """Extract the src landmarks coordinates for all the experiments performed on the same image pair.

    :param corresponding_exps: The experiments paths.
    :type corresponding_exps: Tuple[str, ...]
    :param descriptors: The list of descriptors.
    :type descriptors: List[str]
    :return descriptors_src_lndmrks_coords: The collection of src landmarks coordinates per experiment.
    :rtype: Dict[str, np.ndarray]
    """

    descriptors_src_lndmrks_coords = {}
    for exp, descriptor in zip(corresponding_exps, descriptors):
        src_df = pd.read_csv(os.path.join(exp, "supv/desc_src.csv"))
        descriptors_src_lndmrks_coords[descriptor] = np.array([src_df["x"], src_df["y"]]).T

    return descriptors_src_lndmrks_coords


def _compute_common_landmarks_masks(descriptors_src_lndmrks_coords: Dict[str, np.ndarray]) -> Dict[str, np.ndarray]:
    """Compute the mask of common landmarks throughout all the experiment concerning the same image pair.

    :param descriptors_src_lndmrks_coords: The collection of src landmarks coordinates per experiment.
    :type descriptors_src_lndmrks_coords: Dict[str, np.ndarray]
    :return masks: The collection of masks for each experiment.
    :rtype: Dict[str, np.ndarray]
    """

    tmp = defaultdict(list)
    masks = {}
    for key, value in descriptors_src_lndmrks_coords.items():
        for k, v in descriptors_src_lndmrks_coords.items():
            if key == k:
                tmp[key].append(np.ones((value.shape[0],), dtype=bool))
            else:
                tmp[key].append(_multiply_masks(list(np.in1d(value, v).reshape(value.shape).T)))
        masks[key] = _multiply_masks(tmp[key])

    return masks


def save_pair_lndmrks_mask(corresponding_exps: Tuple[str, ...], descriptors: List[str]) -> None:
    """Save the landmarks mask for each experiment that concerns a common image pair in the experiment folder at
    supv/lndmrks_mask.npy.

    :param corresponding_exps: The experiments paths.
    :type corresponding_exps: Tuple[str, ...]
    :param descriptors: The list of descriptors.
    :type descriptors: List[str]
    """

    descriptors_src_lndmrks_coords = _extract_src_lndmrks_coords(corresponding_exps, descriptors)
    masks = _compute_common_landmarks_masks(descriptors_src_lndmrks_coords)

    for exp, descriptor in zip(corresponding_exps, descriptors):
        np.save(os.path.join(exp, "supv/lndmrks_mask.npy"), masks[descriptor])


def _get_experiment_landmarks_matching_ranks(
    exp_path: str, distance: int = cv2.NORM_L2, dtype: type = np.float32
) -> np.ndarray:
    """Get the landmarks matching ranks when knn with all neighbors.

    :param exp_path: Path to the experiment.
    :type exp_path: str
    :param distance: The distance that is used to match the two sets of points,
    defaults to cv2.NORM_L2.
    :type distance: int, optional
    :param dtype: The descriptors' data type, defaults to np.float32.
    :type dtype: type, optional
    :return ranks: The ranks of the corresponding dst landmarks for the
    given experiment.
    :rtype: np.ndarray
    """

    _, src_lndmrks_desc = extract_coord_desc(pd.read_csv(os.path.join(exp_path, "supv/desc_src.csv")))
    _, dst_lndmrks_desc = extract_coord_desc(pd.read_csv(os.path.join(exp_path, "supv/desc_dst.csv")))

    mask = np.load(os.path.join(exp_path, "supv/lndmrks_mask.npy"))

    ranks = compute_lndmrks_ranks(
        src_lndmrks_desc[mask, :].astype(dtype), dst_lndmrks_desc[mask, :].astype(dtype), distance
    )

    # Correct the indices of the src landmarks with the mask exclusions
    ranks[:, 0] = np.arange(src_lndmrks_desc.shape[0])[mask]

    return ranks


def save_exp_ranks(
    exp_path: str, descriptor: str, ranks_method: Callable = _get_experiment_landmarks_matching_ranks
) -> None:
    """Save the landmarks ranks in the experiment folder at supv/ranks_lndmrks_desc.csv.

    :param exp_path: The path to the experiment.
    :type exp_path: str
    :param descriptor: The descriptor label.
    :type descriptor: str
    :param ranks_method: The method used to compute the ranks, defaults to get_experiment_landmarks_match_ranks.
    :type ranks_method: Callable, optional
    """

    if not DESC_DIST_N_TYPE.get(descriptor, None):
        # Check if any of the descriptors in `DESC_DIST_N_TYPE` is in the descriptor label, it it is the case
        # then use the values from this descriptor
        if tmp := [d for d in DESC_DIST_N_TYPE.keys() if d in descriptor]:
            descriptor = tmp[0]
        else:
            raise KeyError(
                f"{descriptor} is not one of the known descriptor. The list of known descriptors is: "
                f"{','.join(DESC_DIST_N_TYPE.keys())}"
            )

    d, t = DESC_DIST_N_TYPE[descriptor]
    ranks = ranks_method(exp_path, distance=CV_DISTANCES[d], dtype=t)

    df = pd.DataFrame(ranks, columns=["src_lndmrk_ind", "dst_lndmrks_matching_rank"])
    df.to_csv(os.path.join(exp_path, "supv/ranks_lndmrks_desc.csv"))


def descriptors_to_patterns(methods_ordering: List[str]) -> List[str]:
    """Transform a list of descriptors' methods to a list of patterns that can be matched.

    :param methods_ordering: The descriptors' methods.
    :type methods_ordering: List[str]
    :return: The list of patterns (one for each method).
    :rtype: List[str]
    """

    return [dict_to_pattern({"descriptors": [m]}) for m in methods_ordering]


def extend_descriptor_exp_supv_outputs(
    exps: List[str], methods_ordering: List[str] = list(DESC_DIST_N_TYPE.keys())
) -> None:
    """Extend the supv outputs of the leftmost combinations' experiments with the landmarks' mask and the
    matching ranks of dst landmarks.

    :param exps: The list of experiments to extend.
    :type exps: List[str]
    :param methods_ordering: The order in which the methods should be displayed, defaults to
    list(DESC_DIST_N_TYPE.keys()).
    :type methods_ordering: List[str], optional
    """

    print("Extend experiments for descriptors analysis.")

    desc_dict = generate_exp_list_per_method(exps, methods_ordering, descriptors_to_patterns)

    for corresponding_exps in generate_exp_zip(desc_dict, methods_ordering):
        save_pair_lndmrks_mask(corresponding_exps, methods_ordering)
        for i, exp in enumerate(corresponding_exps):
            save_exp_ranks(exp, descriptor=methods_ordering[i])
