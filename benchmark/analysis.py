"""
This module is used to gather all the available analysis in a single dictionary.
"""

from benchmark.analysis_descriptors import DESCRIPTORS
from benchmark.analysis_detectors_descriptors import DETECTORS_DESCRIPTORS
from benchmark.analysis_generic import GENERIC

AVAILABLE_ANALYSIS = GENERIC
AVAILABLE_ANALYSIS.update(DESCRIPTORS)
AVAILABLE_ANALYSIS.update(DETECTORS_DESCRIPTORS)

if __name__ == "__main__":
    import os
    from functools import partial

    import ray_ease as rez
    import tqdm

    from benchmark.analysis_generic import extend_parser
    from benchmark.analysis_utils import create_parser
    from benchmark.benchmark_utils import (
        dict_to_pattern,
        get_all_experiments_list,
        get_all_labels_dict,
        match_pattern,
        un_negate_methods,
    )
    from configs.config import Config

    parser = create_parser()
    parser = extend_parser(parser)
    args = parser.parse_args()

    run_config = os.path.join(
        args.run_path,
        *[elem for elem in os.listdir(args.run_path) if elem.startswith("run") and elem.endswith(".json")],
    )
    analysis_config = (
        args.constraints
        if args.constraints
        else os.path.join(
            args.run_path,
            *[elem for elem in os.listdir(args.run_path) if elem.startswith("analysis") and elem.endswith(".json")],
        )
    )
    cfg = Config(run_config, analysis_config)

    if not (outpath := args.outpath):
        outpath = args.run_path

    config = "serial"
    if args.mosaics:
        config = "ray"

    rez.init(config=config)

    all_labels = get_all_labels_dict(os.path.join(args.run_path, "labels"))
    for analysis_label, constraints in tqdm.tqdm(cfg.analysis.items(), desc="Analysis"):
        analysis = constraints.get("type")

        constraints = un_negate_methods(constraints=constraints, all_labels=all_labels)
        pattern = dict_to_pattern(constraints)
        exps_list = get_all_experiments_list(os.path.join(args.run_path, "experiments"))
        exps = [path for path in exps_list if match_pattern(os.path.basename(path), pattern)]

        analysis_callable = AVAILABLE_ANALYSIS[analysis]
        if analysis == "generic":
            analysis_callable = partial(
                analysis_callable,
                standard_results=args.standard_results,
                representative_example=args.representative_example,
                to_mosaic=args.mosaics,
            )
        elif analysis == "descriptors":
            analysis_callable = partial(
                analysis_callable,
                standard_results=args.standard_results,
                representative_example=args.representative_example,
                exec_times=args.execution_times,
            )
        elif analysis == "detectors_descriptors":
            analysis_callable = partial(analysis_callable, exec_times=args.execution_times)

        analysis_callable(
            exps=exps,
            methods=constraints,
            outpath=outpath,
            analysis_dirname=analysis_label if not args.dirname_prefix else f"{args.dirname_prefix}_{analysis_label}",
        )
