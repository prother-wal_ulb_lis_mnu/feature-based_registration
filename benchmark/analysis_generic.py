"""
Module used to generate the generic analysis on the experiments.
"""

import argparse
import functools
import os
from typing import Dict, List, Optional

import numpy as np
import pandas as pd
import ray_ease as rez
from metrics.common import friedman_test, nemenyi_multitest
from utils.general_utils import available
from utils.helpers_utils import generate_dir_if_missing
from viz.colormaps import generate_cmap, generate_statistical_test_color_palette
from viz.wsi import draw_boxplot, draw_heatmap, draw_mosaic, generate_standalone_figure

from benchmark.analysis_utils import (
    compute_dstrb_descriptive_statistics,
    create_parser,
    extract_exp_registration_metric,
    extract_metric,
    get_experiments_from_pair,
    save_statistical_tests,
    sort_dict,
)
from benchmark.benchmark_utils import (
    dict_to_pattern,
    generate_exp_list_per_method,
    generate_exp_zip,
    get_all_combinations,
    get_all_experiments_list,
    get_all_labels_dict,
    get_pair_name,
    match_pattern,
    un_negate_methods,
)

GENERIC = {}


def extend_parser(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.add_argument(
        "-m",
        "--mosaics",
        type=str,
        nargs="+",
        required=False,
        default=None,
        help="The list of type of images to compare in a mosaic.",
    )

    return parser


@rez.parallelize
def save_mosaics(
    exps: Dict[str, str],
    to_mosaic: List[str],
    outpath: str,
    max_n_cols: int = 4,
) -> None:
    """Save the n mosaic images in n sub-folders corresponding to the n entries in `to_mosaic`.

    :param exps: The experiments that correspond to the same image pair.
    :type exps: Dict[str, str]
    :param to_mosaic: The different outputs to mosaic: preproc, overlap, supv and unsupv. For example, `preproc`
    will create a mosaic per image for each image pair.
    :type to_mosaic: List[str]
    :param outpath: The path where the images should be saved.
    :type outpath: str
    :param max_n_cols: The maximum number of images to display in a single row, defaults to 4.
    :type max_n_cols: int, optional
    """

    name = get_pair_name(list(exps.values())[0], separator="|", to_remove=".png")
    for element in to_mosaic:
        parent_dir = os.path.join(outpath, element)

        generate_dir_if_missing(parent_dir)

        match element:
            case "preproc":
                tmp = [os.path.join(list(exps.values())[0], "init" + os.sep + "init_src.png")]
                imgs_path = tmp + [os.path.join(exp, element + os.sep + "preproc_src.png") for exp in exps.values()]
                size = len(imgs_path)
                generate_standalone_figure(
                    draw_mosaic,
                    nrows=int(np.ceil(size / max_n_cols)),
                    ncols=size if size <= max_n_cols else max_n_cols,
                    out_img=os.path.join(parent_dir, "src_" + name.split("|")[0] + ".png"),
                    imgs_path=imgs_path,
                    labels=["RGB init"] + list(exps.keys()),
                )
                tmp = [os.path.join(list(exps.values())[0], "init" + os.sep + "init_dst.png")]
                imgs_path = tmp + [os.path.join(exp, element + os.sep + "preproc_dst.png") for exp in exps.values()]
                size = len(imgs_path)
                generate_standalone_figure(
                    draw_mosaic,
                    nrows=int(np.ceil(size / max_n_cols)),
                    ncols=size if size <= max_n_cols else max_n_cols,
                    out_img=os.path.join(parent_dir, "dst_" + name.split("|")[-1] + ".png"),
                    imgs_path=imgs_path,
                    labels=["RGB init"] + list(exps.keys()),
                )
            case "overlap":
                tmp = [os.path.join(list(exps.values())[0], element + os.sep + "overlap_init.png")]
                imgs_path = tmp + [os.path.join(exp, element + os.sep + "overlap_warped.png") for exp in exps.values()]
                size = len(imgs_path)
                generate_standalone_figure(
                    draw_mosaic,
                    nrows=int(np.ceil(size / max_n_cols)),
                    ncols=size if size <= max_n_cols else max_n_cols,
                    out_img=os.path.join(parent_dir, name + ".png"),
                    imgs_path=imgs_path,
                    labels=["Init"] + list(exps.keys()),
                )
            case "supv":
                imgs_path = [os.path.join(exp, element + os.sep + "supv_inwata.png") for exp in exps.values()]
                size = len(imgs_path)
                generate_standalone_figure(
                    draw_mosaic,
                    nrows=int(np.ceil(size / max_n_cols)),
                    ncols=size if size <= max_n_cols else max_n_cols,
                    out_img=os.path.join(parent_dir, name + ".png"),
                    imgs_path=imgs_path,
                    labels=list(exps.keys()),
                )
            case "unsupv":
                imgs_path = [os.path.join(exp, element + os.sep + "unsupv_matching.png") for exp in exps.values()]
                size = len(imgs_path)
                generate_standalone_figure(
                    draw_mosaic,
                    nrows=int(np.ceil(size / max_n_cols)),
                    ncols=size if size <= max_n_cols else max_n_cols,
                    out_img=os.path.join(parent_dir, name + ".png"),
                    imgs_path=imgs_path,
                    labels=list(exps.keys()),
                )


def generic_to_patterns(methods_ordering: List[str], concerned_steps: List[str]) -> List[str]:
    """Transform a list of descriptors' methods to a list of patterns that can be matched.

    :param methods_ordering: The descriptors' methods.
    :type methods_ordering: List[str]
    :param concerned_steps: The steps that will be used to build the patterns.
    :type concerned_steps: Dict[str, List[str]]
    :return: The list of patterns (one for each method).
    :rtype: List[str]
    """

    return [dict_to_pattern({s: [m] for s, m in zip(concerned_steps, label.split("_"))}) for label in methods_ordering]


@available(GENERIC, newname="generic")
def generate_generic_analysis(
    exps: List[str],
    methods: Dict[str, List[str]],
    outpath: str,
    analysis_dirname: str = "generic",
    standard_results: bool = True,
    representative_example: bool = True,
    to_mosaic: Optional[List[str]] = None,
) -> None:
    """Generate the tables and figures for the generic analysis.

    :param exps: The list of experiments to analyse.
    :type exps: List[str]
    :param methods: The collection of methods for each step that this analysis concerns.
    :type methods: Dict[str, List[str]]
    :param outpath: The path to the directory where the analysis will be saved, it will be
    saved under a subdirectory named 'experiments_analysis'.
    :type outpath: str
    :param analysis_dirname: The name to give to the analysis folder, defaults to generic.
    :type analysis_dirname: str, optional
    :param standard_results: Whether to generate the standard analysis or not,
    defaults to True.
    :type standard_results: bool, optional
    :param representative_example: Whether to generate the representative example or not,
    defaults to True.
    :type representative_example: bool, optional
    :param to_mosaic: The different outputs to mosaic: preproc, overlap, supv and unsupv. For example, `preproc`
    will create a mosaic per image for each image pair. It defaults to None.
    :type to_mosaic: Optional[List[str]], optional
    """

    concerned_steps = [s for s, m in methods.items() if len(m) > 1 and s != "type"]
    methods_ordering = get_all_combinations(methods, concerned_steps=concerned_steps)

    generic_analysis_path = os.path.join(outpath, "experiments_analysis", analysis_dirname)
    generate_dir_if_missing(generic_analysis_path, recursive=True)

    # Set the `concerned_steps` arg from `generic_to_patterns()`
    _tmp_dict = generate_exp_list_per_method(
        exps, methods_ordering, functools.partial(generic_to_patterns, concerned_steps=concerned_steps)
    )
    # Ensure that `Init` is add as the first method
    generic_dict = {"Init": _tmp_dict[methods_ordering[0]]}
    generic_dict.update(_tmp_dict)

    common_steps = [s for s, m in methods.items() if len(m) <= 1]
    context_methods = ", ".join([f"{step[:-1]}={methods[step][0]}" for step in common_steps])

    if standard_results:
        rmse_dstrb = extract_metric(
            generic_dict, ["Init"] + methods_ordering, extract_exp_registration_metric, metric="rmse"
        )
        rmse_dstrb.update(
            extract_metric(
                {"Init": generic_dict["Init"]}, ["Init"], extract_exp_registration_metric, metric="init_rmse"
            )
        )

        generate_standalone_figure(
            draw_boxplot,
            out_img=os.path.join(generic_analysis_path, "descriptors_rmse.png"),
            distributions=list(rmse_dstrb.values()),
            distrib_labels=list(rmse_dstrb.keys()),
            xlabel="Descriptors",
            ylabel="RMSE [microns]",
            fig_title=f"RMSE distributions\n({context_methods})",
            log=True,
            displayed_median_precision="int",
        )

        # Save Table containing the descriptive statistics per descriptor for the normalized ranks distributions
        df = pd.DataFrame(
            compute_dstrb_descriptive_statistics(rmse_dstrb.copy(), prefix="RMSE_", string_format="int"),
        ).T
        df = df.reindex(["Init"] + methods_ordering)
        df.to_csv(os.path.join(generic_analysis_path, "rmse_summary.csv"))

        # Save Table and heatmap with the results of the Friedman and Nemenyi tests
        rmse_fried = friedman_test(*rmse_dstrb.values())
        rmse_nemenyi = nemenyi_multitest(rmse_fried[-1])
        save_statistical_tests(
            filepath=os.path.join(generic_analysis_path, "rmse_statistical_tests.csv"),
            friedman_pval=rmse_fried[1],
            dstrb_labels=list(rmse_dstrb.keys()),
            nemenyi_pvals=rmse_nemenyi,
            metadata=context_methods,
        )
        named_rmse_nemenyi = {method: p_val for method, p_val in zip(rmse_dstrb.keys(), rmse_nemenyi)}
        generate_standalone_figure(
            draw_heatmap,
            out_img=os.path.join(generic_analysis_path, "nemenyi_rmse_heatmap.png"),
            distributions=named_rmse_nemenyi,
            x_distribution_labels=list(named_rmse_nemenyi.keys()),
            fig_title=f"RMSE (Friedman's p-value: {rmse_fried[1]:.2e})\nNemenyi p-values\n({context_methods})",
            displayed_precision="scientific",
            cmap=generate_cmap(generate_statistical_test_color_palette(np.array(list(named_rmse_nemenyi.values())))),
            adaptive_color=False,
        )

    if representative_example:
        # Get for each method the experiment path that concerns the data pair
        tmp = sort_dict(
            get_experiments_from_pair(
                [
                    "/data/dataset_ANHIR/images/lung-lesion_1/scale-2.5pc/29-041-Izd2-w35-proSPC-4-les1.png",
                    "/data/dataset_ANHIR/images/lung-lesion_1/scale-2.5pc/29-041-Izd2-w35-He-les1.png",
                ],
                generic_dict,
            ),
            ["Init"] + methods_ordering,
        )

        # Get the overlap image for each method
        ref_path = tmp[list(tmp.keys())[0]]
        path_to_overlaps = {
            key: (
                os.path.join(value, "overlap/overlap_warped.png")
                if key != "Init"
                else os.path.join(ref_path, "overlap/overlap_init.png")
            )
            for key, value in tmp.items()
            if key != "Init"
        }

        generate_standalone_figure(
            draw_mosaic,
            nrows=1,
            ncols=len(path_to_overlaps),
            out_img=os.path.join(generic_analysis_path, "overlap_comparison.png"),
            imgs_path=path_to_overlaps.values(),
            labels=path_to_overlaps.keys(),
            fig_title=f"Context: {context_methods}",
        )

    if to_mosaic:
        futures = []
        for _exps in generate_exp_zip(generic_dict, methods_ordering):
            exps_for_mosaic = {label: path for label, path in zip(methods_ordering, _exps)}
            futures.append(save_mosaics(exps_for_mosaic, to_mosaic, generic_analysis_path))

        rez.retrieve(futures, parallel_progress=True, parallel_progress_kwargs={"desc": "Pairs"})


if __name__ == "__main__":
    import tqdm

    from configs.config import Config

    parser = create_parser()
    parser = extend_parser(parser)
    args = parser.parse_args()

    run_config = os.path.join(
        args.run_path,
        *[elem for elem in os.listdir(args.run_path) if elem.startswith("run") and elem.endswith(".json")],
    )
    analysis_config = (
        args.constraints
        if args.constraints
        else os.path.join(
            args.run_path,
            *[elem for elem in os.listdir(args.run_path) if elem.startswith("analysis") and elem.endswith(".json")],
        )
    )
    cfg = Config(run_config, analysis_config)

    if not (outpath := args.outpath):
        outpath = args.run_path

    config = "serial"
    if args.mosaics:
        config = "ray"

    rez.init(config=config)

    all_labels = get_all_labels_dict(os.path.join(args.run_path, "labels"))
    generic_analysis = [
        (label, constraints) for label, constraints in cfg.analysis.items() if constraints["type"] == "generic"
    ]
    for analysis_label, constraints in tqdm.tqdm(generic_analysis, desc="Generic analysis"):
        constraints = un_negate_methods(constraints=constraints, all_labels=all_labels)
        pattern = dict_to_pattern(constraints)
        exps_list = get_all_experiments_list(os.path.join(args.run_path, "experiments"))
        exps = [path for path in exps_list if match_pattern(os.path.basename(path), pattern)]

        generate_generic_analysis(
            exps=exps,
            methods=constraints,
            outpath=outpath,
            analysis_dirname=analysis_label if not args.dirname_prefix else f"{args.dirname_prefix}_{analysis_label}",
            standard_results=args.standard_results,
            representative_example=args.representative_example,
            to_mosaic=args.mosaics,
        )
