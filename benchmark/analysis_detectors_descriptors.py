import os
from collections import defaultdict
from typing import Dict, List, Union

import numpy as np
from matplotlib import cm
from metrics.common import friedman_test, nemenyi_multitest, statistical_score
from utils.general_utils import available
from utils.helpers_utils import generate_dir_if_missing
from viz.colormaps import generate_cmap, generate_statistical_test_color_palette
from viz.wsi import draw_heatmap, generate_standalone_figure

from benchmark.analysis_utils import (
    create_parser,
    extract_exp_detr_desc_step_execution_time,
    extract_exp_registration_metric,
    extract_metric,
    save_statistical_tests,
)
from benchmark.benchmark_utils import (
    dict_to_pattern,
    generate_exp_list_per_method,
    get_all_experiments_list,
    get_all_labels_dict,
    match_pattern,
    un_negate_methods,
)
from configs.config import CONFIG_DEFAULTS

DETECTORS_DESCRIPTORS = {}


def detectors_descriptors_to_patterns(methods_ordering: List[str]) -> List[str]:
    """Transform a list of descriptors' methods to a list of patterns that can be matched.

    :param methods_ordering: The descriptors' methods.
    :type methods_ordering: List[str]
    :return: The list of patterns (one for each method).
    :rtype: List[str]
    """

    return [
        dict_to_pattern({"detectors": [m.split("_")[0]], "descriptors": [m.split("_")[1]]}) for m in methods_ordering
    ]


def _to_heatmap_dstrbs(dstrbs: Dict[str, Union[float, int]]) -> Dict[str, List[Union[float, int]]]:
    heatmap_dstrbs = defaultdict(list)
    for label, value in dstrbs.items():
        heatmap_dstrbs[label.split("_")[0]].append(value)

    return heatmap_dstrbs


@available(DETECTORS_DESCRIPTORS, newname="detectors_descriptors")
def generate_detectors_descriptors_analysis(
    exps: List[str],
    methods: Dict[str, List[str]],
    outpath: str,
    analysis_dirname: str = "detectors_descriptors",
    exec_times: bool = True,
):
    """Generate the tables and figures for the detectors_descriptors analysis.

    :param exps: The list of experiments to analyse.
    :type exps: List[str]
    :param methods: The collection of methods for each step that this analysis concerns.
    :type methods: Dict[str, List[str]]
    :param outpath: The path to the directory where the analysis will be saved, it will be
    saved under a subdirectory named 'experiments_analysis'.
    :type outpath: str
    :param analysis_dirname: The name to give to the analysis folder, defaults to detectors_descriptors.
    :type analysis_dirname: str, optional
    :param exec_times: Whether to generate the execution times results or not, defaults to True.
    :type exec_times: bool, optional
    """

    def _generate_heatmaps_table_for_metric(
        dstrbs: Dict[str, List[float]],
        outpath: str,
        metric: str,
        unit: str,
        context_methods: str,
        displayed_precision: str = "int",
    ) -> None:
        title_name = " ".join((f"{elem[0].upper()}{elem[1:]}" for elem in metric.split("_")))

        # Build median_dstrb: `{"detr_desc": medians(detr_desc_metric_dstrb)}`
        median_dstrb = {}
        for label, metrics in dstrbs.items():
            median = np.median(metrics)
            median_dstrb[label] = median

        # Generate heatmap for detectors-descriptors combinations
        generate_standalone_figure(
            draw_heatmap,
            out_img=os.path.join(outpath, f"{metric}_median_heatmap.png"),
            distributions=_to_heatmap_dstrbs(median_dstrb),
            x_distribution_labels=methods["descriptors"],
            fig_title=f"{title_name} median [{unit}]\n({context_methods})",
            displayed_precision=displayed_precision,
        )

        # Get the dstrbs that are not empty
        meaningful_metric_dstrbs = {}
        for label, metrics in dstrbs.items():
            if not np.any(np.isnan(metrics)):
                meaningful_metric_dstrbs[label] = metrics

        # Save Table and heatmap with the results of the Friedman and Nemenyi tests
        metric_fried = friedman_test(*meaningful_metric_dstrbs.values())
        metric_nemenyi = nemenyi_multitest(metric_fried[-1])
        save_statistical_tests(
            filepath=os.path.join(outpath, f"{metric}_statistical_tests.csv"),
            friedman_pval=metric_fried[1],
            dstrb_labels=list(meaningful_metric_dstrbs.keys()),
            nemenyi_pvals=metric_nemenyi,
            metadata=context_methods,
        )
        named_metric_nemenyi = {
            method: p_val for method, p_val in zip(meaningful_metric_dstrbs.keys(), metric_nemenyi)
        }
        generate_standalone_figure(
            draw_heatmap,
            out_img=os.path.join(outpath, f"nemenyi_{metric}_heatmap.png"),
            distributions=named_metric_nemenyi,
            x_distribution_labels=list(named_metric_nemenyi.keys()),
            fig_title=f"{title_name} (Friedman's p-value: {metric_fried[1]:.2e})\nNemenyi p-values\n({context_methods})",
            displayed_precision="scientific",
            cmap=generate_cmap(generate_statistical_test_color_palette(np.array(list(named_metric_nemenyi.values())))),
            adaptive_color=False,
            figsize_max=50,
            annotation_fontsize=14,
        )

        # Save statistical score as heatmap
        stat_scores_median_metric = statistical_score(
            metric_matrix=np.array([median_dstrb[label] for label in meaningful_metric_dstrbs]),
            pval_matrix=metric_nemenyi,
        )
        stat_score_dstrb = {label: np.nan for label in dstrbs}
        stat_score_dstrb.update(dict(zip(meaningful_metric_dstrbs.keys(), stat_scores_median_metric)))
        generate_standalone_figure(
            draw_heatmap,
            out_img=os.path.join(outpath, f"statistical_score_{metric}_median_N_distribution.png"),
            distributions=_to_heatmap_dstrbs(stat_score_dstrb),
            x_distribution_labels=methods["descriptors"],
            fig_title=f"Statistical scores for {title_name} median and distribution\n({context_methods})",
            displayed_precision="int",
            cmap=cm.coolwarm.reversed(),
        )

    methods_ordering = [f"{detr}_{desc}" for detr in methods["detectors"] for desc in methods["descriptors"]]

    detr_desc_analysis_path = os.path.join(outpath, "experiments_analysis", analysis_dirname)
    generate_dir_if_missing(detr_desc_analysis_path, recursive=True)

    detr_desc_dict = generate_exp_list_per_method(exps, methods_ordering, detectors_descriptors_to_patterns)

    context_methods = ", ".join(
        [
            f"{step[:-1]}={methods[step][0]}"
            for step in CONFIG_DEFAULTS["levels"]
            if step not in ("detectors", "descriptors")
        ]
    )

    # Generate heatmaps (median and statistical score) and table for rmse
    rmse_dstrbs = extract_metric(detr_desc_dict, methods_ordering, extract_exp_registration_metric, metric="rmse")
    _generate_heatmaps_table_for_metric(
        dstrbs=rmse_dstrbs,
        outpath=detr_desc_analysis_path,
        metric="rmse",
        unit="microns",
        context_methods=context_methods,
    )

    # Generate heatmaps (median and statistical score) and table for execution_times
    if exec_times:
        exec_time_dstrbs = extract_metric(detr_desc_dict, methods_ordering, extract_exp_detr_desc_step_execution_time)
        _generate_heatmaps_table_for_metric(
            dstrbs=exec_time_dstrbs,
            outpath=detr_desc_analysis_path,
            metric="execution_times",
            unit="s",
            context_methods=context_methods,
            displayed_precision="scientific",
        )


if __name__ == "__main__":
    import tqdm

    from configs.config import Config

    parser = create_parser()
    args = parser.parse_args()

    run_config = os.path.join(
        args.run_path,
        *[elem for elem in os.listdir(args.run_path) if elem.startswith("run") and elem.endswith(".json")],
    )
    analysis_config = (
        args.constraints
        if args.constraints
        else os.path.join(
            args.run_path,
            *[elem for elem in os.listdir(args.run_path) if elem.startswith("analysis") and elem.endswith(".json")],
        )
    )
    cfg = Config(run_config, analysis_config)

    if not (outpath := args.outpath):
        outpath = args.run_path

    all_labels = get_all_labels_dict(os.path.join(args.run_path, "labels"))
    detr_desc_analysis = [
        (label, constraints)
        for label, constraints in cfg.analysis.items()
        if constraints["type"] == "detectors_descriptors"
    ]
    for analysis_label, constraints in tqdm.tqdm(detr_desc_analysis, desc="Detectors Descriptors analysis"):
        constraints = un_negate_methods(constraints=constraints, all_labels=all_labels)
        pattern = dict_to_pattern(constraints)
        exps_list = get_all_experiments_list(os.path.join(args.run_path, "experiments"))
        exps = [path for path in exps_list if match_pattern(os.path.basename(path), pattern)]

        generate_detectors_descriptors_analysis(
            exps=exps,
            methods=constraints,
            outpath=outpath,
            analysis_dirname=analysis_label if not args.dirname_prefix else f"{args.dirname_prefix}_{analysis_label}",
            exec_times=args.execution_times,
        )
