import itertools
import os
import re
from collections import defaultdict
from typing import Callable, Dict, List, Optional

from configs.config import CONFIG_DEFAULTS


def un_negate_methods(constraints: Dict[str, List[str]], all_labels: Dict[str, List[str]]) -> Dict[str, List[str]]:
    """Change `"not_<step>": [methods to avoid]` to `"<step>": [methods to use]`.

    :param constraints: The dictionary specifying for each step the methods to include or to disregard.
    :type constraints: Dict[str, List[str]]
    :param all_labels: The collection of available methods for each step.
    :type all_labels: Dict[str, List[str]]
    :return new_constraints: The updated constraints.
    :rtype new_constraints: Dict[str, List[str]]
    """

    new_constraints = {}
    for step, methods in constraints.items():
        if "not_" in step:
            un_negated_step = step.split("_")[-1]
            new_constraints[un_negated_step] = [m for m in all_labels[un_negated_step] if m not in methods]
        else:
            new_constraints[step] = methods

    return new_constraints


def get_all_combinations(constraints: Dict[str, List[str]], concerned_steps: List[str]) -> List[str]:
    """Create the list of all possible combinations based on the constraints.

    :param constraints: For each step, the list of methods to include in the combinations.
    :type constraints: Dict[str, List[str]]
    :param concerned_steps: The steps that will be used to get the combinations.
    :type concerned_steps: Dict[str, List[str]]
    :return constraints: The list of all combinations derived from the constraints.
    :rtype: List[str]
    """

    return [
        "_".join(combination)
        for combination in itertools.product(
            *[constraints[step] for step in CONFIG_DEFAULTS["levels"] if step in concerned_steps]
        )
    ]


def dict_to_pattern(constraints: Dict[str, List[str]], default: str = r".+") -> str:
    """Generate from a dictionary (constraints) a pattern that can match any of the combinations.

    :param constraints: For each step, the list of methods to include in the pattern.
    :type constraints: Dict[str, List[str]]
    :param default: The default value to use when no value is provided for a given step, defaults
    to r".+".
    :type default: str, optional
    :return: The pattern that can match any combinations of the constraints.
    :rtype: str
    """

    pieces = []
    for step in CONFIG_DEFAULTS["levels"]:
        tmp = constraints.get(step, default)
        if isinstance(tmp, list):
            tmp = rf"({'|'.join(tmp)})"

        pieces.append(tmp)

    core = r"_".join(pieces)

    return rf"_?({core})_Feature"


def match_pattern(exp_dirname: str, pattern: str) -> bool:
    """Check whether the experiment directory name holds the pattern or not.

    :param exp_dirname: The name of the experiment's directory.
    :type exp_dirname: str
    :param pattern: The pattern to match.
    :type pattern: str
    :return: Whether the pattern has been matched or not.
    :rtype: bool
    """

    if re.search(pattern, exp_dirname):
        return True

    return False


def generate_exp_zip(methods_dict: Dict[str, List[str]], methods_ordering: List[str]) -> zip:
    """Generate the n-uple of experiments related the same image pair for each image pair.

    :param methods_dict: The collection of experiments lists per step method similarly ordered across all the
    methods.
    :type methods_dict: Dict[str, List[str]]
    :param methods_ordering: The order in which the methods should be displayed.
    :type methods_ordering: List[str]
    :return: The list of n-uples of experiments related the same image pair,
    :rtype: zip
    """

    return zip(*[methods_dict[method] for method in methods_ordering])


def get_pair_name(exp_path: str, separator: str = "_", to_remove: str = "") -> str:
    """Retrieve the pair name (<src-basename>`separator`<dst-basename>).

    :param exp_path: The path to a given experiment from which the pair name will be retrieved.
    :type exp_path: str
    :param separator: The separator used between the two basename, defaults to "_".
    :type separator: str, optional
    :param to_remove: The pattern to remove from the basename, defaults to "".
    :type to_remove: str, optional
    :return: The pair name (<src-basename>`separator`<dst-basename>).
    :rtype: str
    """

    def __get_basename(
        path: str, results: List[str] = [], level: int = 0, skip: Optional[List[int]] = None
    ) -> List[str]:
        if level == 0:
            if skip is not None and level in skip:
                return results

            results.append(os.path.basename(path))
            return results

        __get_basename(os.path.dirname(path), results, level - 1, skip)

        if skip is not None and level in skip:
            return results

        results.append(os.path.basename(path))
        return results

    src, dst = None, None
    with open(os.path.join(exp_path, "logs/logs_general.log"), "r", encoding="utf-8") as f:
        for line in f.readlines():
            if match := re.search(r"[a-z]{3} path: (.*)\n", line):
                tmp = "_".join(__get_basename(match.group(1), [], level=2, skip=[1])).replace(to_remove, "")
                if "src" in match.group(0):
                    src = tmp
                elif "dst" in match.group(0):
                    dst = tmp
                    break

    assert src is not None
    assert dst is not None
    return separator.join([src, dst])


def dummy(methods_ordering: List[str]) -> List[str]:
    return methods_ordering


def generate_exp_list_per_method(
    exps: List[str], methods_ordering: List[str], methods_to_pattern: Callable[[List[str]], List[str]] = dummy
) -> Dict[str, List[str]]:
    """Generate, per method, the experiments lists ordered similarly across all the methods.

    :param exps: The list of experiments to extend.
    :type exps: List[str]
    :param methods_ordering: The order in which the methods should be displayed.
    :type methods_ordering: List[str]
    :param methods_to_pattern: The callable that generate a list of patterns based on the list of methods,
    defaults to dummy.
    :type methods_to_pattern: Callable[[List[str]], List[str]]
    :return methods_dict: The collection of experiments lists per step method similarly ordered across all the
    methods.
    :rtype: Dict[str, List[str]]
    """

    methods_ordering_N_patterns = (methods_ordering, methods_to_pattern(methods_ordering))

    def _sort_dict_by_key(d: Dict[int, str]) -> Dict[int, str]:
        return dict(sorted(d.items(), key=lambda item: item[0]))

    methods_dict = defaultdict(dict)

    ref_order = {}
    ref_method = None

    method = None
    method_pattern = None
    for exp in sorted(exps):
        exp_basename = os.path.basename(exp)
        # Shortcut to avoid looping if same method as previous
        keep_going = False
        if not method or (method_pattern is not None and not match_pattern(exp_basename, method_pattern)):
            for m, p in zip(*methods_ordering_N_patterns):
                if match_pattern(exp_basename, p):
                    method = m
                    method_pattern = p
                    keep_going = True
                    break
        else:
            keep_going = True

        if not keep_going:
            continue

        if ref_method is None:
            ref_method = method

        key = get_pair_name(exp)

        if method == ref_method:
            index = len(ref_order)
            ref_order[key] = index
            methods_dict[method][index] = exp
        else:
            methods_dict[method][ref_order[key]] = exp

    return {method: list(_sort_dict_by_key(_exps).values()) for method, _exps in methods_dict.items()}


def get_all_experiments_list(experiments_dirpath: str) -> List[str]:
    """Get all the experiments in a list.

    :param experiments_dirpath: The path to the experiments folder.
    :type experiments_dirpath: str
    :return: The list of all the experiments contained within the provided experiments folder.
    :rtype: List[str]
    """

    exps = []
    for root, dirs, _ in os.walk(experiments_dirpath):
        for d in dirs:
            if "FeatureBasedExperiment" not in d:
                continue

            exps.append(os.path.join(root, d))

    return sorted(exps)


def get_all_labels_dict(label_dirpath: str) -> Dict[str, List[str]]:
    """Build the labels dictionary that contains the labels of each method for each step.

    :param label_dirpath: The path to the folder that contains all the .txt files which themselves contains the
    various methods' labels.
    :type label_dirpath: str
    :return: The dictionary which has for keys the different steps and as values the associated labels.
    :rtype: Dict[str, List[str]]
    """

    all_labels = {}
    for label_file in sorted(os.listdir(label_dirpath)):
        if not label_file.endswith(".txt"):
            continue

        with open(os.path.join(label_dirpath, label_file), "r", encoding="utf-8") as fp:
            all_labels[label_file.replace(".txt", "")] = fp.read().splitlines()

    return all_labels
