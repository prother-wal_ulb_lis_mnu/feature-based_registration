"""
Utils to extend the experiments.
"""

import argparse


def create_parser() -> argparse.ArgumentParser:
    """Create the arg parser.
    For addition information: https://docs.python.org/3/library/argparse.html

    :return parser: The arg parser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--run_path",
        type=str,
        required=True,
        help="The path to the root folder that contains everything related to a specific run.",
    )

    return parser
