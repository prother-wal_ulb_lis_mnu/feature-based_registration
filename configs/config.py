"""
This module is used to handle creating, updating, merging and loading configuration files.
"""

from __future__ import annotations

import itertools
import json
import os
from functools import reduce
from typing import Any, Dict, List, Optional, Tuple, TypeVar

from jinja2 import Template

StepArgs = TypeVar("StepArgs", str, Tuple[str, str, Dict[str, Any]])

CONFIG_DEFAULTS = {
    "distances": {
        "norm_l2": ["Random", "Neighborhood", "SIFT", "RootSIFT", "SURF", "VGG", "DAISY"],
        "norm_hamming": ["BRISK", "FREAK", "BRIEF", "ORB", "LATCH", "Boost"],
    },
    "preprocessing": {
        "Gray": {
            "mode": {"img": {"in": "single", "out": "single"}, "mask": {"in": None, "out": None}},
            "invert": False,
            "as_ubyte": True,
        }
    },
    "levels": ["preprocessings", "detectors", "descriptors", "matchers", "filters", "transforms"],
}


def get_template_from_file(template_path: str) -> Template:
    """Get a Template object from a template file.

    :param template_path: The path to the template.
    :type template_path: str
    :return: The Jinja2 loaded template.
    :rtype: Template
    """

    with open(template_path, "r", encoding="utf-8") as f:
        template = Template(f.read())

    return template


def get_dict_from_template(
    template: Template, step: str, defaults: Dict[str, Any] = CONFIG_DEFAULTS, **kwargs: Any
) -> Dict[str, Any]:
    """Get a dictionary from a template.

    :param template: A Jinja2 template to fill with values.
    :type template: Template
    :param step: The type of step in the feature matching pipeline, e.g."preprocessings" and "descriptors".
    :type step: str
    :param defaults: The default values that should be used, defaults to CONFIG_DEFAULTS.
    :type defaults: Dict[str, Any], optional
    :return: The dictionary of the filled template.
    :rtype: Dict[str, Any]
    """

    _dict = eval(template.render(**kwargs))
    label = kwargs.get("label", kwargs.get("method"))
    match step:
        case "preprocessings":
            _dict[label]["steps"] = kwargs.get("steps") if kwargs.get("steps") else defaults["preprocessing"]
            _dict[label]["overlap_inverted"] = True
            _dict[label]["pad_value"] = 1.0
        case "descriptors":
            distance = "norm_l2"
            for dist, descriptors in defaults["distances"].items():
                if kwargs.get("method") in descriptors:
                    distance = dist
                    break

            _dict[label]["matching_distance"] = distance
        case elem if elem in ("detectors", "matchers", "filters", "transforms"):
            pass

    return _dict


def generate_config_files(filename: str, elements: Dict[str, Any]) -> None:
    """Generate a pair (only if the `analysis` key is provided in elements) of configuration file skeletons
    from a pseudo representation.

    :param filename: The name to give to the configuration file.
    :type filename: str
    :param elements: The pseudo representation. This should contain the following keys:
    * ``levels`` followed by the a list of the modular steps from the point of view of the traditional pipeline
    (``["preprocessing", "detector", "descriptor", "matcher", "filter", "transform"]``). Those can be re-ordered
    to change the results architecture.
    * ``preprocessings`` followed by a list of the pre-processing methods to include.
    * ``pipelines`` followed by a dictionary containing this structure
    ``{"PIPE_LABELS": {"PIPE_STEPS": ["STEP_METHODS"]}}}``.
    * [Optionally] ``shared`` followed by a list of dictionaries containing this structure
    ``{"pipelines": ["PIPE_LABELS"], "PIPE_STEPS": ["STEP_METHODS"]}}}``. Note that it is imperative that
    ``pipelines`` is one of the provided key in shared and that the other keys-values are similar to what
    you would put in under each ``pipelines.PIPE_LABEL``.
    * [Optionally] ``analysis`` followed by a dictionary which keys are the labels of the analysis to perform and values
    the type of analysis it is and the constraints to select the experiments. Additionally, one can use `not_<step>`, in
    the constraints, to specify that all the methods should be taken except those specified by there. The presence of
    this `analysis` key is mandatory to create the second configuration file skeleton dedicated to the analysis.

    All methods can either be: (i) a simple str, (ii) a tuple of two str ``("METHOD_LABEL", "ACTUAL_METHOD")``
    or (iii) a tuple of two str and a dict containing the kwargs ``("METHOD_LABEL", "ACTUAL_METHOD", {kwargs})``.
    For preprocessing methods, it is either (i) a simple str or (ii) a tuple of a str and a dict containing the
    steps ``("METHOD_LABEL", {steps})`` where a step is key (ACTUAL_METHOD) and value its kwargs.

    Here is a complete example which will be saved as follows
    `.../Gray/<detector>/<descriptor>/<matcher>/affRANSAC/AffineTransform/...`:
    ``{
        "levels": ["preprocessings", "detectors", "descriptors", "matchers", "filters", "transforms"],
        "preprocessings": [
            (
                "Gray",
                {
                    "Gray": {
                        "mode": {"img": {"in": "single", "out": "single"}, "mask": {"in": None, "out": None}},
                        "invert": False,
                        "as_ubyte": True,
                    },
                },
            ),
        ],
        "pipelines": {
            "Traditional": {
                "method": "Traditional",
                "detectors": ["BRISK"],
                "descriptors": ["BRIEF", ("O-BRIEF", "BRIEF", {"use_orientation": True})],
            },
            "SecondPipeline": {"method": "SecondPipeline", "detectNdescribe": ["detectNdescribe"]},
            "SomeOtherPipeline": {{"method": "SomeOtherPipeline", "detectNdescribeNmatch": ["detectNdescribeNmatch"]},
        },
        "shared": [
            {
                "pipelines": ["Traditional", "SecondPipeline"],
                "matchers": [("BruteForce", "BruteForce", {"cross_check": True})],
            },
            {
                "pipelines": ["Traditional", "SecondPipeline", "SomeOtherPipeline"],
                "filters": [("affRANSAC", "affRANSAC", {"ransacReprojThreshold": 7})],
                "transforms": ["skAffine"],
            },
        ],
        "analysis": {
            "generic": {
                "type": "generic",
                "preprocessings": ["Gray"],
                "detectors": ["BRISK"],
                "not_descriptors": ["detectNdescribe", "detectNdescribeNmatch"],
                "matchers": ["BruteForce"],
                "filters": ["affRANSAC"],
                "transforms": ["skAffine"],
            },
            "descriptors": {
                "type": "descriptors",
                "preprocessings": ["Gray"],
                "detectors": ["BRISK"],
                "descriptors": ["O-BRIEF", "BRIEF"],
                "matchers": ["BruteForce"],
                "filters": ["affRANSAC"],
                "transforms": ["skAffine"],
            },
        },
    }

    :type elements: Dict[str, Any]
    """

    def __to_kwargs(value: Any, keys: List[str] = ["label", "method", "kwargs"]) -> Dict[str, Any]:
        """Change str or tuple to kwargs."""

        if isinstance(value, tuple) or isinstance(value, list):
            _kwargs = {}
            for i, v in enumerate(value):
                _kwargs[keys[i]] = v
            return _kwargs
        elif isinstance(value, dict):
            return value
        else:
            return {"method": value}

    def __treat_step_args(
        step_template: Template, step: str, raw: List[StepArgs], keys: List[str] = ["label", "method", "kwargs"]
    ) -> Dict[str, Dict[str, Any]]:
        """Process step args to get a dictionary based on a given template."""

        methods = {}

        for method in raw:
            kwargs = __to_kwargs(method, keys)
            methods.update(get_dict_from_template(step_template, step=step, **kwargs))

        return methods

    run_config = {}
    analysis_config = {}
    pre_processing_template = get_template_from_file(os.path.join("configs", "templates", "preprocessing.json.jinja2"))
    template = get_template_from_file(os.path.join("configs", "templates", "method.json.jinja2"))

    for key, value in elements.items():
        if key == "preprocessings":
            run_config[key] = __treat_step_args(pre_processing_template, key, value, keys=["method", "steps"])
        elif key == "pipelines":
            run_config[key] = {}
            for pipeline_label, pipeline_args in value.items():
                pipeline_method = pipeline_args.pop("method")
                pipeline_methods = {"method": pipeline_method}
                for step, step_methods in pipeline_args.items():
                    pipeline_methods[step] = __treat_step_args(template, step, step_methods)

                run_config[key][pipeline_label] = pipeline_methods
        elif key == "shared":
            for share in value:
                try:
                    pipelines = share.pop("pipelines")
                except KeyError as e:
                    raise KeyError(
                        "Either `shared` was supplied before `pipelines` or no pipelines were specified in "
                        "elements.shared."
                    ) from e

                for p in pipelines:
                    for step, step_methods in share.items():
                        current_methods = run_config["pipelines"][p].get(step, {})
                        current_methods.update(__treat_step_args(template, step, step_methods))
                        run_config["pipelines"][p][step] = current_methods

        elif key == "analysis":
            analysis_config = value
        else:
            run_config[key] = value

    # Write config file
    for config, subfolder in zip((run_config, analysis_config), ("runs", "analysis")):
        with open(os.path.join("configs", subfolder, filename), "w", encoding="utf-8") as fp:
            json.dump(config, fp)


class ExperimentConfig:
    """A class that stores the specifications of an experiment."""

    def __init__(self, rootpath: str, pipeline: str, **kwargs: Any) -> None:
        """ExperimentConfig's constructor.

        :param rootpath: The root folder for all the experiments.
        :type rootpath: str
        :param pipeline: The concerned pipeline.
        :type pipeline: str

        Note that additional attributes from the kwargs are set without being explicitly specified to account for
        dynamic behavior.
        """

        self.rootpath = rootpath
        self.pipeline = pipeline

        for key, value in kwargs.items():
            self.__dict__[key] = value

        kwargs.update({"rootpath": rootpath, "pipeline": pipeline})

        self.__dict__["_config_dict"] = kwargs

        # For `outpath` property
        self._outpath: Optional[str] = None

    def __eq__(self, other: ExperimentConfig) -> bool:
        if type(other) is type(self):
            return {
                key: value for key, value in self.__dict__.items() if key not in ("pipeline_label", "_config_dict")
            } == {key: value for key, value in other.__dict__.items() if key not in ("pipeline_label", "_config_dict")}

        return False

    @classmethod
    def extract_label(cls, step: Dict[str, Any]) -> str:
        """Extract the label of the given step. This label is primarily used to distinguish a method from another one.

        :param step: The dictionary of the step got from the configuration dictionary.
        :type step: Dict[str, Any]
        :return: The label of the given step.
        :rtype: str
        """

        return list(step.keys())[0]

    @classmethod
    def extract_label_method_N_kwargs(cls, step: Dict[str, Any]) -> Tuple[str, str, Dict[str, Any]]:
        """Extract the name of the method that should be present in one of the `AVAILABLE_` dictionaries, the label it
        will go by in the results and its kwargs.

        :param step: The dictionary of the step got from the configuration dictionary.
        :type step: Dict[str, Any]
        :return method: The label that will be used to identify the combination `method(**kwargs)`.
        :rtype method: str
        :return method: The name of the method.
        :rtype method: str
        :return kwargs: The kwargs associated to the method.
        :rtype kwargs: Dict[str, Any]
        """

        label = cls.extract_label(step)
        method = step[label].get("method")
        kwargs = step[label].get("kwargs")

        return label, method, kwargs

    @classmethod
    def extract_preprocessing_label_steps_N_kwargs(
        cls, preprocessing: Dict[str, Any]
    ) -> Tuple[str, List[Tuple[str, Dict[str, Any]]], Dict[str, Any]]:
        """Extract the steps `[(a_method, a_kwargs), (b_method, b_kwargs), ...]` which together build the
        pre-processing pipeline. Each method in the steps should be present in the `AVAILABLE_PREPROCESSING`
        dictionaries. The class method also extract the pre-processing pipeline label and its global kwargs.

        :param preprocessing: The dictionary of the step got from the configuration dictionary.
        :type preprocessing: Dict[str, Any]
        :return label: The label that will be used to identify the pre-processing pipeline.
        :rtype label: str
        :return steps: The steps of the pre-processing pipeline.
        :rtype steps: List[Tuple[str, Dict[str, Any]]]
        :return global_kwargs: The kwargs associated to the method.
        :rtype global_kwargs: Dict[str, Any]
        """

        label = cls.extract_label(preprocessing)
        global_kwargs = {}
        for key, value in preprocessing[label].items():
            if key == "steps":
                steps = [(method, kwargs) for method, kwargs in value.items()]
            else:
                global_kwargs[key] = value

        return label, steps, global_kwargs

    @property
    def outpath(self) -> Optional[str]:
        return self._outpath

    def add_attributes_from_dict(self, to_add: Dict[str, Any]) -> None:
        """Add attributes to the object based off of a dictionary.

        :param to_add: The dictionary that contains the new attributes (keys) and their values (values).
        :type to_add: Dict[str, Any]
        """

        for attribute, value in to_add.items():
            self.__dict__[attribute] = value


class Config:
    def __init__(self, run_config_file: str, analysis_config_file: Optional[str] = None) -> None:
        """Config's constructor.

        :param run_config_file: The path or the name of the configuration file that will be used for running all
        the experiments. If a name is provided, the corresponding configuration file is found by scanning the
        config folder.
        :type run_config_file: str
        :param analysis_config_file: The path or the name of the configuration file that will be used for
        analyzing the run. If a name is provided, the corresponding configuration file is found by scanning the
        config folder. It defaults to None.
        :type analysis_config_file: Optional[str], optional
        :raises FileNotFoundError: Triggered if the file name does not exist in the configuration directory.
        """

        def _get_path(config_filename: str, subfolder: str) -> str:
            config_dir = os.path.dirname(os.path.abspath(__file__))
            config_filepath = os.path.join(config_dir, subfolder, config_filename)
            if not os.path.exists(config_filepath):
                raise FileNotFoundError(
                    f"The configuration file, named {config_filename=}, does not exists in "
                    f"{os.path.join(config_dir, subfolder)}."
                )

            return config_filepath

        def _get_config_content(config_filepath: str) -> Dict[str, Any]:
            if not os.path.exists(config_filepath):
                raise FileNotFoundError(f"The configuration file, {config_filepath=}, does not exists.")

            with open(config_filepath, "r", encoding="utf-8") as fp:
                config = json.load(fp)

            return config

        self.run_config_filepath: str = ""
        self.run_config: Dict[str, Any] = {}
        self.analysis_config_filepath: Optional[str] = None
        self.analysis_config: Optional[Dict[str, Any]] = None

        for config_filepath, config_type in zip(
            (run_config_file, analysis_config_file),
            ("runs", "analysis"),
        ):
            if config_filepath is None:
                continue

            if os.sep not in config_filepath:
                config_filepath = _get_path(config_filepath, config_type)

            if config := _get_config_content(config_filepath):
                self.__dict__[f"{'run' if config_type == 'runs' else config_type}_config_filepath"] = config_filepath
                self.__dict__[f"{'run' if config_type == 'runs' else config_type}_config"] = config

        self._rootpath: Optional[str] = None
        self._labels_dir: Optional[str] = None

    @property
    def rootpath(self) -> Optional[str]:
        return self._rootpath

    @rootpath.setter
    def rootpath(self, value: str) -> None:
        self._rootpath = value

    @property
    def labels_dir(self) -> Optional[str]:
        return self._labels_dir

    @labels_dir.setter
    def labels_dir(self, value: str) -> None:
        self._labels_dir = value

    @property
    def levels(self) -> List[str]:
        return self.run_config.get("levels", CONFIG_DEFAULTS["levels"])

    @property
    def analysis(self) -> Dict[str, Dict]:
        assert self.analysis_config is not None

        return self.analysis_config

    @property
    def experiments_path(self) -> str:
        assert self.rootpath is not None

        return os.path.join(self.rootpath, "experiments")

    def _deep_get(self, dictionary, keys, default=None):
        return reduce(
            lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary
        )

    def get_target_step_methods_ordering(self, step: str) -> Dict[str, int]:
        if step == "preprocessings":
            return {label: index for index, label in enumerate(self.run_config.get("preprocessings", ()))}

        methods_ordering = {}
        index = 0
        for pipeline in self.run_config.get("pipelines", ()):
            for label in self.run_config["pipelines"][pipeline].get(step, []):
                if methods_ordering.get(label, None) is not None:
                    continue

                methods_ordering[label] = index
                index += 1

        return methods_ordering

    @property
    def grid_search(self) -> List[ExperimentConfig]:
        """The unrolled list of all the combinations to compare.

        :return: The unrolled list of all the combinations to compare.
        :rtype: List[ExperimentConfig]
        """

        grid_search = []
        for pipeline_label, _pipeline_specs in self.run_config.get("pipelines", {}).items():
            # Copy to avoid removing the `method` key in the 'original' pipeline_specs
            pipeline_specs = _pipeline_specs.copy()
            pipeline = pipeline_specs.pop("method")

            steps_complete_list = [
                [
                    {"preprocessing": {label: method}}
                    for label, method in self.run_config.get("preprocessings", {}).items()
                ]
            ]
            for step, step_specs in pipeline_specs.items():
                steps_complete_list.append([{step[:-1]: {label: method}} for label, method in step_specs.items()])

            combinations = itertools.product(*steps_complete_list)

            for combi in combinations:
                exp_specs = {
                    "rootpath": self.experiments_path,
                    "pipeline_label": pipeline_label,
                    "pipeline": pipeline,
                }
                for elem in combi:
                    exp_specs.update(elem)

                grid_search.append(ExperimentConfig(**exp_specs))

        return grid_search


if __name__ == "__main__":

    # Generate config file
    generate_config_files(
        filename="detectors_descriptors.json",
        elements={
            "levels": ["preprocessings", "detectors", "descriptors", "matchers", "filters", "transforms"],
            "preprocessings": [
                (
                    "Gray",
                    {
                        "Gray": {
                            "mode": {"img": {"in": "single", "out": "single"}, "mask": {"in": None, "out": None}},
                            "invert": False,
                            "as_ubyte": True,
                        },
                    },
                ),
            ],
            "pipelines": {
                "BRISK": {
                    "method": "Traditional",
                    "detectors": ["BRISK"],
                },
                "FAST": {
                    "method": "Traditional",
                    "detectors": ["FAST"],
                },
                "SIFT": {
                    "method": "Traditional",
                    "detectors": ["SIFT"],
                },
                "SURF": {
                    "method": "Traditional",
                    "detectors": ["SURF"],
                },
                "ORB": {
                    "method": "Traditional",
                    "detectors": ["ORB"],
                },
            },
            "shared": [
                {
                    "pipelines": ["BRISK", "FAST", "SIFT", "SURF", "ORB"],
                    "descriptors": [
                        "Random",
                        "Neighborhood",
                        "SURF",
                        "BRISK",
                        "BRIEF",
                        ("O-BRIEF", "BRIEF", {"use_orientation": True}),
                        ("VGG", "VGG", {"scale_factor": 5.0}),
                        # "DAISY",
                        # "LATCH",
                        # "Boost",
                    ],
                    "matchers": [("BruteForce", "BruteForce", {"cross_check": True})],
                    "filters": [("affRANSAC", "affRANSAC", {"ransacReprojThreshold": 7})],
                    "transforms": ["skAffine"],
                },
                {
                    "pipelines": ["BRISK", "FAST", "SIFT", "SURF"],
                    "descriptors": [
                        "SIFT",
                        "FREAK",
                    ],
                },
                {
                    "pipelines": ["FAST", "SIFT", "SURF"],
                    "descriptors": [
                        "RootSIFT",
                    ],
                },
                {
                    "pipelines": ["BRISK", "FAST", "SURF", "ORB"],
                    "descriptors": [
                        "ORB",
                    ],
                },
            ],
            "analysis": {
                "SIFT": {
                    "type": "generic",
                    "preprocessings": ["Gray"],
                    "detectors": ["SIFT"],
                    "not_descriptors": ["ORB"],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "BRISK": {
                    "type": "generic",
                    "preprocessings": ["Gray"],
                    "detectors": ["BRISK"],
                    "not_descriptors": ["RootSIFT"],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "FAST": {
                    "type": "generic",
                    "preprocessings": ["Gray"],
                    "detectors": ["FAST"],
                    "not_descriptors": [],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "SURF": {
                    "type": "generic",
                    "preprocessings": ["Gray"],
                    "detectors": ["SURF"],
                    "not_descriptors": [],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "ORB": {
                    "type": "generic",
                    "preprocessings": ["Gray"],
                    "detectors": ["ORB"],
                    "not_descriptors": ["FREAK", "SIFT", "RootSIFT"],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "descriptors": {
                    "type": "descriptors",
                    "preprocessings": ["Gray"],
                    "detectors": ["SURF"],
                    "not_descriptors": [],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
                "detectors_descriptors": {
                    "type": "detectors_descriptors",
                    "preprocessings": ["Gray"],
                    "not_detectors": [],
                    "not_descriptors": [],
                    "matchers": ["BruteForce"],
                    "filters": ["affRANSAC"],
                    "transforms": ["skAffine"],
                },
            },
        },
    )

    # generate_config_files(
    #     filename="sipaim.json",
    #     elements={
    #         "levels": ["preprocessings", "detectors", "descriptors", "matchers", "filters", "transforms"],
    #         "preprocessings": [
    #             (
    #                 "Gray",
    #                 {
    #                     "Gray": {
    #                         "mode": {"img": {"in": "single", "out": "single"}, "mask": {"in": None, "out": None}},
    #                         "invert": False,
    #                         "as_ubyte": True,
    #                     },
    #                 },
    #             ),
    #         ],
    #         "pipelines": {
    #             "Traditional": {
    #                 "method": "Traditional",
    #                 "detectors": ["SIFT"],
    #                 "descriptors": [
    #                     "Random",
    #                     "Neighborhood",
    #                     "SIFT",
    #                     "BRISK",
    #                     "FREAK",
    #                     "BRIEF",
    #                     ("VGG", "VGG", {"scale_factor": 5.0}),
    #                 ],
    #             },
    #         },
    #         "shared": [
    #             {
    #                 "pipelines": ["Traditional"],
    #                 "matchers": [("BruteForce", "BruteForce", {"cross_check": True})],
    #                 "filters": [("affRANSAC", "affRANSAC", {"ransacReprojThreshold": 7})],
    #                 "transforms": ["skAffine"],
    #             },
    #         ],
    #         "analysis": {
    #             "generic": {
    #                 "type": "generic",
    #                 "preprocessings": ["Gray"],
    #                 "detectors": ["SIFT"],
    #                 "not_descriptors": [],
    #                 "matchers": ["BruteForce"],
    #                 "filters": ["affRANSAC"],
    #                 "transforms": ["skAffine"],
    #             },
    #             "descriptors": {
    #                 "type": "descriptors",
    #                 "preprocessings": ["Gray"],
    #                 "detectors": ["SIFT"],
    #                 "not_descriptors": [],
    #                 "matchers": ["BruteForce"],
    #                 "filters": ["affRANSAC"],
    #                 "transforms": ["skAffine"],
    #             },
    #         },
    #     },
    # )
