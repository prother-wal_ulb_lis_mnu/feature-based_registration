# Setting global ARGs enable to access their values in any of the latter stage (cf. https://github.com/moby/moby/issues/38379#issuecomment-447835596)

# For a base image with cuda (11.7.1), nvcc (devel) and cuDNN (cudnn8) use the following base image: nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04
ARG IMAGE=ubuntu:jammy

ARG USERNAME=feature
ARG USER_UID=1000
ARG USER_GID=$USER_UID

FROM $IMAGE

LABEL maintainer="Arthur Elskens <arthur.elskens@ulb.be>"
LABEL description="The Dockerfile to build an image used to run the feature-based registration pipeline."

ARG USERNAME
ARG USER_UID
ARG USER_GID

# Avoid question/dialog when apt-get install
ENV DEBIAN_FRONTEND=noninteractive

# Create user that correspond to the user on the OS
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
	&& usermod --shell /bin/bash $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
ENV USER_HOME="/home/${USERNAME}"

# Fix ownership issue with the home directory
RUN sudo chown -R ${USERNAME} ${USER_HOME}

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Get build dependencies
RUN sudo apt-get update \
	&& sudo apt-get install -y \
		build-essential \
		software-properties-common \
	    python-is-python3 \
        python3.10-venv \
        python3-pip \
        git \
		wget \
        cmake \
		ca-certificates

# Create and activate a venv
ENV VIRTUAL_ENV="${USER_HOME}/opt/feature"
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ARG OPENCV=false
ARG VFC=false

# Build OpenCV from source with Python bindings
RUN if [ "${OPENCV}" = "true" ] || [ "${VFC}" = "true" ] ; \
	then mkdir ~/opencv_build && cd ~/opencv_build \
	&& git clone https://github.com/opencv/opencv.git \
	&& git clone https://github.com/opencv/opencv_contrib.git \
	&& cd ~/opencv_build/opencv \
	&& mkdir -p build && cd build \
	# Requirement to build from source
	&& pip install numpy \
	&& cmake -D CMAKE_BUILD_TYPE=RELEASE \
		-D CMAKE_INSTALL_PREFIX=/usr/local \
		-D INSTALL_PYTHON_EXAMPLES=OFF \
		-D INSTALL_C_EXAMPLES=OFF \
		-D WITH_TBB=ON \
		-D WITH_CUDA=OFF \
		-D WITH_V4L=OFF \
		-D WITH_QT=OFF \
		-D WITH_OPENGL=OFF \
		-D WITH_GSTREAMER=OFF \
		-D OPENCV_GENERATE_PKGCONFIG=ON \
		-D OPENCV_ENABLE_NONFREE=ON \
		-D INSTALL_CREATE_DISTRIB=ON \
		# To avoid using a single world.so
		-D BUILD_opencv_world=OFF \
		-D BUILD_opencv_python2=OFF \
		-D BUILD_opencv_python3=ON \
		-D PYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
		-D PYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
		-D OPENCV_EXTRA_MODULES_PATH=~/opencv_build/opencv_contrib/modules \
		-D PYTHON3_EXECUTABLE=$(which python3) \
		-D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
		-D BUILD_EXAMPLES=OFF .. \
	&& make -j8 \
	&& sudo make install \
	&& sudo ldconfig ; \
	fi

# Fix permissions of OpenCV's `cv2` package in the venv
RUN sudo chown ${USERNAME}:${USERNAME} -R "$(python3 -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())')/cv2"

# Install VFC
RUN if [ ${VFC} = true ] ; \
	then cd ${WKDIR} \
	&& git clone https://github.com/aelskens/pyvfc.git \
	&& pip install ./pyvfc ; \
	fi

ARG WKDIR="${USER_HOME}/src"
WORKDIR $WKDIR

# Install the repository at WORKDIR location
COPY . .

# Install all extra package for this project
RUN pip install -r requirements.txt

ENV PYTHONPATH="${WKDIR}:${WKDIR}/src/feature-based_registration"