from importlib.metadata import PackageNotFoundError, version

try:
    __version__ = version("{{ cookiecutter.__package_name }}")
except PackageNotFoundError:
    __version__ = "dev"
