"""
The FeatureBasedExperiment class is defined in this module. This class is used to apply a complete feature-based
registration pipeline (with modular choice for each steps) and generate the registration outputs useful for
analyzing the results.
"""

from copy import deepcopy
from dataclasses import dataclass, field
from logging import Logger
from typing import Any, Callable, Dict, List, Optional, Tuple, Type

import cv2
import numpy as np
from helpers.logger import ExperimentLogger
from helpers.outputs import (
    OutputsHelper,
    pickle_object,
    save_array,
    save_config,
    save_exec_time,
    save_img,
    save_metrics,
    save_points,
)
from matplotlib import colors
from metrics.registration import rmse, tre
from openwholeslide import (
    FloatVector,
    PaddingParameters,
    SlideRegion,
    VectorType,
    WholeSlide,
)
from processing.chaining import ChainingPreProcessing
from processing.image import ProcessedImage
from processing.transform import Warping2D
from registration.feature_matching import ModularFeatureMatching, dummy
from skimage import img_as_ubyte
from skimage.transform import EuclideanTransform
from viz.colormaps import generate_cmap
from viz.wsi import (
    draw_init_warped_target,
    draw_matches,
    draw_overlap_img,
    generate_standalone_figure,
)

from configs.config import ExperimentConfig


@dataclass
class FeatureBasedExperiment:
    """A class that allows to run modular experiments for feature-based registration."""

    src_wsi: WholeSlide
    dst_wsi: WholeSlide
    outputs_helper: OutputsHelper

    resolution: float = 10.0
    src_gt_path: Optional[str] = None
    dst_gt_path: Optional[str] = None

    config: ExperimentConfig = ExperimentConfig("", "")
    preprocessing_cls: Type[ChainingPreProcessing] = ChainingPreProcessing
    preprocessing_kwargs: Dict[str, Any] = field(default_factory=lambda: {"links": []})
    feature_matching_pipeline: ModularFeatureMatching = field(repr=False, default=dummy)

    __logger: Logger | ExperimentLogger = field(
        init=False, repr=False, default=ExperimentLogger.get_logger(setup_logger=False)
    )

    __src_region: Optional[SlideRegion] = field(init=False, repr=False, default=None)
    __dst_region: Optional[SlideRegion] = field(init=False, repr=False, default=None)
    __preprocessor: Optional[ChainingPreProcessing] = field(init=False, repr=False, default=None)
    __src_pre_process: Optional[ProcessedImage] = field(init=False, repr=False, default=None)
    __dst_pre_process: Optional[ProcessedImage] = field(init=False, repr=False, default=None)
    __warp2D: Optional[Warping2D] = field(init=False, repr=False, default=None)
    __viz_warp2D: Optional[Warping2D] = field(init=False, repr=False, default=None)

    __exec_times: Dict[str, float] = field(init=False, repr=False, default_factory=dict)
    __eval_results: Optional[Dict[str, Any]] = field(init=False, repr=False, default=None)

    def __post_init__(self) -> None:
        self.__logger = ExperimentLogger.get_logger(
            to_handle=self.outputs_helper.get_levels_for_log(), output_dir=self.outputs_helper.dir_path
        )

        self.outputs_helper.generate_outputs(
            level_flag="LOGFILE",
            level_dir="logs",
            prefix="logs",
            suffix="config.json",
            save_method=save_config,
            save_method_kwargs={"config": self.__dict__.copy()},
        )

    @property
    def dir_path(self) -> Optional[str]:
        return self.outputs_helper.dir_path

    def __load_image(self, ws: WholeSlide) -> SlideRegion:
        return ws.read_full(resolution=self.resolution)

    def __preprocess(self, src_img: np.ndarray, dst_img: np.ndarray) -> Tuple[ProcessedImage, ProcessedImage]:
        self.__preprocessor = self.preprocessing_cls(**self.preprocessing_kwargs)
        return self.__preprocessor(src_img, dst_img)

    def execute(self) -> None:
        """Execute the experiment with the given config. If the output level has been set, then
        outputs are generated accordingly.
        """

        self.__logger.info(f"src path: {self.src_wsi.path}")
        self.__logger.info(f"dst path: {self.dst_wsi.path}")

        # Load the region from the WSI
        self.__src_region = self.__load_image(self.src_wsi)
        self.__dst_region = self.__load_image(self.dst_wsi)

        # Preprocess the WSI
        (
            self.__src_pre_process,
            self.__dst_pre_process,
        ) = self.__preprocess(self.__src_region.as_ndarray, self.__dst_region.as_ndarray)
        assert self.__preprocessor is not None
        assert self.__preprocessor.exec_time is not None
        self.__exec_times["pre-processing"] = self.__preprocessor.exec_time

        # INIT outputs
        self.outputs_helper.generate_outputs(
            level_flag="INIT",
            level_dir="init",
            prefix="init",
            suffix="src.png",
            save_method=save_img,
            save_method_kwargs={"img": img_as_ubyte(self.__src_pre_process.initial)},
        )
        self.outputs_helper.generate_outputs(
            level_flag="INIT",
            level_dir="init",
            prefix="init",
            suffix="dst.png",
            save_method=save_img,
            save_method_kwargs={"img": img_as_ubyte(self.__dst_pre_process.initial)},
        )

        # PREPROC outputs
        self.outputs_helper.generate_outputs(
            level_flag="PREPROC",
            level_dir="preproc",
            prefix="preproc",
            suffix="src.png",
            save_method=save_img,
            save_method_kwargs={"img": self.__src_pre_process.preprocessed},
        )
        self.outputs_helper.generate_outputs(
            level_flag="PREPROC",
            level_dir="preproc",
            prefix="mask",
            suffix="src.npy",
            save_method=save_array,
            save_method_kwargs={"array": self.__src_pre_process.mask},
        )
        self.outputs_helper.generate_outputs(
            level_flag="PREPROC",
            level_dir="preproc",
            prefix="preproc",
            suffix="dst.png",
            save_method=save_img,
            save_method_kwargs={"img": self.__dst_pre_process.preprocessed},
        )
        self.outputs_helper.generate_outputs(
            level_flag="PREPROC",
            level_dir="preproc",
            prefix="mask",
            suffix="dst.npy",
            save_method=save_array,
            save_method_kwargs={"array": self.__dst_pre_process.mask},
        )

        # Apply feature matching pipeline
        self.feature_matching_pipeline(src_img=self.__src_pre_process, dst_img=self.__dst_pre_process)
        self.__exec_times.update(self.feature_matching_pipeline.exec_times)

        # FEATURES outputs and store execution time
        self.outputs_helper.generate_outputs(
            level_flag="FEATURES",
            level_dir="features",
            prefix="raw",
            suffix="src.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.src_keypoints_xy,
                "descriptors": self.feature_matching_pipeline.src_keypoints_desc,
            },
        )
        self.outputs_helper.generate_outputs(
            level_flag="FEATURES",
            level_dir="features",
            prefix="raw",
            suffix="dst.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.dst_keypoints_xy,
                "descriptors": self.feature_matching_pipeline.dst_keypoints_desc,
            },
        )

        # MATCHED outputs
        self.outputs_helper.generate_outputs(
            level_flag="MATCHED",
            level_dir="features",
            prefix="matched",
            suffix="src.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.matched_src_keypoints_xy,
            },
        )
        self.outputs_helper.generate_outputs(
            level_flag="MATCHED",
            level_dir="features",
            prefix="matched",
            suffix="dst.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.matched_dst_keypoints_xy,
            },
        )

        # FILTERED outputs
        self.outputs_helper.generate_outputs(
            level_flag="FILTERED",
            level_dir="features",
            prefix="filtered",
            suffix="src.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.filtered_matched_src_keypoints_xy,
            },
        )
        self.outputs_helper.generate_outputs(
            level_flag="FILTERED",
            level_dir="features",
            prefix="filtered",
            suffix="dst.csv",
            save_method=save_points,
            save_method_kwargs={
                "coords": self.feature_matching_pipeline.filtered_matched_dst_keypoints_xy,
            },
        )

        # Instantiate 2D warping method
        self.__warp2D = Warping2D(self.feature_matching_pipeline.robust_transform, resolution=self.resolution)
        # Adapt transform to take the padding into account (visualization purposes only)
        viz_robust_transform = deepcopy(self.feature_matching_pipeline.robust_transform)
        viz_robust_transform.params = (
            EuclideanTransform(translation=self.__dst_pre_process.offset.xy).params
            @ self.feature_matching_pipeline.robust_transform.params
        )
        self.__viz_warp2D = Warping2D(viz_robust_transform, resolution=self.resolution)

        # OVERLAP outputs
        assert self.__dst_pre_process.padded_preprocessed is not None
        assert self.__src_pre_process.padded_preprocessed is not None
        self.outputs_helper.generate_outputs(
            level_flag="OVERLAP",
            level_dir="overlap",
            prefix="overlap",
            suffix="init.png",
            save_method=save_img,
            save_method_kwargs={
                "img": draw_overlap_img(
                    [self.__dst_pre_process.padded_preprocessed, self.__src_pre_process.padded_preprocessed],
                    inverted=getattr(self.config, "overlap_inverted", True),
                )
            },
        )
        assert self.__src_pre_process.preprocessed is not None
        assert self.__dst_pre_process.padded_preprocessed is not None
        self.outputs_helper.generate_outputs(
            level_flag="OVERLAP",
            level_dir="overlap",
            prefix="overlap",
            suffix="warped.png",
            save_method=save_img,
            save_method_kwargs={
                "img": draw_overlap_img(
                    [
                        self.__dst_pre_process.padded_preprocessed,
                        self.__viz_warp2D(
                            self.__src_pre_process.preprocessed,
                            order=3,
                            cval=1.0,
                            output_shape=self.__dst_pre_process.padded_preprocessed.shape,
                        ),
                    ],
                    inverted=getattr(self.config, "overlap_inverted", True),
                )
            },
        )

        # Add execution time of the wrap method
        assert self.__viz_warp2D.exec_time is not None
        self.__exec_times["warping"] = self.__viz_warp2D.exec_time

        # LOGFILE outputs
        self.outputs_helper.generate_outputs(
            level_flag="LOGFILE",
            level_dir="logs",
            prefix="logs",
            suffix="exec_times.csv",
            save_method=save_exec_time,
            save_method_kwargs={"times": self.__exec_times},
        )

        # REGPARAMS outputs
        self.outputs_helper.generate_outputs(
            level_flag="REGPARAMS",
            level_dir="regparams",
            prefix="raw",
            suffix="warp2D.pickle",
            save_method=pickle_object,
            save_method_kwargs={"object_callable": self.warp2D},
        )

    @property
    def src_img_process(self) -> Optional[ProcessedImage]:
        """The source processed image object.

        :return: The source processed image object.
        :rtype: Optional[ProcessedImage]
        """

        if not self.__src_pre_process:
            return self.__src_pre_process

        return self.__src_pre_process

    @property
    def dst_img_process(self) -> Optional[ProcessedImage]:
        """The destination processed image object.

        :return: The destination processed image object.
        :rtype: Optional[ProcessedImage]
        """

        if not self.__dst_pre_process:
            return self.__dst_pre_process

        return self.__dst_pre_process

    @property
    def src_img(self) -> Optional[np.ndarray]:
        """The source image (RGB).

        :return: The source image (RGB).
        :rtype: Optional[np.ndarray]
        """

        if not self.__src_pre_process:
            return self.__src_pre_process

        return self.__src_pre_process.initial

    @property
    def dst_img(self) -> Optional[np.ndarray]:
        """The destination image (RGB).

        :return: The destination image (RGB).
        :rtype: Optional[np.ndarray]
        """

        if not self.__dst_pre_process:
            return self.__dst_pre_process

        return self.__dst_pre_process.initial

    @property
    def src_pre_process(self) -> Optional[np.ndarray]:
        """The pre-processed source image.

        :return: The pre-processed source image.
        :rtype: Optional[np.ndarray]
        """

        if not self.__src_pre_process:
            return self.__src_pre_process

        return self.__src_pre_process.preprocessed

    @property
    def dst_pre_process(self) -> Optional[np.ndarray]:
        """The pre-processed destination image.

        :return: The pre-processed destination image.
        :rtype: Optional[np.ndarray]
        """

        if not self.__dst_pre_process:
            return self.__dst_pre_process

        return self.__dst_pre_process.preprocessed

    @property
    def src_padder(self) -> Optional[PaddingParameters]:
        """The padder associated to the source image.

        :return: The source image's padder.
        :rtype: Optional[PaddingParameters]
        """

        if not self.__src_pre_process:
            return self.__src_pre_process

        return self.__src_pre_process.padder

    @property
    def dst_padder(self) -> Optional[PaddingParameters]:
        """The padder associated to the destination image.

        :return: The destination image's padder.
        :rtype: Optional[PaddingParameters]
        """

        if not self.__dst_pre_process:
            return self.__dst_pre_process

        return self.__dst_pre_process.padder

    @property
    def src_keypoints_xy(self) -> np.ndarray:
        """The source image's keypoints coordinates.

        :return: The source image's keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.src_keypoints_xy

    @property
    def dst_keypoints_xy(self) -> np.ndarray:
        """The destination image's keypoints coordinates.

        :return: The destination image's keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.dst_keypoints_xy

    @property
    def src_keypoints_desc(self) -> np.ndarray:
        """The source image's descriptors vectors.

        :return: The source image's descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.src_keypoints_desc

    @property
    def dst_keypoints_desc(self) -> np.ndarray:
        """The destination image's descriptors vectors.

        :return: The destination image's descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.dst_keypoints_desc

    @property
    def matched_src_keypoints_xy(self) -> np.ndarray:
        """The source image's matched keypoints coordinates.

        :return: The source image's matched keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.matched_src_keypoints_xy

    @property
    def matched_dst_keypoints_xy(self) -> np.ndarray:
        """The destination image's matched keypoints coordinates.

        :return: The destination image's matched keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.matched_dst_keypoints_xy

    @property
    def matched_src_keypoints_desc(self) -> np.ndarray:
        """The source image's matched descriptors vectors.

        :return: The source image's matched descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.matched_src_keypoints_desc

    @property
    def matched_dst_keypoints_desc(self) -> np.ndarray:
        """The destination image's matched descriptors vectors.

        :return: The destination image's matched descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.matched_dst_keypoints_desc

    @property
    def matched_indices(self) -> np.ndarray:
        """The indices of the matches in the source and destination keypoints and descriptors' arrays.

        :return: The indices of the matches in the source and destination keypoints and descriptors' arrays.
        The first column is for the source and the second for the destination arrays.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.matched_indices

    @property
    def filtered_matched_src_keypoints_xy(self) -> np.ndarray:
        """The source image's filtered matched keypoints coordinates.

        :return: The source image's filtered matched keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.filtered_matched_src_keypoints_xy

    @property
    def filtered_matched_dst_keypoints_xy(self) -> np.ndarray:
        """The destination image's filtered matched keypoints coordinates.

        :return: The destination image's filtered matched keypoints coordinates.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.filtered_matched_dst_keypoints_xy

    @property
    def filtered_matched_src_keypoints_desc(self) -> np.ndarray:
        """The source image's filtered matched descriptors vectors.

        :return: The source image's filtered matched descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.filtered_matched_src_keypoints_desc

    @property
    def filtered_matched_dst_keypoints_desc(self) -> np.ndarray:
        """The destination image's filtered matched descriptors vectors.

        :return: The destination image's filtered matched descriptors vectors.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.filtered_matched_dst_keypoints_desc

    @property
    def filtered_indices(self) -> np.ndarray:
        """The indices of the filtered matches in the source and destination keypoints and descriptors' arrays.

        :return: The indices of the filtered matches in the source and destination keypoints and descriptors'
        arrays. The first column is for the source and the second for the destination arrays.
        :rtype: np.ndarray
        """

        return self.feature_matching_pipeline.filtered_indices

    @property
    def warp2D(self) -> Optional[Warping2D]:
        """The transformation that can warp either points or images.

        :return: The transformation that can warp either points or images.
        :rtype: Optional[Warping2D]
        """

        return self.__warp2D

    @property
    def viz_warp2D(self) -> Optional[Warping2D]:
        """The transformation that can warp, for vizualisation purposes, either points or images.

        :return: The transformation that can warp either points or images.
        :rtype: Optional[Warping2D]
        """

        return self.__viz_warp2D

    @property
    def exec_times(self) -> Dict[str, float]:
        """The execution time of each steps of the experiment.

        :return: The dictionary that contains all the execution times.
        :rtype: Dict[str, float]
        """

        return self.__exec_times

    @property
    def eval_results(self) -> Optional[Dict[str, Any]]:
        """The results of the evaluation.

        :return: The results of the evaluation.
        :rtype: Optional[Dict[str, Any]]
        """

        return self.__eval_results

    def __quantitative(
        self,
        src: np.ndarray,
        warped_src: np.ndarray,
        dst: np.ndarray,
        pixel_mapping: VectorType = FloatVector(x=1.0, y=1.0),
        metrics: List[str] = ["TRE", "RMSE"],
    ) -> Dict[str, Any]:

        out = {
            "pixel_mapping_xy": pixel_mapping.xy,
            "src": src,
            "warped": warped_src,
            "dst": dst,
            "metrics": {"local": {}, "global": {}},
        }

        if "TRE" in metrics:
            # Initial TRE
            _tre = tre(src=src, dst=dst, pixel_mapping=pixel_mapping)
            out["metrics"]["local"]["init_tre"] = _tre

            # Post registration TRE
            _tre = tre(src=warped_src, dst=dst, pixel_mapping=pixel_mapping)
            out["metrics"]["local"]["tre"] = _tre

        if "RMSE" in metrics:
            # Initial TRE
            _rmse = (
                rmse(src=out["metrics"]["local"]["init_tre"], pixel_mapping=pixel_mapping)
                if out["metrics"]["local"].get("init_tre", None) is not None
                else rmse(src=src, dst=dst, pixel_mapping=pixel_mapping)
            )
            out["metrics"]["global"]["init_rmse"] = _rmse

            # Post registration TRE
            _rmse = (
                rmse(src=out["metrics"]["local"]["tre"], pixel_mapping=pixel_mapping)
                if out["metrics"]["local"].get("tre", None) is not None
                else rmse(src=warped_src, dst=dst, pixel_mapping=pixel_mapping)
            )
            out["metrics"]["global"]["rmse"] = _rmse

        return out

    def __qualitative(
        self,
        src_img: np.ndarray,
        dst_img: np.ndarray,
        src_pts: np.ndarray,
        warped_src_pts: np.ndarray,
        dst_pts: np.ndarray,
        good_indices: Optional[np.ndarray] = None,
        plots: List[str] = ["InWaTa", "Matching"],
        cmap: colors.ListedColormap = generate_cmap(),
    ) -> Optional[Dict[str, Any]]:
        if good_indices is None or not list(good_indices):
            good_indices = np.ones((src_pts.shape[0],), bool)

        out = {}

        if "InWaTa" in plots:
            img = generate_standalone_figure(
                draw_init_warped_target,
                src_img=src_img,
                dst_img=dst_img,
                src_pts=src_pts[good_indices, :],
                warped_src_pts=warped_src_pts,
                dst_pts=dst_pts[good_indices, :],
                out_img="numpy",
            )
            out["inwata"] = img

        if "Matching" in plots:
            img = generate_standalone_figure(
                draw_matches,
                src_img=src_img,
                src_matched_kp=src_pts,
                dst_img=dst_img,
                dst_matched_kp=dst_pts,
                good_indices=good_indices,
                cmap=cmap,
                out_img="numpy",
            )
            out["matching"] = img

        return out

    def __describe_gt(self, img: np.ndarray, lndmrks: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        def _augment(mask: np.ndarray, desc: np.ndarray) -> np.ndarray:
            augmented = np.zeros((mask.shape[0], desc.shape[1]))
            augmented[:] = np.NaN
            augmented[mask] = desc

            return augmented

        cv_lndmrks = cv2.KeyPoint_convert(lndmrks.tolist())

        descriptor = self.feature_matching_pipeline.descriptor
        tmp, lndmrks_descriptors = descriptor(img, cv_lndmrks)

        indices = np.ones(lndmrks.shape[0], dtype=bool)
        updated_lndmrks = np.array([lndmrk.pt for lndmrk in tmp])
        if updated_lndmrks.shape != lndmrks.shape:
            indices = ~indices
            for updated_coords in updated_lndmrks:
                for i, coords in enumerate(lndmrks):
                    if np.allclose(updated_coords, coords):
                        indices[i] = True
                        break

        lndmrks_descriptors = _augment(indices, lndmrks_descriptors)

        return indices, lndmrks_descriptors

    def __combine_lost_lndmrks(self, src_indices: np.ndarray, dst_indices: np.ndarray) -> np.ndarray:
        return src_indices * dst_indices

    def evaluate(self, extract_gt: Optional[Callable[..., np.ndarray]] = None) -> None:
        """Evaluate the experiment's performance both quantitatively and qualitatively in an
        unsupervised manner with the use of the corresponding keypoints and in a supervised manner
        with use of provided landmarks.

        :param extract_gt: A callable use to extract the ground truth if provided, defaults to None.
        :type extract_gt: Optional[Callable[..., np.ndarray]], optional
        :raises TypeError: Triggered if the experiment has not yet been executed.
        """

        if not isinstance(self.__warp2D, Warping2D):
            raise TypeError(
                "The experiment has not been executed yet. Please execute it before evaluating "
                "its performance on the input."
            )

        self.__eval_results = {"unsupv": {}}

        # Compute unsupervised quantitative analysis
        pixel_mapping = FloatVector(x=self.resolution, y=self.resolution)
        assert self.__src_pre_process is not None
        assert self.__dst_pre_process is not None
        assert self.__viz_warp2D is not None
        offset_src_pts = self.feature_matching_pipeline.matched_src_keypoints_xy + np.array(
            self.__src_pre_process.offset.xy
        )
        offset_dst_pts = self.feature_matching_pipeline.matched_dst_keypoints_xy + np.array(
            self.__dst_pre_process.offset.xy
        )
        warped_src_pts = self.__viz_warp2D(self.feature_matching_pipeline.filtered_matched_src_keypoints_xy)
        self.__eval_results["unsupv"]["quant"] = self.__quantitative(
            src=offset_src_pts[self.feature_matching_pipeline.filtered_indices, :],
            warped_src=warped_src_pts,
            dst=offset_dst_pts[self.feature_matching_pipeline.filtered_indices, :],
            pixel_mapping=pixel_mapping,
            metrics=["TRE", "RMSE"],
        )

        # UNSUPV_QUANT outputs
        self.outputs_helper.generate_outputs(
            level_flag="UNSUPV_QUANT",
            level_dir="unsupv",
            prefix="quant",
            suffix="metrics.csv",
            save_method=save_metrics,
            save_method_kwargs={
                "quant": self.__eval_results["unsupv"]["quant"],
                "imgs_path": {"dst_path": self.dst_wsi.path, "src_path": self.src_wsi.path},
            },
        )

        # Compute unsupervised qualitative analysis
        self.__eval_results["unsupv"]["qual"] = self.__qualitative(
            src_img=self.__src_pre_process.padded_initial,
            dst_img=self.__dst_pre_process.padded_initial,
            src_pts=offset_src_pts,
            warped_src_pts=warped_src_pts,
            dst_pts=offset_dst_pts,
            good_indices=self.feature_matching_pipeline.filtered_indices,
            plots=["InWaTa", "Matching"],
        )

        # UNSUPV_QUAL outputs
        self.outputs_helper.generate_outputs(
            level_flag="UNSUPV_QUAL",
            level_dir="unsupv",
            prefix="qual",
            suffix="inwata.png",
            save_method=save_img,
            save_method_kwargs={"img": self.__eval_results["unsupv"]["qual"]["inwata"]},
        )
        self.outputs_helper.generate_outputs(
            level_flag="UNSUPV_QUAL",
            level_dir="unsupv",
            prefix="unsupv",
            suffix="matching.png",
            save_method=save_img,
            save_method_kwargs={"img": self.__eval_results["unsupv"]["qual"]["matching"]},
        )

        if self.src_gt_path and self.dst_gt_path:
            self.__eval_results["supv"] = {}

            # Extract landmarks
            assert extract_gt is not None
            src_lndmrks = extract_gt(
                self.src_gt_path,
                factor=self.src_wsi.get_magnification_for_resolution(FloatVector(x=self.resolution, y=self.resolution))
                / self.src_wsi.mag,
            )
            dst_lndmrks = extract_gt(
                self.dst_gt_path,
                factor=self.src_wsi.get_magnification_for_resolution(FloatVector(x=self.resolution, y=self.resolution))
                / self.dst_wsi.mag,
            )

            # Remove landmarks which have no correspondences in either one of the given ground truth
            nb_common = min(len(pts) for pts in [src_lndmrks, dst_lndmrks])
            src_lndmrks = src_lndmrks[:nb_common]
            dst_lndmrks = dst_lndmrks[:nb_common]

            # Compute supervised quantitative analysis
            self.__eval_results["supv"]["quant"] = self.__quantitative(
                src=src_lndmrks + np.array(self.__src_pre_process.offset.xy),
                warped_src=self.__viz_warp2D(src_lndmrks),
                dst=dst_lndmrks + np.array(self.__dst_pre_process.offset.xy),
                pixel_mapping=pixel_mapping,
                metrics=["TRE", "RMSE"],
            )

            # SUPV_QUANT outputs
            self.outputs_helper.generate_outputs(
                level_flag="SUPV_QUANT",
                level_dir="supv",
                prefix="quant",
                suffix="metrics.csv",
                save_method=save_metrics,
                save_method_kwargs={
                    "quant": self.__eval_results["supv"]["quant"],
                    "imgs_path": {"dst_path": self.dst_wsi.path, "src_path": self.src_wsi.path},
                    "gts_path": {"dst_gt_path": self.dst_gt_path, "src_gt_path": self.src_gt_path},
                },
            )

            # Compute the gt descriptors if their is a descriptor
            if getattr(self.feature_matching_pipeline, "descriptor", None):
                assert self.__src_pre_process.preprocessed is not None
                assert self.__dst_pre_process.preprocessed is not None
                src_indices, src_desc = self.__describe_gt(self.__src_pre_process.preprocessed, src_lndmrks)
                dst_indices, dst_desc = self.__describe_gt(self.__dst_pre_process.preprocessed, dst_lndmrks)

                # Get the mask to remove the lost landmarks due to the description
                kept_lndmrks_mask = self.__combine_lost_lndmrks(src_indices, dst_indices)

                # SUPV_QUANT outputs
                self.outputs_helper.generate_outputs(
                    level_flag="SUPV_QUANT",
                    level_dir="supv",
                    prefix="desc",
                    suffix="src.csv",
                    save_method=save_points,
                    save_method_kwargs={
                        "coords": src_lndmrks[kept_lndmrks_mask],
                        "descriptors": src_desc[kept_lndmrks_mask],
                    },
                )
                self.outputs_helper.generate_outputs(
                    level_flag="SUPV_QUANT",
                    level_dir="supv",
                    prefix="desc",
                    suffix="dst.csv",
                    save_method=save_points,
                    save_method_kwargs={
                        "coords": dst_lndmrks[kept_lndmrks_mask],
                        "descriptors": dst_desc[kept_lndmrks_mask],
                    },
                )

            # Compute supervised qualitative analysis
            self.__eval_results["supv"]["qual"] = self.__qualitative(
                src_img=self.__src_pre_process.padded_initial,
                dst_img=self.__dst_pre_process.padded_initial,
                src_pts=src_lndmrks + np.array(self.__src_pre_process.offset.xy),
                warped_src_pts=self.__viz_warp2D(src_lndmrks),
                dst_pts=dst_lndmrks + np.array(self.__dst_pre_process.offset.xy),
                plots=["InWaTa"],
            )

            # SUPV_QUAL outputs
            assert self.__eval_results["supv"]["qual"] is not None
            self.outputs_helper.generate_outputs(
                level_flag="SUPV_QUAL",
                level_dir="supv",
                prefix="supv",
                suffix="inwata.png",
                save_method=save_img,
                save_method_kwargs={"img": self.__eval_results["supv"]["qual"]["inwata"]},
            )

        assert isinstance(self.__logger, ExperimentLogger)
        self.__logger.clear_handlers(self.__logger)


if __name__ == "__main__":
    from datasets.anhir import ANHIRDataset
    from datasets.generic import CompleteDataset
    from processing.chaining import ChainingPreProcessing, PreProcessingLink
    from processing.channel_reduction import grayscale
    from registration.descriptors import AVAILABLE_DESCRIPTORS, Descriptor
    from registration.detectors import AVAILABLE_DETECTORS, Detector
    from registration.feature_matching import Traditional
    from registration.filtering_methods import RANSAC_FAMILY, RANSACFamily
    from registration.matching_methods import BruteForce
    from registration.transform_estimators import AVAILABLE_TRANSFORMS

    ds = CompleteDataset(
        datasets=[
            ANHIRDataset("/data/dataset_ANHIR"),
        ]
    )

    for pair in ds.dev_set.values():
        slide_pair = pair["ds_cls"].get_slides(pair)

        preprocessing_steps = [
            (
                grayscale,
                {
                    "mode": {
                        "img": {"in": "single", "out": "single"},
                        "mask": {"in": None, "out": None},
                    },
                    "invert": False,
                    "as_ubyte": True,
                },
            )
        ]

        exp = FeatureBasedExperiment(
            src_wsi=slide_pair["src"],
            dst_wsi=slide_pair["dst"],
            outputs_helper=OutputsHelper(
                output_level_specs=["ALL"],
                parent_dir="/workspaces/feature-based_registration/tmp",
                basename=FeatureBasedExperiment.__name__,
            ),
            src_gt_path=slide_pair["src_gt"],
            dst_gt_path=slide_pair["dst_gt"],
            resolution=10.0,
            preprocessing_cls=ChainingPreProcessing,
            preprocessing_kwargs={
                "links": [PreProcessingLink(method, **kwargs) for method, kwargs in preprocessing_steps]
            },
            feature_matching_pipeline=Traditional(
                detector=Detector(method=AVAILABLE_DETECTORS["SIFT"]),
                descriptor=Descriptor(method=AVAILABLE_DESCRIPTORS["BRIEF"]),
                matcher=AVAILABLE_TRANSFORMS["BruteForce"](distance="norm_hamming", cross_check=True),
                filter_method=RANSACFamily(method=RANSAC_FAMILY["affRANSAC"], ransacReprojThreshold=7),
                transform=AVAILABLE_TRANSFORMS["skAffine"](),
            ),
        )

        exp.execute()
        exp.evaluate(extract_gt=slide_pair["gt_extractor"])
        break
