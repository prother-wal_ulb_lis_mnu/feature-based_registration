"""
Additional functions useful for the registration are defined in this module.
"""

import importlib
from typing import Any, Callable

import cv2
from cv2.xfeatures2d import (
    DAISY,
    FREAK,
    LATCH,
    SURF,
    VGG,
    BoostDesc,
    BriefDescriptorExtractor,
    StarDetector,
)

MANUAL_PICKLE = {
    str(cv2.SIFT.create): cv2.SIFT,
    str(SURF.create): SURF,
    str(cv2.BRISK.create): cv2.BRISK,
    str(cv2.ORB.create): cv2.ORB,
    str(cv2.KAZE.create): cv2.KAZE,
    str(cv2.AKAZE.create): cv2.AKAZE,
    str(StarDetector.create): StarDetector,  # derived from CenSurE
    str(DAISY.create): DAISY,
    str(LATCH.create): LATCH,
    str(VGG.create): VGG,
    str(BoostDesc.create): BoostDesc,
    str(BriefDescriptorExtractor.create): BriefDescriptorExtractor,
    str(FREAK.create): FREAK,
    str(cv2.BFMatcher): cv2.BFMatcher,
}


def get_cls_from_str(module_file: str, cls: str) -> Callable[..., Any]:
    """Dynamically import and extract the desired class.

    :param module_file: Module to import
    :type module_file: str
    :param cls: Class to import and extract
    :type cls: str
    :return: The desired class
    :rtype: Callable[..., Any]
    """

    return getattr(importlib.import_module(module_file), cls)
