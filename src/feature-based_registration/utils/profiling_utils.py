"""
Additional functions useful for profiling are defined in this module.
"""

HEADER_FILLING_SIZE = 20


def get_heading(file: str, size: int = HEADER_FILLING_SIZE, parent_folder: bool = False) -> str:
    """Get the name of the python file in upper case for logging purposes.

    :param file: The python file ``__name__``.
    :type file: str
    :param size: The number of `#` that should be displayed, defaults to HEADER_FILLING_SIZE.
    :type size: int, optional
    :param parent_folder: Whether to use the parent folder name instead as a
    heading, defaults to False.
    :type parent_folder: bool, optional
    :return: The heading to log.
    :rtype: str
    """

    index = -1
    if parent_folder:
        index = -2

    return f"{'#'*size} {file.rsplit('.', maxsplit=abs(index))[index].upper()} {'#'*size}"
