"""
Additional useful functions for the evaluation part are found in this module.
"""

import logging

import numpy as np
from openwholeslide import VectorType


def convert_pixel_to_physical_units(distances: np.ndarray, pixel_mapping: VectorType) -> np.ndarray:
    """Convert pixel distances to physical distances such as microns and millimeters.

    :param distances: The distances in the y and x axis ([y, x]) in pixels.
    :type distances: np.ndarray
    :param pixel_mapping: The size, in both the x and y axis, of a pixel.
    :type pixel_mapping: VectorType
    :return distances: The distances in the y and x axis ([y, x]) in given physical units.
    :rtype: np.ndarray
    """

    logging.info(f"Pixel mapping: x={pixel_mapping.x} and y={pixel_mapping.y}")
    distances *= pixel_mapping.yx

    return distances
