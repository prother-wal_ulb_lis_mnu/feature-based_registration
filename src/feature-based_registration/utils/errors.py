"""
The module defines additional errors.
"""


class PathNotADirectoryError(ValueError):
    """Custom error that is raised when the given path is not a directory."""

    def __init__(self, *, path: str) -> None:
        self.path = path

    def __str__(self) -> str:
        return f"{self.path} is not a valid directory path."


class FailedExperiments(RuntimeError):
    """Custom error that is raised when the run's failed experiments' logs are not empty."""

    def __init__(self, *args: object) -> None:
        super().__init__(*args)

    def __str__(self) -> str:
        return (
            "Some experiments have failed. Please take a look at the logs present in the "
            "run's experiments directory for more details."
        )
