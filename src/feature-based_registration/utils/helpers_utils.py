import os
from types import NoneType
from typing import Dict, List, Union


def generate_dir_if_missing(path: str, recursive: bool = False) -> None:
    """Create a directory is it does not exists.

    :param path: The absolute path of the directory to create if not already existing.
    :type path: str
    :param recursive: Whether to create all the missing directories in the given path or not, defaults to False.
    :type recursive: bool, optional
    """

    if os.path.exists(path):
        return
    elif not recursive:
        os.mkdir(path)
        return

    generate_dir_if_missing(os.path.dirname(path), recursive=recursive)

    os.mkdir(path)


def serialize_iterable(to_serialize: Union[Dict, List]) -> Union[Dict, List]:
    if isinstance(to_serialize, list):
        _iter = enumerate(to_serialize)
    elif isinstance(to_serialize, dict):
        _iter = to_serialize.items()

    for key, value in _iter:
        if isinstance(value, list):
            to_serialize[key] = serialize_iterable(value.copy())
        elif isinstance(value, dict):
            to_serialize[key] = serialize_iterable(value.copy())
        elif (
            not isinstance(value, str)
            and not isinstance(value, int)
            and not isinstance(value, float)
            and not isinstance(value, NoneType)
            and not isinstance(value, bool)
        ):
            to_serialize[key] = str(value)

    return to_serialize
