from typing import Any, Callable, Dict, Optional, TypeVar

TCallable = TypeVar("TCallable", bound=Callable[..., Any])


def available(
    methods: Dict[str, Callable[..., Any]], newname: Optional[str] = None
) -> Callable[[TCallable], TCallable]:
    """Closure that allows to provide the methods dictionary to the decorator.

    :param methods: The methods dictionary.
    :type methods: Dict[str, Callable[..., Any]]
    :param newname: The name that will be used as key in the dictionary, defaults to None.
    :type newname: Optional[str], optional
    """

    def decorator(cls: TCallable) -> TCallable:
        """Add the given class in the methods dictionary.

        :param cls: The class to add in the methods dictionary.
        :type cls: TCallable
        :return: The class itself.
        :rtype: TCallable
        """

        if newname:
            methods[newname] = cls
        else:
            methods[cls.__name__] = cls
        return cls

    return decorator
