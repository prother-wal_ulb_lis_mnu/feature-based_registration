from typing import Optional, Tuple

import numpy as np
from scipy.interpolate import Akima1DInterpolator
from skimage.exposure import match_histograms
from utils.general_utils import available

AVAILABLE_MAP_DISTRIBUTIONS = {}


def get_channel_stats(img: np.ndarray) -> np.ndarray:
    return np.array(
        [np.percentile(img, 1), np.percentile(img, 5), np.mean(img), np.percentile(img, 95), np.percentile(img, 99)]
    )


@available(AVAILABLE_MAP_DISTRIBUTIONS, newname="NormStats")
def norm_img_stats(
    src: np.ndarray, dst: np.ndarray, src_mask: Optional[np.ndarray] = None, dst_mask: Optional[np.ndarray] = None
) -> Tuple[np.ndarray, np.ndarray]:
    """Normalize the src image with respect to the dst image statistics.

    Based on method in
    "A nonlinear mapping approach to stain normalization in digital histopathology images using
    image-specific color deconvolution.", Khan et al. 2014

    :param src: The image to normalize, should be encoded in 8 bits (value range = [0, 255]).
    :type src: np.ndarray
    :param dst: The image used to normalize src, should be encoded in 8 bits (value range = [0, 255]).
    :type dst: np.ndarray
    :param src_mask: A mask that will be applied to the src image to restrain the area from which the
    statistics will be computed on, defaults to None.
    :type src_mask: Optional[np.ndarray], optional
    :param dst_mask: A mask that will be applied to the dst image to restrain the area from which the
    statistics will be computed on, defaults to None.
    :type dst_mask: Optional[np.ndarray], optional
    :return src_normed: The normalized src image with respect to the dst image statistics.
    :rtype: np.ndarray
    :return dst: The dst image.
    :rtype: np.ndarray
    """

    stats_flat = []
    for i, m in zip([dst, src], [dst_mask, src_mask]):
        if m is None:
            stats_flat.append(get_channel_stats(i))
        else:
            stats_flat.append(get_channel_stats(i[m > 0]))

    dst_stats_flat, src_stats_flat = stats_flat

    # Avoid duplicates and keep in ascending order
    lower_knots = np.array([0])
    upper_knots = np.array([300, 350, 400, 450])
    src_stats_flat = np.hstack([lower_knots, src_stats_flat, upper_knots]).astype(float)
    dst_stats_flat = np.hstack([lower_knots, dst_stats_flat, upper_knots]).astype(float)

    # Add epsilon to avoid duplicate values
    eps = 10 * np.finfo(float).resolution
    eps_array = np.arange(len(src_stats_flat)) * eps
    src_stats_flat = src_stats_flat + eps_array
    dst_stats_flat = dst_stats_flat + eps_array

    # Make sure src stats are in ascending order
    src_order = np.argsort(src_stats_flat)
    src_stats_flat = src_stats_flat[src_order]
    dst_stats_flat = dst_stats_flat[src_order]

    cs = Akima1DInterpolator(src_stats_flat, dst_stats_flat)

    if src_mask is None:
        src_normed = cs(src.reshape(-1)).reshape(src.shape)
    else:
        src_normed = src.copy()
        src_normed[src_mask > 0] = cs(src[src_mask > 0])

    if src.dtype == np.uint8:
        src_normed = np.clip(src_normed, 0, 255)

    return src_normed, dst


@available(AVAILABLE_MAP_DISTRIBUTIONS, newname="MatchHist")
def match_hists(
    src: np.ndarray, dst: np.ndarray, src_mask: Optional[np.ndarray] = None, dst_mask: Optional[np.ndarray] = None
) -> Tuple[np.ndarray, np.ndarray]:

    if src_mask is None and dst_mask is None:
        return match_histograms(src, dst), dst
    elif src_mask is None and dst_mask is not None:
        return match_histograms(src, dst[dst_mask]), dst
    elif src_mask is not None and dst_mask is None:
        src[src_mask] = match_histograms(src[src_mask], dst)
        return src, dst
    else:
        src[src_mask] = match_histograms(src[src_mask], dst[dst_mask])
        return src, dst
