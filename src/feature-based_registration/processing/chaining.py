from typing import Any, Callable, Dict, List, Optional, Tuple

import numpy as np
from helpers.logger import logger
from openwholeslide import IntVector
from processing.channel_reduction import AVAILABLE_CHANNEL_REDUCTIONS
from processing.image import ProcessedImage
from processing.map_distributions import AVAILABLE_MAP_DISTRIBUTIONS
from processing.masking import AVAILABLE_MASKINGS
from profiling.decorators import benchmark_method, method_with_logging
from utils.profiling_utils import HEADER_FILLING_SIZE, get_heading

AVAILABLE_PREPROCESSING = AVAILABLE_CHANNEL_REDUCTIONS
AVAILABLE_PREPROCESSING.update(AVAILABLE_MAP_DISTRIBUTIONS)
AVAILABLE_PREPROCESSING.update(AVAILABLE_MASKINGS)


class PreProcessingLink:
    """Implement a building block referred to as link in that can be chained to other links."""

    def __init__(
        self,
        method: Callable[..., Any],
        mode: Dict[str, Optional[Dict[str, Optional[str]]]] = {
            "img": {"in": "single", "out": "single"},
            "mask": {"in": "single", "out": None},
        },
        **kwargs: Any,
    ) -> None:
        """Constructor of PreProcessingLink class.

        :param method: The method that will be used to process the image.
        :type method: Callable[..., Any]
        :param mode: This parameter defines how the inputs will be handled upon calling this link. It is
        a dictionary with 2 main keys: `img` and `mask`. The value for each key follow this template:
        `{"in": <in_value>, "out": <out_value>}` with `<in_value>` and `<out_value>` equal to either: `None`
        (not passed or returned]), `single` (one at a time) or `both`. If either of the main key is missing
        or its value is set to None, it will be considered as having `None` for both `in` and `out` sub-keys.
        Defaults to {"img": {"in": "single", "out": "single"}, "mask": {"in": "single", "out": None}}.
        :type mode: Dict[str, Optional[Dict[str, Optional[str]]]]

        Additionally, kwargs specific to the method to use can be provided.
        """

        self.exec_time: Optional[float] = None

        self.method = method
        self.mode = mode
        self.method_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.method=}, {self.method_kwargs=})"

    def __mode_to_str(
        self,
        mode: Dict,
        to_stringify: List[str] = [],
        template: Dict = {
            "img": {"in": None, "out": None},
            "mask": {"in": None, "out": None},
        },
        symbol: str = "_",
    ) -> str:
        for key in template.keys():
            value = mode.get(key, None)
            if not value:
                if "None" not in to_stringify:
                    to_stringify.append("None")
            elif isinstance(value, dict):
                to_stringify.append(self.__mode_to_str(mode[key], [key], template[key], "-"))
            elif value in ("single", "both"):
                if value not in to_stringify:
                    to_stringify.append(value)
            else:
                raise ValueError(
                    f"Key '{key}' has been provided with value '{value}'. Please refer to the documentation "
                    "to provide one of the available options."
                )

        return f"{symbol}".join(to_stringify)

    @method_with_logging(heading=f"{'-'*((2*HEADER_FILLING_SIZE)+2+len(__name__.rsplit('.', maxsplit=1)[-1]))}")
    @benchmark_method
    def __call__(
        self,
        src_img: np.ndarray,
        dst_img: np.ndarray,
        src_mask: np.ndarray,
        dst_mask: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Process the inputs.

        :param src_img: The src image to process.
        :type src_img: np.ndarray
        :param dst_img: The dst image to process.
        :type dst_img: np.ndarray
        :param src_mask: The src mask to process.
        :type src_mask: np.ndarray
        :param dst_mask: The dst mask to process.
        :type dst_mask: np.ndarray
        :return src_img: The processed src image.
        :return dst_img: The processed dst image.
        :return src_mask: The src mask.
        :return dst_mask: The dst mask.
        :rtype: Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]
        """

        str_mode = self.__mode_to_str(self.mode, [])

        if str_mode == "img-None_mask-None":
            logger.info(f"Inputs unchanged due to 'empty' mode: {self.mode}.")
            return src_img, dst_img, src_mask, dst_mask
        elif str_mode == "img-single_mask-None":
            return (
                self.method(src_img, **self.method_kwargs),
                self.method(dst_img, **self.method_kwargs),
                src_mask,
                dst_mask,
            )
        elif str_mode == "img-single_mask-single-None":
            return (
                self.method(src_img, mask=src_mask, **self.method_kwargs),
                self.method(dst_img, mask=dst_mask, **self.method_kwargs),
                src_mask,
                dst_mask,
            )
        elif str_mode == "img-single-None_mask-single":
            return (
                src_img,
                dst_img,
                self.method(src_img, mask=src_mask, **self.method_kwargs),
                self.method(dst_img, mask=dst_mask, **self.method_kwargs),
            )
        elif str_mode == "img-single-None_mask-None-single":
            return (
                src_img,
                dst_img,
                self.method(src_img, **self.method_kwargs),
                self.method(dst_img, **self.method_kwargs),
            )
        elif str_mode == "img-both_mask-both-None":
            return (
                *self.method(src_img, dst_img, src_mask=src_mask, dst_mask=dst_mask, **self.method_kwargs),
                src_mask,
                dst_mask,
            )
        else:
            logger.info(f"Inputs unchanged due to unsupported mode: {self.mode}.")
            return src_img, dst_img, src_mask, dst_mask


class ChainingPreProcessing:
    """Implement the pre-processing chain."""

    def __init__(self, *, links: List[PreProcessingLink], pad_value: float = 0.0) -> None:
        """Constructor for ChainingPreProcessing.

        :param links: The ordered links that will be used to pre-process the inputs.
        :type: List[PreProcessingLink]
        :param pad_value: The value to use when padding the image, defaults to 0.0.
        :type: float, optional
        """

        self.exec_time: Optional[float] = None

        self.links = links
        self.pad_value = pad_value

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.links=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(
        self,
        src_img: np.ndarray,
        dst_img: np.ndarray,
    ) -> Tuple[ProcessedImage, ProcessedImage]:
        """Pre-process the input images according to a set of given pre-processing steps.

        :param src_img: The src image to pre-process.
        :type src_img: np.ndarray
        :param dst_img: The dst image to pre-process.
        :type dst_img: np.ndarray
        :return preproc_src_img: The pre-processed src image object.
        :return preproc_dst_img: The pre-processed dst image object.
        :rtype: Tuple[ProcessedImage, ProcessedImage]
        """

        _preproc_src_img = src_img.copy()
        _preproc_dst_img = dst_img.copy()
        src_mask = np.ones(src_img.shape[:2], dtype=bool)
        dst_mask = np.ones(dst_img.shape[:2], dtype=bool)

        for link in self.links:
            _preproc_src_img, _preproc_dst_img, src_mask, dst_mask = link(
                _preproc_src_img, _preproc_dst_img, src_mask, dst_mask
            )

        init_src_dimensions, init_dst_dimensions = (
            IntVector.from_yx((src_img.shape[0], src_img.shape[1])).xy,
            IntVector.from_yx((dst_img.shape[0], dst_img.shape[1])).xy,
        )
        pad_pair_dimensions_xy = np.max((init_src_dimensions, init_dst_dimensions), axis=0)

        # Add a 10th of the pad_pair_dimensions_xy to purposely overpad the images
        # This is to avoid having issues when src is bigger than dst (rigid transform)
        pad_dimensions_xy = pad_pair_dimensions_xy + pad_pair_dimensions_xy // 10

        preproc_src_img = ProcessedImage(
            initial=src_img,
            preprocessed=_preproc_src_img,
            mask=src_mask,
            padder=ProcessedImage.get_padder(src_img, pad_dimensions_xy, self.pad_value),
        )

        preproc_dst_img = ProcessedImage(
            initial=dst_img,
            preprocessed=_preproc_dst_img,
            mask=dst_mask,
            padder=ProcessedImage.get_padder(dst_img, pad_dimensions_xy, self.pad_value),
        )

        return preproc_src_img, preproc_dst_img
