from dataclasses import dataclass, field
from typing import Optional

import numpy as np
from openwholeslide import IntVector, PaddingParameters


@dataclass
class ProcessedImage:
    initial: np.ndarray
    padder: PaddingParameters

    preprocessed: Optional[np.ndarray] = field(default=None)
    mask: Optional[np.ndarray] = field(default=None)

    @classmethod
    def get_padder(cls, img: np.ndarray, padding: np.ndarray, pad_value: float) -> PaddingParameters:
        """Get the padder object that allows padding the image.

        :param img: The image to which the padder is associated.
        :type img: np.ndarray
        :param padding: The desired dimensions for the padded image.
        :type padding: np.ndarray
        :param pad_value: The value to use for the padding.
        :type pad_value: float
        :return: The padder object that can be used to pad the given image.
        :rtype: PaddingParameters
        """

        if img.dtype == np.uint8:
            pad_value = (
                max(np.iinfo(img.dtype).max, int(pad_value)) if int(pad_value) != np.iinfo(img.dtype).min else 0
            )

        pad_dimensions = IntVector.from_xy((padding[0], padding[1])) - IntVector.from_yx((img.shape[0], img.shape[1]))
        pad_before = IntVector(x=pad_dimensions.x // 2, y=pad_dimensions.y // 2)

        padder = PaddingParameters(
            pad_before=pad_before,
            pad_after=IntVector(x=pad_dimensions.x - pad_before.x, y=pad_dimensions.y - pad_before.y),
            pad_value=pad_value,
        )

        return padder

    @property
    def offset(self) -> IntVector:
        return self.padder.pad_before
    
    def _pad(self, img: np.ndarray) -> np.ndarray:
        # Adapt the `pad_value` to the image's encoding type
        pad_value = self.padder.pad_value
        if img.dtype == np.uint8:
            pad_value = (
                max(np.iinfo(img.dtype).max, int(self.padder.pad_value))
                if int(self.padder.pad_value) != np.iinfo(img.dtype).min
                else 0
            )

        return np.pad(img, self.padder.pad_width[: len(img.shape)], constant_values=pad_value)

    @property
    def padded_initial(self) -> np.ndarray:
        return self._pad(self.initial)

    @property
    def padded_preprocessed(self) -> Optional[np.ndarray]:
        if self.preprocessed is None:
            return self.preprocessed
        
        return self._pad(self.preprocessed)

    @property
    def padded_mask(self) -> Optional[np.ndarray]:
        if self.mask is None:
            return self.mask
        
        return np.pad(self.mask, self.padder.pad_width[: len(self.mask.shape)], constant_values=0.0)
