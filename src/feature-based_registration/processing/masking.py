import numpy as np
from skimage import img_as_ubyte
from skimage.color import rgb2gray, rgb2hsv
from skimage.filters._gaussian import gaussian
from skimage.morphology import opening, square
from utils.general_utils import available

AVAILABLE_MASKINGS = {}


def merge_masks(mask_1: np.ndarray, mask_2: np.ndarray, value: bool = True) -> np.ndarray:
    """Merge two masks where the condition is satisfied.

    :param mask_1: The first mask.
    :type mask_1: np.ndarray
    :param mask_2: The second mask.
    :type mask_2: np.ndarray
    :param value: The interesting value in the masks, defaults to True.
    :type value: bool, optional
    :return: The fusion of both masks.
    :rtype: np.ndarray
    """

    return (mask_1 == value) & (mask_2 == value)


def get_artifacts_mask(
    img: np.ndarray, value_threshold: float = 0.25, saturation_threshold: float = 0.1
) -> np.ndarray:
    """Compute the artifacts mask of an image.

    :param img: The given image from which the artifacts mask will be derived.
    :type img: np.ndarray
    :param value_threshold: The threshold value, in the Value channel from the HSV representation of the
    image, under which pixels may be part of an artefact. This defaults to 0.25.
    :type value_threshold: float, optional
    :param saturation_threshold: The threshold value, in the Saturation channel from the HSV representation
    of the image, under which pixels may be part of an artefact. This defaults to 0.1.
    :type saturation_threshold: float, optional
    :return: The artifacts mask where pixels assigned with `True` are considered to be part of the artifacts.
    :rtype: np.ndarray
    """

    blur = gaussian(img, sigma=5, preserve_range=True, channel_axis=2).astype(np.uint8)
    hsv = rgb2hsv(blur)

    v_mask = hsv[..., 2] < value_threshold
    s_mask = hsv[..., 1] > saturation_threshold
    tmp = merge_masks(v_mask, s_mask)
    mask = opening(tmp, square(20))

    return mask


def get_trivial_mask(img: np.ndarray) -> np.ndarray:
    """Compute the trivial background mask of an image.

    :param img: The given image from which the trivial mask will be derived.
    :type img: np.ndarray
    :return: The trivial mask where pixels assigned with `True` are considered to be part of the trivial
    background.
    :rtype: np.ndarray
    """

    return opening(img_as_ubyte(rgb2gray(img)) == 255, square(20))


@available(AVAILABLE_MASKINGS, newname="MaskTrivialNArtefacts")
def get_tissue_mask_without_trivial_nor_artifacts(
    img: np.ndarray, value_threshold: float = 0.25, saturation_threshold: float = 0.1, as_ubyte: bool = True
) -> np.ndarray:
    """Compute the boolean mask that only keeps the tissue data without trivial background nor artifacts.

    :param img: The given image from which the artifacts mask will be derived.
    :type img: np.ndarray
    :param value_threshold: The threshold value, in the Value channel from the HSV representation of the
    image, under which pixels may be part of an artefact. This defaults to 0.25.
    :type value_threshold: float, optional
    :param saturation_threshold: The threshold value, in the Saturation channel from the HSV representation
    of the image, under which pixels may be part of an artefact. This defaults to 0.1.
    :type saturation_threshold: float, optional
    :param as_ubyte: Whether to convert the image to 8-bits before computing the mask or not, defaults to False.
    :type as_ubyte: bool, optional
    :return: The mask where pixels assigned with `True` are considered to be either part of the trivial
    background or the artifacts.
    :rtype: np.ndarray
    """

    _img = img.copy()

    if as_ubyte:
        _img = img_as_ubyte(_img)

    return merge_masks(
        get_trivial_mask(_img), get_artifacts_mask(_img, value_threshold, saturation_threshold), value=False
    )
