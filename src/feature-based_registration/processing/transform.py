"""
This module defines image transformation methods to both transform point clouds and 2D images.
"""

from copy import deepcopy
from dataclasses import dataclass, field
from typing import Any, Optional

import numpy as np
from profiling.decorators import benchmark_method, method_with_logging
from skimage.transform import ProjectiveTransform, warp
from utils.profiling_utils import get_heading


@dataclass
class Warping2D:
    """Warping class that allow the warping of a set of points in 2D.

    :param transform: The instantiation of a skimage-like transformation class that will be used to warp
    the data. It should be callable and implement an inverse method to get the inverse transform.
    :type transform: ProjectiveTransform
    :param resolution: The resolution, in µm per pixel, to which the transformation was computed. This is
    used to adjust the transformation's translation parameters to the resolution level at which it will be
    applied.
    :type resolution: float
    """

    exec_time: Optional[float] = field(init=False, repr=False, default=None)

    transform: ProjectiveTransform
    resolution: float

    @method_with_logging()
    def warp_img(self, img: np.ndarray, target_resolution: Optional[float], **kwargs: Any) -> np.ndarray:
        """Warp the image according to the given transformation parameters.

        :param img: The image to warp.
        :type img: np.ndarray
        :param target_resolution: The resolution of the image that will be warped. If `None` is provided,
        then the nominal resolution will be taken into account.
        :type target_resolution: Optional[float]
        :return: The warped image according to the given transformation.
        :rtype: np.ndarray
        """

        tf = self.transform
        if target_resolution is not None and (factor := self.resolution / target_resolution) != 1.0:
            tf = deepcopy(self.transform)
            tf.params[:2, 2] *= factor

        return warp(img, tf.inverse, **kwargs)

    @method_with_logging()
    def warp_pts(self, pts: np.ndarray, target_resolution: Optional[float]) -> np.ndarray:
        """Warp the keypoints/landmarks according to the given transformation parameters.

        :param pts: The keypoints/landmarks coordinates to warp.
        :type pts: np.ndarray
        :param target_resolution: The resolution of the image that will be warped. If `None` is provided,
        then the nominal resolution will be taken into account.
        :type target_resolution: Optional[float]
        :return: The warped keypoints/landmarks.
        :rtype: np.ndarray
        """

        tf = self.transform
        if target_resolution is not None and (factor := self.resolution / target_resolution) != 1.0:
            tf = deepcopy(self.transform)
            tf.params[:2, 2] *= factor

        return tf(pts)

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(self, data: np.ndarray, target_resolution: Optional[float] = None, **kwargs: Any) -> np.ndarray:
        """Warp the data according to the given transformation parameters.

        :param data: The data to warp, either a set of keypoints/landmarks
        coordinates (shape = N x 2) or an image (shape = M x P).
        :type data: np.ndarray
        :param target_resolution: The resolution of the image that will be warped. If `None` is provided,
        then the nominal resolution will be taken into account, defaults to None.
        :type target_resolution: Optional[float]
        :return: The warped data according to the given transformation.
        :rtype: np.ndarray
        """

        if data.shape[1] > 2:
            return self.warp_img(data, target_resolution=target_resolution, **kwargs)

        return self.warp_pts(data, target_resolution=target_resolution)
