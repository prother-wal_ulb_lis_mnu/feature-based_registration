"""
Module containing methods to reduce RGB images to a single channel for further
processing.
"""

from __future__ import annotations

from typing import List, Optional, Tuple, Union

import numpy as np
from processing.stain import rgb2stain
from skimage import img_as_ubyte
from skimage.color import rgb2gray
from skimage.exposure import rescale_intensity
from utils.general_utils import available

AVAILABLE_CHANNEL_REDUCTIONS = {}


@available(AVAILABLE_CHANNEL_REDUCTIONS, newname="Gray")
def grayscale(im: np.ndarray, invert: bool = False, as_ubyte: bool = True):
    """Convert the given RGB image to a grayscale one.

    :param im: The image to convert.
    :type im: np.ndarray
    :param invert: Whether resulting image should be inverted or not, defaults to False.
    :type invert: bool, optional
    :param as_ubyte: Whether the image should be encoded as uint8 or not, defaults to False.
    :return gray: bool, optional
    :rtype gray: np.ndarray
    """

    gray = rgb2gray(im)
    if as_ubyte:
        gray = img_as_ubyte(gray)
    if invert:
        return gray.max() - gray
    return gray


@available(AVAILABLE_CHANNEL_REDUCTIONS, newname="Hem")
def haematoxylin(
    im: np.ndarray,
    mask: np.ndarray,
    white: Optional[np.ndarray] = None,
    white_percentile: int = 99,
    rescale_range: Union[float, Tuple[float, ...], List[float]] = (-0.1082, 1.627),
    invert: bool = False,
    as_ubyte: bool = True,
) -> np.ndarray:
    """Extract the hematoxylin channel in the given image.

    :param im: The image from which the hematoxylin channel will be extracted.
    :type im: np.ndarray
    :param mask: A mask that will be applied to the image to restrain the area from which the
    H&E deconvolution will be computed on.
    :type mask: np.ndarray
    :param white: The estimation of the background "white" color. If nothing is provided
    the white is automatically estimated with the extract_white function, defaults to None.
    :type white: Optional[np.ndarray], optional
    :param white_percentile: The percentile of the brightness distribution to take as threshold. This
    argument is left aside if the white argument is not None, defaults to 99.
    :type white_percentile: int, optional
    :param rescale_range: The range within the intensity values of the haematoxylin should rescaled. The value
    can either be a tuple specifying the extrema within which the values should be rescaled or quantiles
    (q and 1-q) that will be used to define the 'in range' of the rescaled intensity values. This defaults
    to (-0.1082, 1.627).
    :type rescale_range: Union[float, Tuple[float, float], List[float]], optional
    :param invert: Whether resulting image should be inverted or not, defaults to False.
    :type invert: bool, optional
    :param as_ubyte: Whether the image should be encoded as uint8 or not, defaults to False.
    :return hem: bool, optional
    :rtype hem: np.ndarray
    """

    if isinstance(rescale_range, list):
        rescale_range = tuple(rescale_range)

    assert isinstance(rescale_range, tuple) or isinstance(rescale_range, float)
    
    hem = rgb2stain(
        im, mask=mask, white=white, white_percentile=white_percentile, rescale_range=[rescale_range, 0.005, 0.005]
    )[..., 0]

    if invert:
        hem = 255 - hem

    if not as_ubyte:
        hem = rescale_intensity(hem, out_range=np.float32)

    return hem
