"""
This module implement a stain normalisation method adapted from https://github.com/schaugf/HEnorm_python.
"""

from typing import List, Optional, Tuple, Union

import numpy as np
from colour import convert, utilities
from skimage import img_as_ubyte
from skimage.exposure import rescale_intensity

HE_REF = np.array([[0.5626, 0.2159, 0], [0.7201, 0.8012, 0], [0.4062, 0.5581, 0]])
HDAB_REF = np.array([[0.5626, 0.27, 0], [0.7201, 0.57, 0], [0.4062, 0.78, 0]])
MAX_C_REF = np.array([1.9705, 1.0308, 1])


def extract_white(img: np.ndarray, percentile: int = 99) -> np.ndarray:
    """Extract the background (white) pixel value with use of a brightness threshold.

    :param img: The image from which the white should be extracted.
    :type img: np.ndarray
    :param percentile: The brightness percentile to use as brightness threshold, defaults to 99.
    :type percentile: int, optional
    :return: The estimated white value.
    :rtype: np.ndarray
    """

    eps = np.finfo("float").eps
    with utilities.suppress_warnings(colour_usage_warnings=True):
        if 1 < img.max() <= 255 and np.issubdtype(img.dtype, np.integer):
            cam = convert(img / 255 + eps, "sRGB", "CAM16UCS")
        else:
            cam = convert(img + eps, "sRGB", "CAM16UCS")

    brightest_thresh = np.percentile(cam[..., 0], percentile)
    brightest_idx = np.where(cam[..., 0] >= brightest_thresh)

    white = img[brightest_idx]

    return np.ceil(np.mean(white, axis=0))


def rgb2od(img: np.ndarray, white: Optional[np.ndarray] = None, white_percentile: int = 99) -> np.ndarray:
    """Convert RGB intensity to RGB Optical Density.

    :param img: The image to covert from the RGB to the OD space.
    :type img: np.ndarray
    :param white: The estimation of the background "white" color. If nothing is provided
    the white is automatically estimated with the extract_white function, defaults to None.
    :type white: Optional[np.ndarray], optional
    :param white_percentile: The percentile of the brightness distribution to take as threshold. This
    argument is left aside if the white argument is not None, defaults to 99.
    :type white_percentile: int, optional
    :return: The image in the OD space.
    :rtype: np.ndarray
    """

    img = img_as_ubyte(img)

    if white is None:
        white = extract_white(img, white_percentile)

    return -np.log((img.astype(np.float32) + 1) / white)


def complement_stain_matrix(incomplete_stains_mat: np.ndarray) -> np.ndarray:
    """Complete the stain basis by computing the normalized cross product between
    the two provided vectors.

    :param incomplete_stains_mat: The 3x2 stain matrix which miss the third basis vector.
    :type incomplete_stains_mat: np.ndarray
    :return: The completed stains matrix.
    :rtype: np.ndarray
    """

    stain0 = incomplete_stains_mat[:, 0]
    stain1 = incomplete_stains_mat[:, 1]
    stain2 = np.cross(stain0, stain1)

    return np.array([stain0, stain1, stain2 / np.linalg.norm(stain2)]).T


def magnitude(mat: np.ndarray) -> np.ndarray:
    """Get the magnitude of each column vector in a matrix.

    :param mat: The matrix from which the magnitude of each column is computed.
    :type mat: np.ndarray
    :return: The magnitude of each column vector.
    :rtype: np.ndarray
    """

    return np.sqrt((mat**2).sum(0))


def normalize(mat: np.ndarray) -> np.ndarray:
    """Normalize each column vector in a matrix.

    :param mat: The matrix to normalize.
    :type mat: np.ndarray
    :return: The normalized matrix.
    :rtype: np.ndarray
    """

    return mat / magnitude(mat)


def od2stain_mat(od: np.ndarray, alpha: int = 1, beta: float = 0.15) -> np.ndarray:
    """Find stain matrix from Optical Density image.

    :param od: The OD image (MxNx3).
    :type od: np.ndarray
    :param alpha: The percentile used to estimate the cone vectors, defaults to 1.
    :type alpha: int, optional
    :param beta: The threshold under which the OD values are considered too transparent, defaults to 0.15.
    :type beta: float, optional
    :return: The stain matrix where each column represent a stain vector. The first one being associated
    to the hematoxylin, the second one the eosin or DAB depending the situation we are in and the third
    one is third vector to constitute a orthogonal basis.
    :rtype: np.ndarray
    """

    # Flatten OD (columns for RGB channels) and remove transparent pixels
    od_filt = od[~np.any(od < beta, axis=2)]

    # Compute eigenvectors (ascending order)
    _, eigvecs = np.linalg.eigh(np.cov(od_filt.T))

    # Project on the plane spanned by the eigenvectors corresponding to the two
    # largest eigenvalues
    od_filt_proj = od_filt.dot(eigvecs[:, 1:3])

    # Compute the distribution of angles between each projected point and the
    # first eigenvector: theta = arctan(adjacent/hyphotenus)
    angle2eigvec = np.arctan2(od_filt_proj[:, 1], od_filt_proj[:, 0])

    # Compute the alphath and (100-alpha)th percentiles of the angles distribution
    lower_bound = np.percentile(angle2eigvec, alpha)
    upper_bound = np.percentile(angle2eigvec, 100 - alpha)

    # Projection of the two largest eigenvectors in the basis composed by the corrected
    # vectors corresponding to the alphath and (100-alpha)th percentiles
    v_min = eigvecs[:, 1:3].dot(np.array([(np.cos(lower_bound), np.sin(lower_bound))]).T)
    v_max = eigvecs[:, 1:3].dot(np.array([(np.cos(upper_bound), np.sin(upper_bound))]).T)

    # A heuristic to make the vector corresponding to hematoxylin first and the
    # one corresponding to eosin/dab second
    if v_min[0] > v_max[0]:
        stains = np.array((v_min[:, 0], v_max[:, 0])).T
    else:
        stains = np.array((v_max[:, 0], v_min[:, 0])).T

    # Each stain is a column in this matrix
    stains = complement_stain_matrix(normalize(stains))

    return stains


def od2stainc(od: np.ndarray, stains_mat: np.ndarray) -> np.ndarray:
    """Converts RGB Optical Density image to stain concentration images.

    :param od: The OD image.
    :type od: np.ndarray
    :param stainvec: The stains matrix in which each stain vector is a column.
    :type stainvec: np.ndarray
    :return: The unmixed image in each stain direction.
    :rtype: np.ndarray
    """

    # Rows correspond to channels (RGB), columns to OD values
    transposed_od_flat = np.reshape(od, (-1, 3)).T

    # Determine the concentrations of the individual stains
    conc = np.linalg.lstsq(stains_mat, transposed_od_flat, rcond=None)[0]

    # Normalize stain concentrations
    # maxC = np.array([np.percentile(conc[0, :], 99), np.percentile(conc[1, :], 99), np.percentile(conc[2, :], 99)])
    # tmp = np.divide(maxC, MAX_C_REF)
    # unmixed_flat = np.divide(conc, tmp[:, np.newaxis]).T

    # return unmixed_flat.reshape((od.shape[0], od.shape[1], -1))
    return conc.T.reshape((od.shape[0], od.shape[1], -1))


def stainc2stain(
    stainc: np.ndarray,
    mask: np.ndarray,
    rescale_range: List = [(-0.1082, 1.627), 0.005, 0.005],
) -> np.ndarray:
    """From stain concentration image to the actual unmixed stain channels rescaled to the uint8 range.

    :param stainc: The unmixed image in each stain direction.
    :type stainc: np.ndarray
    :param mask: The mask used to delimit the regions on which the rescale range is computed and/or applied to.
    :type mask: np.ndarray
    :param rescale_range: Ranges, one per channel, within the intensity values should rescaled. The values
    can either be a tuple specifying the extrema within which the values should be rescaled or quantiles
    (q and 1-q) that will be used to define the 'in range' of the rescaled intensity values. This defaults
    to [(-0.1082, 1.627), 0.005, 0.005].
    :type rescale_range: List, optional
    :return: The unmixed stain channels rescaled to the uint8 range.
    :rtype: np.ndarray
    """

    stains = np.zeros(stainc.shape, dtype=np.uint8)
    for i in range(stainc.shape[-1]):
        if isinstance(rescale_range[i], tuple):
            in_range = rescale_range[i]
        else:
            in_range = tuple(np.quantile(stainc[..., i][mask], (rescale_range[i], 1 - rescale_range[i])))

        stains[..., i][mask] = rescale_intensity(stainc[..., i][mask], in_range=in_range, out_range=np.uint8)

    return stains


def rgb2stainc(
    img: np.ndarray, white: Optional[np.ndarray] = None, white_percentile: int = 99, alpha: int = 1, beta: float = 0.15
) -> np.ndarray:
    """Deconvolution of an RGB image to unmixed stains space.

    :param img: The image to deconvolve of shape MxNx3.
    :type img: np.ndarray
    :param white: The estimation of the background "white" color. If nothing is provided
    the white is automatically estimated with the extract_white function, defaults to None.
    :type white: Optional[np.ndarray], optional
    :param white_percentile: The percentile of the brightness distribution to take as threshold. This
    argument is left aside if the white argument is not None, defaults to 99.
    :type white_percentile: int, optional
    :param alpha: The percentile used to estimate the cone vectors, defaults to 1.
    :type alpha: int, optional
    :param beta: The threshold under which the OD values are considered too transparent, defaults to 0.15.
    :type beta: float, optional
    :return: The unmixed image with respect to the computed stains matrix. Its dimension is MxNx3 where the first
    channel correspond to the hematoxylin, the second one to either eosin or DAB depending on the situation we are
    in and the third one is all the remaining staining which should not contain much information.
    :rtype: np.ndarray
    """

    # Convert the image to Optical Density
    od = rgb2od(img, white, white_percentile)

    # Compute the stains matrix
    stains_mat = od2stain_mat(od, alpha, beta)

    return od2stainc(od, stains_mat)


def rgb2stain(
    img: np.ndarray,
    mask: np.ndarray,
    white: Optional[np.ndarray] = None,
    white_percentile: int = 99,
    alpha: int = 1,
    beta: float = 0.15,
    rescale_range: List[Union[float, Tuple[float, ...]]] = [(-0.1082, 1.627), 0.005, 0.005],
) -> np.ndarray:
    img = img_as_ubyte(img)

    if white is None:
        white = extract_white(img[mask], white_percentile)

    # Convert the image to Optical Density
    od = rgb2od(img, white)

    # Compute the stains matrix
    stains_mat = od2stain_mat(od, alpha, beta)

    # Get the stains concentrations
    stainc = od2stainc(od, stains_mat)

    return stainc2stain(stainc, mask, rescale_range)


def stainc2norm(stainc: np.ndarray, white: np.ndarray, ref: Optional[np.ndarray] = HE_REF) -> np.ndarray:
    """Reconstruct the RGB image from the stains concentrations based on reference values.

    :param stainc: The stains concentrations.
    :type stainc: np.ndarray
    :param white: The value of the background.
    :type white: np.ndarray
    :param ref: The reference values, defaults to HE_REF.
    :type ref: Optional[np.ndarray], optional
    :return: The reconstructed RGB image.
    :rtype: np.ndarray
    """

    # Rows correspond to stain channels, columns to stain concentration values
    transposed_stainc_flat = stainc.reshape(-1, 3).T

    assert ref is not None

    # recreate the image using reference mixing matrix
    Inorm = np.multiply(white, np.exp(-ref.dot(transposed_stainc_flat)).T).T
    np.clip(Inorm, a_min=0, a_max=254, out=Inorm)

    return np.reshape(Inorm.T, (stainc.shape[0], stainc.shape[1], 3)).astype(np.uint8)


def rgb2norm(img: np.ndarray, white: np.ndarray, ref: Optional[np.ndarray] = HE_REF) -> np.ndarray:
    """Normalize the stain appearances of the RGB image.

    :param img: The image to deconvolve and reconstruct based on the stains concentrations found.
    :type img: np.ndarray
    :param white: The value of the background.
    :type white: np.ndarray
    :param ref: The reference values, defaults to HE_REF.
    :type ref: Optional[np.ndarray], optional
    :return: _description_
    :rtype: np.ndarray
    """

    hec = rgb2stainc(img)
    return stainc2norm(hec, white, ref)


def stainc2rgbunmixed(
    stainc: np.ndarray, white: np.ndarray, ref: Optional[np.ndarray] = HE_REF
) -> Tuple[np.ndarray, np.ndarray]:
    """Get a RGB visualisation of the stains concentrations using their reference colors.

    :param stainc: The stains concentrations.
    :type stainc: np.ndarray
    :param white: The value of the background.
    :type white: np.ndarray
    :param ref: The reference values, defaults to HE_REF.
    :type ref: Optional[np.ndarray], optional
    :return: The RGB visualisations for the H and E/DAB channels.
    :rtype: Tuple[np.ndarray, np.ndarray]
    """

    # Rows correspond to stain channels, columns to stain concentration values
    transposed_stainc_flat = stainc.reshape(-1, 3).T

    assert ref is not None

    # Unmix H
    rgb_hem = np.multiply(
        white, np.exp(np.expand_dims(-ref[:, 0], axis=1).dot(np.expand_dims(transposed_stainc_flat[0, :], axis=0))).T
    ).T
    np.clip(rgb_hem, a_min=0, a_max=254, out=rgb_hem)
    rgb_hem = np.reshape(rgb_hem.T, stainc.shape).astype(np.uint8)

    # Unmix E or DAB
    rgb_eos = np.multiply(
        white, np.exp(np.expand_dims(-ref[:, 1], axis=1).dot(np.expand_dims(transposed_stainc_flat[1, :], axis=0))).T
    ).T
    np.clip(rgb_eos, a_min=0, a_max=254, out=rgb_eos)
    rgb_eos = np.reshape(rgb_eos.T, stainc.shape).astype(np.uint8)

    return rgb_hem, rgb_eos
