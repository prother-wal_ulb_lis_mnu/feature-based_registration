"""
This module is used to define useful colormaps functions for visualization.
"""

from colour import utilities, convert
import numba as nb
import numpy as np
from matplotlib import cm, colors
from scipy.spatial import distance
from skimage.exposure import rescale_intensity


def jzazbz_cmap(luminosity: float = 0.012, colorfulness: float = 0.02, max_h: int = 260) -> np.ndarray:
    """Get colormap based on JzAzBz colorspace, which has good hue linearity. Already
    preceptually uniform.

    Implementation comes from valis-wsi package.

    :param luminosity: defaults to 0.012.
    :type luminosity: float, optional
    :param colorfulness: defaults to 0.02.
    :type colorfulness: float, optional
    :param max_h: defaults to 260.
    :type max_h: int, optional
    :return: The RGB colormap.
    :rtype: np.ndarray
    """

    h = np.deg2rad(np.arange(0, 360))
    a = colorfulness * np.cos(h)
    b = colorfulness * np.sin(h)
    j = np.repeat(luminosity, len(h))

    jzazbz = np.dstack([j, a, b])
    with utilities.suppress_warnings(colour_usage_warnings=True):
        rgb = convert(jzazbz, "JzAzBz", "sRGB")

    rgb = np.clip(rgb, 0, 1)[0]
    if max_h != 360:
        rgb = rgb[0:max_h]

    return rgb


def get_n_colors(rgb: np.ndarray, n: int) -> np.ndarray:
    """Pick n most different colors in RGB. Differences based of RGB values in the CAM16UCS colorspace.

    Implementation comes from the valis-wsi package.

    :param rgb: The RGB colormap from which n colors will be picked.
    :type rgb: np.ndarray
    :param n: The number of desired colors.
    :type n: int
    :return: The picked colors.
    :rtype: np.ndarray
    """

    with utilities.suppress_warnings(colour_usage_warnings=True):
        if 1 < rgb.max() <= 255 and np.issubdtype(rgb.dtype, np.integer):
            cam = convert(rgb / 255, "sRGB", "CAM16UCS")
        else:
            cam = convert(rgb, "sRGB", "CAM16UCS")

    sq_D = distance.cdist(cam, cam)
    max_D = sq_D.max()
    most_dif_2Didx = np.where(sq_D == max_D)  # 2 most different colors
    most_dif_img1 = most_dif_2Didx[0][0]
    most_dif_img2 = most_dif_2Didx[1][0]
    rgb_idx = [most_dif_img1, most_dif_img2]

    possible_idx = list(range(sq_D.shape[0]))
    possible_idx.remove(most_dif_img1)
    possible_idx.remove(most_dif_img2)

    for _ in range(2, n):
        max_d_idx = np.argmax([np.min(sq_D[i, rgb_idx]) for i in possible_idx])
        new_rgb_idx = possible_idx[max_d_idx]
        rgb_idx.append(new_rgb_idx)
        possible_idx.remove(new_rgb_idx)

    return rgb[rgb_idx]


def generate_statistical_test_color_palette(
    pvalues: np.ndarray, significance_threshold: float = 0.05, reverse: bool = True
) -> np.ndarray:
    """Generate an adequate the color palette for a statistical test based on the given values and the significance
    threshold.

    :param values: The p-values.
    :type values: np.ndarray
    :param significance_threshold: The threshold under which the null hypothesis is rejected, defaults to 0.05.
    :type significance_threshold: float, optional
    :param reverse: Whether to reverse the color or not, defaults to True.
    :type reverse: bool, optional
    :return: The color palette that is suited to the statistical test's p-values.
    :rtype: np.ndarray
    """

    unique_values = np.unique(pvalues)
    n_under = len(np.where(unique_values < significance_threshold)[0])
    n_over = len(unique_values) - n_under

    cmap = cm.RdYlGn
    if reverse:
        cmap = cm.RdYlGn.reversed()

    if n_over == 1:
        return np.vstack((cmap(np.linspace(0, 110, n_under).astype(np.uint8)), cmap(255)))

    return np.vstack(
        (cmap(np.linspace(0, 110, n_under).astype(np.uint8)), cmap(np.linspace(150, 255, n_over).astype(np.uint8)))
    )


def generate_cmap(color_palette: np.ndarray = get_n_colors(jzazbz_cmap(), 260)) -> colors.ListedColormap:
    """Generate a colormap based on a color palette.

    :param color_palette: The color palette that will be turned into a colormap, defaults to
    get_n_colors(jzazbz_cmap(), 260).
    :type color_palette: np.ndarray, optional
    :return: The colormap.
    :rtype: colors.ListedColormap
    """

    return colors.ListedColormap(color_palette.tolist())


@nb.njit(fastmath=True, cache=True)
def blend_colors(img: np.ndarray, colors: np.ndarray, scale_by: str) -> np.ndarray:
    """Color an image by blending.

    Implementation comes from the valis-wsi package.

    :param img: Image containing the raw data encoded as float32
    :type img: np.ndarray
    :param colors: Colors for each channel in `img`.
    :type colors: np.ndarray
    :param scale_by: How to scale each channel. "image" will scale the channel by
    the maximum value in the image. "channel" will scale the channel by the maximum
    in the channel.
    :type scale_by: str
    :return: A colored version of `img`.
    :rtype: np.ndarray
    """

    if len(colors) > 1:
        n_channel_colors = colors.shape[1]
    else:
        n_channel_colors = len(colors)

    if img.ndim > 2:
        r, c, nc = img.shape[:3]
    else:
        nc = 1
        r, c = img.shape[:2]

    eps = 1.0000000000000001e-15
    sum_img = img.sum(axis=2) + eps
    if scale_by == "image":
        img_max = img.max()

    blended_img = np.zeros((r, c, n_channel_colors))
    for i in range(nc):
        # relative image is how bright the channel will be
        if scale_by != "image":
            channel_max = max(img[..., i].max(), eps)
            relative_img = img[..., i] / channel_max
        else:
            relative_img = img[..., i] / img_max

        # blending is how to weight the mix of colors, similar to an alpha channel
        blending = img[..., i] / sum_img
        for j in range(colors.shape[1]):
            channel_color = colors[i, j]
            blended_img[..., j] += channel_color * relative_img * blending

    return blended_img


def color_multichannel(
    multichannel_img: np.ndarray,
    marker_colors: np.ndarray,
    rescale_channels: bool = False,
    normalize_by: str = "image",
    cspace: str = "Hunter Lab",
) -> np.ndarray:
    """Color a multichannel image to view as RGB.

    Implementation comes from the valis-wsi package.

    :param multichannel_img: Image to color.
    :type multichannel_img: np.ndarray
    :param marker_colors: sRGB colors for each channel.
    :type marker_colors: np.ndarray
    :param rescale_channels: If True, then each channel will be scaled between 0 and
    1 before building the composite RGB image. This will allow markers to 'pop' in
    areas where they are expressed in isolation, but can also make it appear more
    marker is expressed than there really is, defaults to False.
    :type rescale_channels: bool, optional
    :param normalize_by: ``image`` will produce an image where all values are scaled
    between 0 and the highest intensity in the composite image. This will produce an
    image where one can see the expression of each marker relative to the others,
    making it easier to compare marker expression levels. ``channel`` will first scale
    the intensity of each channel, and then blend all of the channels together. This
    will allow one to see the relative expression of each marker, but won't allow one
    to directly compare the expression of markers across channels, defaults to "image".
    :type normalize_by: str, optional
    :param cspace: Colorspace in which `marker_colors` will be blended. JzAzBz, Hunter
    Lab, and sRGB all work well. But, see colour.COLOURSPACE_MODELS for other possible
    colorspaces, defaults to "Hunter Lab".
    :type cspace: str, optional
    :return: An sRGB version of `multichannel_img`.
    :rtype: np.ndarray
    """

    if rescale_channels:
        rescaled_channels = [
            rescale_intensity(multichannel_img[..., i].astype(float), in_range="image", out_range=(0, 1))
            for i in range(multichannel_img.shape[2])
        ]

        multichannel_img = np.dstack(rescaled_channels)

    is_srgb = cspace.lower() == "srgb"
    is_srgb_01 = True
    if 1 < marker_colors.max() <= 255 and np.issubdtype(marker_colors.dtype, np.integer):
        srgb_01 = marker_colors / 255
        is_srgb_01 = False

    else:
        srgb_01 = marker_colors
    eps = np.finfo("float").eps
    if not is_srgb:
        with utilities.suppress_warnings(colour_usage_warnings=True):
            cspace_colors = convert(srgb_01 + eps, "sRGB", cspace)
    else:
        cspace_colors = srgb_01

    blended_img = blend_colors(multichannel_img, cspace_colors, normalize_by)
    if not is_srgb:
        with utilities.suppress_warnings(colour_usage_warnings=True):
            srgb_blended = convert(blended_img + eps, cspace, "sRGB") - 2 * eps
    else:
        srgb_blended = blended_img

    srgb_blended = np.clip(srgb_blended, 0, 1)
    if not is_srgb_01:
        srgb_blended = (255 * srgb_blended).astype(marker_colors.dtype)

    return srgb_blended
