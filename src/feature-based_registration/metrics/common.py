"""
In this module are defined metrics commonly used to compare distributions.
"""

import itertools as it
from numbers import Number
from typing import Any, Callable, List, Tuple, TypeVar

import numpy as np
import scipy.stats as st

T = TypeVar("T", bound=Number)


def compute_interquartile(values: np.ndarray | List[float]) -> Tuple[np.floating[Any], np.floating[Any]]:
    """Compute the interquartile range (IQ range) of the given distribution.

    :param values: The distribution from which to compute the IQ range.
    :type values: np.ndarray | List[float]
    :return: The IQ range of the given distribution expressed as: (p25, p75).
    :rtype: Tuple[np.floating[Any], np.floating[Any]]
    """

    return (np.percentile(values, 25), np.percentile(values, 75))


def compute_descriptive_statistics(
    values: np.ndarray | List[float], measures: List[Callable] = [np.median, compute_interquartile]
) -> List[T]:
    """Compute the descriptive statistics of a distribution.

    :param values: The distribution from which to compute the descriptive statistics.
    :type values: np.ndarray | List[float]
    :param measures: The descriptive statistics to compute on the given distribution. The options are
    any descriptive statistic callable that accepts as input the distribution (e.g. np.mean, np.median,
    np.std, ...), defaults to [np.median, compute_interquartile].
    :type measures: List[Callable], optional
    :return: The list of descriptive statistics in the same order as the one given by the ``measures``
    argument.
    :rtype: List[T]
    """

    out = []
    for measure in measures:
        out.append(measure(values))

    return out


def friedman_test(*args: Any) -> Tuple[np.floating[Any], float, List[np.floating[Any]], List[np.floating[Any]]]:
    """Performs a Friedman ranking test. Tests the hypothesis that in a set of k dependent samples groups
    (where k >= 2) at least two of the groups represent populations with different median values.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015. (https://github.com/citiususc/stac)

    References:
    M. Friedman, The use of ranks to avoid the assumption of normality implicit in the analysis of variance,
        Journal of the American Statistical Association 32 (1937) 674-701.
    D.J. Sheskin, Handbook of parametric and nonparametric statistical procedures. crc Press, 2003,
        Test 25: The Friedman Two-Way Analysis of Variance by Ranks

    :param sample1, sample2, ... : The sample measurements for each group.
    :type sample1, sample2, ...: array_like
    :raises ValueError: Raised when less than two samples are provided.
    :raises ValueError: Raised when the samples' sizes are not equal.
    :return iman_davenport: The computed F-value of the test.
    :rtype iman_davenport: np.floating[Any]
    :return p-value: The associated p-value from the F-distribution.
    :rtype p-value: float
    :return rankings: The ranking for each group.
    :rtype rankings: List[np.floating[Any]]
    :return pivots: The pivotal quantities for each group.
    :rtype pivots: List[np.floating[Any]]
    """

    k = len(args)
    if k < 2:
        raise ValueError("Less than 2 levels")
    n = len(args[0])
    if len(set([len(v) for v in args])) != 1:
        raise ValueError("Unequal number of samples")

    rankings = []
    for i in range(n):
        row = [col[i] for col in args]
        row_sort = sorted(row)
        rankings.append([row_sort.index(v) + 1 + (row_sort.count(v) - 1) / 2.0 for v in row])

    rankings_avg = [np.mean([case[j] for case in rankings]) for j in range(k)]
    rankings_cmp = [r / np.sqrt(k * (k + 1) / (6.0 * n)) for r in rankings_avg]

    chi2 = ((12 * n) / float((k * (k + 1)))) * (
        (np.sum([r**2 for r in rankings_avg])) - ((k * (k + 1) ** 2) / float(4))
    )
    iman_davenport = ((n - 1) * chi2) / float((n * (k - 1) - chi2))

    p_value = float((1 - st.f.cdf(iman_davenport, k - 1, (k - 1) * (n - 1))).item())

    return iman_davenport, p_value, rankings_avg, rankings_cmp


def nemenyi_multitest(pivots: List[np.floating[Any]]) -> np.ndarray:
    """Performs a Nemenyi post-hoc test using the pivot quantities obtained by a ranking test. Tests the
    hypothesis that the ranking of each pair of groups are different.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015. (https://github.com/citiususc/stac)

    References:
    Bonferroni-Dunn: O.J. Dunn, Multiple comparisons among means, Journal of the American Statistical
        Association 56 (1961) 52-64.

    :param pivots: A list with the pivotal quantity for each group.
    :type pivots: List[np.floating[Any]]
    :return pvArray: The associated p-values from the Z-distribution as a symmetrical 2D array.
    :rtype: np.ndarray
    """

    k = len(pivots)
    versus = list(it.combinations(range(k), 2))
    pvArray = np.ones((k, k))
    m = int(k * (k - 1) / 2.0)

    for vs in versus:
        z = abs(pivots[vs[0]] - pivots[vs[1]])
        pv = float((2 * (1 - st.norm.cdf(abs(z)))).item())
        pvArray[vs[0], vs[1]] = min(m * pv, 1)
        pvArray[vs[1], vs[0]] = min(m * pv, 1)

    return pvArray


def statistical_score(
    metric_matrix: np.ndarray, pval_matrix: np.ndarray, significance_threshold: float = 0.05
) -> np.ndarray:
    """Compute the statistical score of a p-values matrix and balance it by a metric matrix.

    :param metric_matrix: The matrix of the metric that is used to characterized each distribution. Its shape is n x m.
    :type metric_matrix: np.ndarray
    :param pval_matrix: The p-values matrix, its shape is n*m x n*m.
    :type pval_matrix: np.ndarray
    :param significance_threshold: The threshold under which the null hypothesis is rejected, defaults to 0.05.
    :type significance_threshold: float, optional
    :return: The statistical score matrix, its shape is n x m.
    :rtype: np.ndarray
    """

    # Score the metric matrix
    scored_metric_matrix = np.zeros(pval_matrix.shape)
    for i, metric in enumerate(metric_matrix.flatten()):
        over_ids = np.where(metric_matrix.flatten() < metric)
        under_ids = np.where(metric_matrix.flatten() > metric)

        tmp = scored_metric_matrix[i, :]
        tmp[over_ids] = -1
        tmp[under_ids] = 1

        scored_metric_matrix[i, :] = tmp

    # Score the p-value matrix
    scored_pval_matrix = np.zeros(pval_matrix.shape)
    significant = np.where(pval_matrix < significance_threshold)
    scored_pval_matrix[significant] = 1

    score_matrix = np.dot(scored_metric_matrix, scored_pval_matrix)

    return np.diag(score_matrix).reshape(metric_matrix.shape)
