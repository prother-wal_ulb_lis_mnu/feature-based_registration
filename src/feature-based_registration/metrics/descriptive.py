"""
In this module are defined metrics used to analyse the descriptive capabilities of the descriptors.
"""

import cv2
import numpy as np


def compute_lndmrks_ranks(
    src_lndmrks_desc: np.ndarray, dst_lndmrks_desc: np.ndarray, distance: int = cv2.NORM_L2
) -> np.ndarray:
    """Get the landmarks matching ranks when matched with a knn taking all neighbours
    into account.

    :param src_lndmrks_desc: The src landmarks' descriptors.
    :type src_lndmrks_desc: np.ndarray
    :param dst_lndmrks_desc: The dst landmarks' descriptors.
    :type dst_lndmrks_desc: np.ndarray
    :param distance: The distance that is used to match the two sets of points, defaults
    to cv2.NORM_L2.
    :type distance: int, optional
    :return ranks: The ranks of the corresponding dst landmarks.
    :rtype: np.ndarray
    """

    matcher = cv2.BFMatcher(distance, crossCheck=False)
    match_mat = matcher.knnMatch(src_lndmrks_desc, dst_lndmrks_desc, dst_lndmrks_desc.shape[0])

    ranks = np.empty((dst_lndmrks_desc.shape[0], 2), dtype=np.uint8)
    ranks[:, 0] = np.arange(dst_lndmrks_desc.shape[0])
    for lndmrk in match_mat:
        for ind, counterpart in enumerate(lndmrk):
            if counterpart.trainIdx == counterpart.queryIdx:
                ranks[counterpart.queryIdx, 1] = ind

    return ranks


def normalize_ranks(ranks: np.ndarray) -> np.ndarray:
    """Normalize the landmarks matching ranks by the number of landmarks (-1 because of rank 0).

    :param ranks: The landmarks matching ranks.
    :type ranks: np.ndarray
    :return: The normalized landmarks matching ranks.
    :rtype: np.ndarray
    """

    return ranks / (ranks.shape[0] - 1)
