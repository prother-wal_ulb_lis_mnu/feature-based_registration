"""
In this module is defined an helper class for all the logging aspects of an experiment.
"""

from __future__ import annotations

import logging
import os
from typing import List, Optional

from utils.helpers_utils import generate_dir_if_missing


def get_username() -> str:
    username = os.popen("id -un").read().replace("\n", "")
    return username if username else "username"


class ExperimentLogger(logging.getLoggerClass()):
    """An helper class to handle the logging configuration."""

    @classmethod
    def get_logger(
        cls,
        name: str = get_username(),
        setup_logger: bool = True,
        to_handle: List[str] = [],
        clear: bool = True,
        output_dir: Optional[str] = None,
        level: int = logging.INFO,
        log_filename: str = "logs_general.log",
    ) -> logging.Logger | ExperimentLogger:
        """ExperimentLogger factory.

        :param name: The name of the logger, defaults to get_username().
        :type name: str, optional
        :param setup_logger: Whether the instantiated logger should be fully set-up or not, defaults to True.
        :type setup_logger: bool, optional
        :param to_handle: The list of handlers to add to the logger. This is limited for the
        moment to: (i) ``LOGS`` which will set a stdout handler and (ii) ``LOGFILE`` which is
        used to set a file handler. It defaults to [].
        :type to_handle: List[str], optional
        :param clear: Whether to clear the handlers or not, defaults to True.
        :type clear: bool, optional
        :param output_dir: The output directory where the logs should be saved, defaults to None.
        :type output_dir: Optional[str], optional
        :param level: The level of the logger, defaults to logging.INFO.
        :type level: int, optional
        :param log_filename: The name to give to the log file if needed, defaults to logs_general.log.
        :type log_filename: str, optional
        :return logger: The logger that correspond to the given name.
        :rtype logger: logging.Logger | ExperimentLogger
        """

        logger = logging.getLogger(name)

        if setup_logger:
            assert isinstance(logger, ExperimentLogger)
            logger.set_logger(
                to_handle=to_handle, clear=clear, output_dir=output_dir, level=level, log_filename=log_filename
            )

        return logger

    def set_logger(
        self,
        to_handle: List[str] = [],
        clear: bool = True,
        output_dir: Optional[str] = None,
        level: int = logging.INFO,
        log_filename: str = "logs_general.log",
    ) -> None:
        """Properly set-up the logger.

        :param to_handle: The list of handlers to add to the logger. This is limited for the
        moment to: (i) ``LOGS`` which will set a stdout handler and (ii) ``LOGFILE`` which is
        used to set a file handler. It defaults to [].
        :type to_handle: List[str], optional
        :param clear: Whether to clear the handlers or not, defaults to True.
        :type clear: bool, optional
        :param output_dir: The output directory where the logs should be saved, defaults to None.
        :type output_dir: Optional[str], optional
        :param level: The level of the logger, defaults to logging.INFO.
        :type level: int, optional
        :param log_filename: The name to give to the log file if needed, defaults to logs_general.log.
        :type log_filename: str, optional
        """

        self.setLevel(level)

        # Avoid propagation to higher level loggers (e.g. root in this case)
        self.propagate = False

        if clear:
            self.clear_handlers(self)

        if "LOGS" in to_handle:
            self._set_logger_handler(self._get_add_handler("stdout", logging.StreamHandler()))

        if "LOGFILE" in to_handle and output_dir:
            generate_dir_if_missing(os.path.join(output_dir, "logs"))
            filename = os.path.join(output_dir, "logs", log_filename)

            _handler = logging.FileHandler(filename)
            self._set_logger_handler(self._get_add_handler("file", _handler))

    @classmethod
    def clear_handlers(cls, logger: logging.Logger) -> None:
        """Clear the given logger's handlers.

        :param logger: The logger from which the handlers will be clear.
        :type logger: logging.Logger
        """

        logger.handlers.clear()

    def __get_logger_handler(self, handler_name: str) -> Optional[logging.Handler]:
        _handler = None

        if self.handlers:
            for h in self.handlers:
                if h.get_name() != handler_name:
                    continue

                _handler = h

        return _handler

    def __add_logger_handler(self, handler_name: str, _handler: logging.Handler) -> logging.Handler:
        _handler.set_name(handler_name)
        self.addHandler(_handler)

        return _handler

    def _get_add_handler(self, handler_name: str, new_handler: Optional[logging.Handler] = None) -> logging.Handler:
        """Get or add (if necessary) a given logging handler.

        :param handler_name: The name that should be given to the handler.
        :type handler_name: str
        :param new_handler: The given handler to add. If there already exists a handler with the given name, this
        argument is overlooked. Otherwise, create a new handler with this argument, defaults to None.
        :type new_handler: Optional[logging.Handler], optional
        :raises TypeError: Triggered when there is no existing handler with the specified handler_name and the
        new_handler's type is not logging.Handler
        :return: The handler named as specified, either the existing one or the new one.
        :rtype: logging.Handler
        """

        _handler = self.__get_logger_handler(handler_name)

        if not _handler:
            if not isinstance(new_handler, logging.Handler):
                raise TypeError(
                    f"No existing handler named: {handler_name} and new handler of wrong type has been "
                    f"provided {type(new_handler)=}."
                )

            _handler = self.__add_logger_handler(handler_name, new_handler)

        return _handler

    def _set_logger_handler(
        self,
        _handler: logging.Handler,
        level: int = logging.INFO,
        log_format: str = "[%(asctime)s] %(levelname)s: %(message)s",
    ) -> None:
        """Set an existing logger's handler to profile the execution of the experiment.

        :param level: The logging level which should be used, defaults to logging.INFO.
        :type level: int, optional
        :param log_format: The format of the logging messages, defaults to
        "[%(asctime)s] %(levelname)s: %(message)s".
        :type log_format: str, optional
        """

        self.setLevel(level)

        _handler.setLevel(level)
        _handler.setFormatter(logging.Formatter(log_format))


logging.setLoggerClass(ExperimentLogger)
logger = ExperimentLogger.get_logger(setup_logger=False)
