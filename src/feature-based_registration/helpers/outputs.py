"""
In this module is defined an helper class for managing all the outputs of an experiment.
"""

import json
import os
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Callable, Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
import pytz
from helpers.logger import logger
from psutil import Process
from skimage.io import imsave
from utils.errors import PathNotADirectoryError
from utils.helpers_utils import generate_dir_if_missing, serialize_iterable
import pickle

ALL_OUTPUTS = [
    "LOGS",
    "LOGFILE",
    "INIT",
    "PREPROC",
    "FEATURES",
    "MATCHED",
    "FILTERED",
    "UNSUPV_QUANT",
    "UNSUPV_QUAL",
    "SUPV_QUANT",
    "SUPV_QUAL",
    "OVERLAP",
    "REGPARAMS",
]


@dataclass
class OutputsHelper:
    """An helper class to handle the outputs generation.

    :param output_level_specs: A list of strings that specifies which outputs should be saved. Each possibility
    is associated to a specific word such as ``'INIT'`` to save the initial images. Different combinations
    of outputs can be achieved by providing each desired output word, for instance: ``['INIT', 'PREPROC']``
    will save the initial images and their pre-processed versions, defaults to 'ALL'. It is also possible
    to specify the undesired outputs rather than the desired ones by adding ``'NOT_'`` before the word, for
    example: ``'NOT_INIT'`` will save every possible outputs apart from the initial images. ``'NOT_ALL'``
    will be interpreted as no outputs desired. It defaults to [].
    All the possibilities are listed here:
    * ``'ALL'``: every possible outputs (when provided overrule anything else except
    when ``'NOT_'`` elements are also present)
    * ``'LOGS'``: log of all the processes via stdout
    * ``'LOGFILE'``: log of all the processes via a logfile
    * ``'INIT'``: initial images
    * ``'PREPROC'``: pre-processed images
    * ``'FEATURES'``: keypoints coordinates and descriptors
    * ``'MATCHED'``: matched keypoints coordinates
    * ``'FILTERED'``: filtered keypoints coordinates
    * ``'UNSUPV_QUANT'``: keypoints quantitative performance
    * ``'UNSUPV_QUAL'``: keypoints qualitative performance
    * ``'SUPV_QUANT'``: landmarks quantitative performance
    * ``'SUPV_QUAL'``: landmarks qualitative performance
    * ``'OVERLAP'``: overlapping of the initial and registered images
    * ``'REGPARAMS'``: registration results such as the transform and other parameters useful to align the pair

    :type output_level_specs: List[str], optional
    :param parent_dir: The location where the experiment directory should be generated. If ``None`` provided
    then the directory will not be generated, defaults to None.
    :type parent_dir: Optional[str], optional
    :param timezone: The timezone from which the date and time will be retrieved for the directory naming
    convention, defaults to "CET".
    :type timezone: str, optional
    """

    output_level_specs: List[str] = field(default_factory=list)
    parent_dir: Optional[str] = None
    basename: str = "Experiment"
    timezone: str = "CET"

    dir_path: Optional[str] = field(init=False, default=None)

    __output_level: Optional[List[str]] = field(init=False, default=None)

    def __post_init__(self) -> None:
        self.__set_experiment_dir(self.parent_dir, self.basename, self.timezone)
        self.__output_level = self.convert_output_level(self.output_level_specs)

    def __set_experiment_dir(
        self, parent_dir: Optional[str] = None, basename: str = "Experiment", timezone: str = "CET"
    ) -> None:
        """Generate the output directory that will contain the various outputs.

        :param parent_dir: The location where the output directory should be generated. If ``None`` provided
        then the directory will not be generated, defaults to None.
        :type parent_dir: Optional[str], optional
        :param basename: The basename for the outputs directory, defaults to "Experiment".
        :type basename: str, optional
        :param timezone: The timezone from which the date and time will be retrieved
        for the name of the directory, defaults to "CET".
        :type timezone: str, optional
        :raises PathNotADirectoryError: The given path is not a directory.
        """

        logger.info("Set experiment dir.")
        if not parent_dir:
            self.dir_path = None
            return

        generate_dir_if_missing(parent_dir)

        if not os.path.isdir(parent_dir):
            raise PathNotADirectoryError(path=parent_dir)

        tz = pytz.timezone(timezone)
        time = datetime.now(tz).strftime("%Y%m%d_%H%M%S")
        cpu_id = Process().cpu_num()
        dir_name = f"{basename}_{time}_{cpu_id:02}"

        self.dir_path = os.path.join(parent_dir, dir_name)
        generate_dir_if_missing(self.dir_path)

    @classmethod
    def convert_output_level(cls, output_level_specs: List[str] = ["ALL"]) -> List[str]:
        """Convert the provided output level to an explicit one.

        :param output_level_specs: A list of strings that specifies which outputs should be saved. Each possibility
        is associated to a specific word such as ``'INIT'`` to save the initial images. Different combinations
        of outputs can be achieved by providing each desired output word, for instance: ``['INIT', 'PREPROC']``
        will save the initial images and their pre-processed versions, defaults to 'ALL'. It is also possible
        to specify the undesired outputs rather than the desired ones by adding ``'NOT_'`` before the word, for
        example: ``'NOT_INIT'`` will save every possible outputs apart from the initial images. ``'NOT_ALL'``
        will be interpreted as no outputs desired.
        All the possibilities are listed here:
        * ``'ALL'``: every possible outputs (when provided overrule anything else except
        when ``'NOT_'`` elements are also present)
        * ``'LOGS'``: log of all the processes via stdout
        * ``'LOGFILE'``: log of all the processes via a logfile
        * ``'INIT'``: initial images
        * ``'PREPROC'``: pre-processed images
        * ``'FEATURES'``: keypoints coordinates and descriptors
        * ``'MATCHED'``: matched keypoints coordinates
        * ``'FILTERED'``: filtered keypoints coordinates
        * ``'UNSUPV_QUANT'``: keypoints quantitative performance
        * ``'UNSUPV_QUAL'``: keypoints qualitative performance
        * ``'SUPV_QUANT'``: landmarks quantitative performance
        * ``'SUPV_QUAL'``: landmarks qualitative performance
        * ``'OVERLAP'``: overlapping of the initial and registered images
        * ``'REGPARAMS'``: registration results such as the transform and other parameters useful to align the pair
        :type output_level_specs: List[str], optional
        :return: The explicit outputs to save.
        :rtype: List[str]
        """

        if not output_level_specs:
            logger.info(f"Due to provided value for {output_level_specs=}, it is set to [].")
            return []

        def _split_desired_undesired(given: List[str]) -> Tuple[List[str], ...]:
            desired, undesired = [], []
            for g in given:
                if "NOT_" in g:
                    undesired.append(g.replace("NOT_", ""))
                else:
                    desired.append(g)
            return desired, undesired

        outputs = ALL_OUTPUTS.copy()

        desired, undesired = _split_desired_undesired(output_level_specs)

        if "ALL" in undesired and "ALL" not in desired:
            return []
        elif "ALL" in undesired and "ALL" in desired:
            logger.info(
                "Inconsistancy, ALL and NOT_ALL have been specified. \n Resolved by taking only ALL into account."
            )
            return outputs
        elif ("ALL" in desired) or (not desired and undesired):
            for n in undesired:
                outputs.remove(n)
            return outputs
        elif desired and undesired:
            logger.info(
                "Inconsistancy, presence of both desired and undesired outputs. \n"
                "Resolved by taking only the desired ones into account."
            )
            return desired
        else:
            return desired

    def get_levels_for_log(self) -> List[str]:
        """Get levels associated to logging.

        :return: The list of levels that contains "LOG".
        :rtype: List[str]
        """

        if not self.__output_level:
            return []

        return [level for level in self.__output_level if "LOG" in level]

    def generate_outputs(
        self,
        level_flag: str,
        level_dir: str,
        prefix: str,
        suffix: str,
        save_method: Callable[..., None],
        save_method_kwargs: Dict[str, Any],
        outpath_key: str = "outpath",
    ) -> Any:
        """Generate the desired outputs in the output directory in the respective subdirectory using the given
        saving method and arguments.

        :param level_flag: The level flag to which these outputs are related to.
        :type level_flag: str
        :param level_dir: The subdirectory in which the outputs will be saved.
        :type level_dir: str
        :param prefix: The prefix to give to the outputs files.
        :type prefix: str
        :param suffix: The suffix to give to the outputs files.
        :type suffix: str
        :param save_method: The method that will save the outputs.
        :type save_method: Callable[..., None]
        :param save_method_kwargs: The arguments of the saving method.
        :type save_method_kwargs: Dict[str, Any]
        :param outpath_key: The key associated to the filename argument, defaults to "outpath".
        :type outpath_key: str, optional
        :return: The result of the saving method.
        :rtype: Any
        """
        
        assert self.__output_level is not None
        
        if level_flag not in self.__output_level:
            return

        experiment_dir = self.dir_path
        
        if experiment_dir:
            generate_dir_if_missing(os.path.join(experiment_dir, level_dir))

            outpath = os.path.join(experiment_dir, level_dir, f"{prefix}_{suffix}")
            save_method_kwargs[outpath_key] = outpath

            return save_method(**save_method_kwargs)


def save_config(outpath: str, config: Dict[str, Any]) -> None:
    """Save "config" as a JSON file.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param config: The "config" that will be saved.
    :type config: Dict[str, Any]
    """

    with open(outpath, "w", encoding="utf-8") as file:
        json.dump(serialize_iterable(config), file)


def save_img(outpath: str, img: np.ndarray) -> None:
    """Save an image.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param img: The image to save
    :type img: np.ndarray
    """

    imsave(outpath, img)


def save_array(outpath: str, array: np.ndarray) -> None:
    """Save an array.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param array: The array to save.
    :type array: np.ndarray
    """

    np.save(outpath, array)


def save_points(outpath: str, coords: np.ndarray, descriptors: Optional[np.ndarray] = None) -> None:
    """Save keypoints' coordinates and optionally their descriptors.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param coords: The coordinates to save.
    :type coords: np.ndarray
    :param descriptors: The descriptors to save, defaults to None.
    :type descriptors: Optional[np.ndarray], optional
    """

    data = coords
    cols = ["x", "y"]

    if descriptors is not None:
        data = np.hstack([coords, descriptors])
        cols += [f"desc_{i+1}" for i in range(descriptors.shape[1])]

    df = pd.DataFrame(data, columns=cols)
    df.to_csv(outpath)


def save_exec_time(outpath: str, times: Dict[str, float], unit: str = "s") -> None:
    """Save the execution times of the various steps.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param times: The execution times to save.
    :type times: Dict[str, float]
    :param unit: The unit of the given execution times, default s.
    :type unit: str, optional
    """

    table = {f"Execution times [{unit}]": np.nan}
    table.update(times)

    df = pd.DataFrame([table])
    df.to_csv(outpath, index=False)


def save_metrics(
    outpath: str, quant: Dict[str, Any], imgs_path: Dict[str, str], gts_path: Optional[Dict[str, str]] = None
) -> None:
    """Save the metrics.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param quant: The metrics to save.
    :type quant: Dict[str, Any]
    :param imgs_path: The path to both images, pattern: `"x_path": "path/to/x` with x either src or dst.
    :type imgs_path: Dict[str, str]
    :param gts_path: The path to both ground truth annotations, pattern: `"x__gt_path": "path/to/x_gt` with x
    either src or dst. This defaults to None.
    :type gts_path: Optional[Dict[str, str]], optional
    """

    def __add_suffix(fname: str, suffix: str) -> str:
        root, ext = os.path.splitext(fname)

        return root + "_" + suffix + ext

    nb_common = min(len(pts) for pts in [quant["dst"], quant["src"]])

    cols = ["dst_path", "src_path"]
    pts_pairs = [[imgs_path["dst_path"] for i in range(nb_common)], [imgs_path["src_path"] for i in range(nb_common)]]

    _globals = {"dst_path": imgs_path["dst_path"], "src_path": imgs_path["src_path"]}

    if gts_path:
        cols += list(gts_path.keys())
        pts_pairs += [
            [gts_path["dst_gt_path"] for i in range(nb_common)],
            [gts_path["src_gt_path"] for i in range(nb_common)],
        ]
        _globals.update(gts_path)

    cols += ["dst_coord_xy", "src_coord_xy", "warped_src_coord_xy"]
    pts_pairs += [
        quant["dst"][:nb_common, :],
        quant["src"][:nb_common, :],
        quant["warped"][:nb_common, :],
    ]

    for key, value in quant["metrics"]["local"].items():
        cols.append(key)
        pts_pairs.append(value)

        _mean = np.mean(value)
        _median = np.median(value)
        _lower_quartile = np.percentile(value, 25)
        _higher_quartile = np.percentile(value, 75)
        _std = np.std(value)

        _globals[f"{key}_mean"] = f"{_mean}"
        _globals[f"{key}_median"] = f"{_median}"
        _globals[f"{key}_lower_quartile"] = f"{_lower_quartile}"
        _globals[f"{key}_higher_quartile"] = f"{_higher_quartile}"
        _globals[f"{key}_std"] = f"{_std}"

    df = pd.DataFrame(list(zip(*pts_pairs)), columns=cols)
    df.to_csv(__add_suffix(outpath, "local"))

    _globals.update(quant["metrics"]["global"])
    df = pd.DataFrame([_globals])
    df.to_csv(__add_suffix(outpath, "global"))


def pickle_object(outpath: str, object_callable: Callable) -> None:
    """Save specific objects.

    :param outpath: The absolute path to the file that will be created.
    :type outpath: str
    :param object_callable: The object that should be saved.
    :type object_callable: Callable
    """

    with open(outpath, "wb") as fp:
        pickle.dump(object_callable, fp)