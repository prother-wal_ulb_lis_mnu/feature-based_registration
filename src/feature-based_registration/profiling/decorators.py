"""
In this module are defined the decorators used to profile the callable from this project.
"""

import functools
from time import perf_counter
from typing import Any, Callable, Concatenate, Optional, ParamSpec, TypeVar

from helpers.logger import logger

T = TypeVar("T")
P = ParamSpec("P")

def _get_correct_method_name(initial_name: str, given_object: Any) -> str:
    if not hasattr(given_object, "method"):
        return initial_name
    
    method_name = initial_name.split(".")[-1]
    callable_name = (
        given_object.method.__name__
        if "function" in str(type(given_object.method))
        else given_object.method.__class__.__name__
    )
        
    return initial_name.replace(f".{method_name}", f"({callable_name}).{method_name}")


def benchmark_method(func: Callable[Concatenate[Any, P], T]) -> Callable[Concatenate[Any, P], T]:
    """Decorator that benchmarks the execution time of a given method.

    :param func: The method to benchmark.
    :type func: Callable[Concatenate[Any, P], T]
    :return: The wrapper which will actually benchmark the method and return its outputs.
    :rtype: Callable[Concatenate[Any, P], T]
    """

    @functools.wraps(func)
    def wrapper(self: Any, *args: P.args, **kwargs: P.kwargs) -> T:
        # If the class is a wrapper around the `real` method defined under the `func.method` attribute
        # then update the message to explicit which method it is.
        message = _get_correct_method_name(func.__qualname__, self)

        start_time = perf_counter()
        value = func(self, *args, **kwargs)
        end_time = perf_counter()
        run_time = end_time - start_time

        logger.info(f"{message}: execution time = {run_time:.2f} seconds.")

        # To update `func.exec_time`
        if hasattr(self, "exec_time"):
            self.exec_time = run_time

        return value
    
    return wrapper


def benchmark_function(func: Callable[P, T]) -> Callable[P, T]:
    """Decorator that benchmarks the execution time of a given function.

    :param func: The function to benchmark.
    :type func: Callable[P, T]
    :return: The wrapper which will actually benchmark the function and return its outputs.
    :rtype: Callable[P, T]
    """

    @functools.wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
        message = func.__qualname__

        start_time = perf_counter()
        value = func(*args, **kwargs)
        end_time = perf_counter()
        run_time = end_time - start_time

        logger.info(f"{message}: execution time = {run_time:.2f} seconds.")

        return value
    
    return wrapper


def method_with_logging(heading: Optional[str] = None) -> Callable[[Callable[Concatenate[Any, P], T]], Callable[Concatenate[Any, P], T]]:
    """Closure that allows to pass a additional information to the decorator.

    :param heading: Add a heading to the loggings, defaults to None.
    :type heading: Optional[str], optional
    """

    def decorator(func: Callable[Concatenate[Any, P], T]) -> Callable[Concatenate[Any, P], T]:
        """Decorator that add logging information to a given method.

        :param func: The method to log.
        :type func: Callable[Concatenate[Any, P], T]
        :return: The wrapper which will actually log the method.
        :rtype: Callable[Concatenate[Any, P], T]
        """

        @functools.wraps(func)
        def wrapper(self, *args: P.args, **kwargs: P.kwargs) -> T:
            if heading:
                logger.info(heading)

            # If the class is a wrapper around the `real` method defined under the `func.method` attribute
            # then update the message to explicit which method it is.
            message = _get_correct_method_name(func.__qualname__, self)

            logger.info(f"Calling {message}")
            value = func(self, *args, **kwargs)
            logger.info(f"Finished {message}")

            return value
        
        return wrapper
    
    return decorator

def func_with_logging(heading: Optional[str] = None) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """Closure that allows to pass a additional information to the decorator.

    :param heading: Add a heading to the loggings, defaults to None.
    :type heading: Optional[str], optional
    """

    def decorator(func: Callable[P, T]) -> Callable[P, T]:
        """Decorator that add logging information to a given function.

        :param func: The function to log.
        :type func: Callable[P, T]
        :return: The wrapper which will actually log the function.
        :rtype: Callable[P, T]
        """

        @functools.wraps(func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            message = func.__qualname__

            if heading:
                logger.info(heading)

            logger.info(f"Calling {message}")
            value = func(*args, **kwargs)
            logger.info(f"Finished {message}")

            return value

        return wrapper

    return decorator
