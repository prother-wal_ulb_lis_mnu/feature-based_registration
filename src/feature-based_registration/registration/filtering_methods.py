"""
In this module are implemented various robust filtering methods.
"""

import copy
from functools import partial
from typing import Any, Callable, Dict, Optional, Tuple, Type

import cv2
import numpy as np
import pydegensac
import pyvfc
from cv2.xfeatures2d import matchGMS
from helpers.logger import logger
from profiling.decorators import benchmark_method, method_with_logging
from skimage.measure import ransac
from skimage.transform import (
    AffineTransform,
    EuclideanTransform,
    ProjectiveTransform,
    SimilarityTransform,
)
from utils.general_utils import available
from utils.profiling_utils import get_heading

Filter = Callable[..., Tuple[ProjectiveTransform, np.ndarray]]


AVAILABLE_FILTERS = {}


# For available OpenCV RANSACs refer to
# https://stackoverflow.com/questions/72534497/how-to-use-usac-parameters-in-opencv-for-fundamental-matrix
RANSAC_FAMILY = {
    "affRANSAC": partial(cv2.estimateAffinePartial2D, method=cv2.RANSAC),
    "RANSAC": partial(cv2.findHomography, method=cv2.RANSAC),
    "PROSAC": partial(cv2.findHomography, method=cv2.RHO),
    "DEGENSAC": pydegensac.findHomography,  # Non-deterministic method and no possibility to set the random seed
    "USAC_DEFAULT": partial(cv2.findHomography, method=cv2.USAC_DEFAULT),
    "USAC_FAST": partial(cv2.findHomography, method=cv2.USAC_FAST),
    "USAC_ACCURATE": partial(cv2.findHomography, method=cv2.USAC_ACCURATE),
    "USAC_MAGSAC": partial(cv2.findHomography, method=cv2.USAC_MAGSAC),
}


@available(AVAILABLE_FILTERS)
class RANSACFamily:
    """Class that implements the filtering part of the feature matching process with use of
    an algorithm from the RANSAC family.
    """

    def __init__(self, method: Callable[..., Tuple[Any, np.ndarray]], **method_kwargs: Any) -> None:
        """RANSACFamily constructor.

        :param method: The actual ransac-like algorithm that will be used to filter the correspondences.
        :type method: Callable[..., Tuple[Any, np.ndarray]]

        Each method accepts specific kwargs that can be also provided. In particular with skRansac method, one
        should pass the required arguments from skimage.measure.ransac other than ``data``.
        """

        self.exec_time: Optional[float] = None

        self.__filter = method
        self.__filter_kwargs = method_kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__filter=}, {self.__filter_kwargs=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(
        self, src: np.ndarray, dst: np.ndarray, transform: ProjectiveTransform, **kwargs: Any
    ) -> Tuple[ProjectiveTransform, np.ndarray]:
        """Filter the matched keypoints according to a specific transform model.

        :param src: The source keypoints.
        :type src: np.ndarray
        :param dst: The destination keypoints.
        :type dst: np.ndarray
        :param transform: The model transform that is used to determine the inliers from the
        outliers. It should be derived from the scikit-image ProjectiveTransform.
        :type transform: ProjectiveTransform
        :return robust_transform: Best transform with largest consensus set.
        :rtype robust_transform: ProjectiveTransform
        :return good_indices: Boolean mask of inliers classified as ``True``.
        :rtype good_indices: np.ndarray
        """

        def _reshape_mask(mask: np.ndarray) -> np.ndarray:
            if len(mask.shape) == 1:
                return mask
            else:
                indices = np.where(mask.reshape(-1) == 1)[0]
                good_indices = np.zeros((src.shape[0],), bool)
                good_indices[indices] = True
                return good_indices

        _, mask = self.__filter(src, dst, **self.__filter_kwargs)

        good_indices = _reshape_mask(mask)

        robust_transform = copy.deepcopy(transform)
        # In a degenerate case the .estimate method will return False. In those situations, an identity transform is returned.
        if not robust_transform.estimate(src[good_indices, :], dst[good_indices, :]):
            robust_transform = transform

        return robust_transform, good_indices


@available(RANSAC_FAMILY, newname="skRANSAC")
def sk_ransac(
    src: np.ndarray,
    dst: np.ndarray,
    transform: Type[ProjectiveTransform] = EuclideanTransform,
    min_samples: int = 10,
    residual_threshold: float = 7.0,
    **kwargs,
) -> Tuple[ProjectiveTransform, np.ndarray]:
    """Function that implements the filtering part of the feature matching process with use of
    the RANSAC algorithm from scikit-image.

    :param src: The source keypoints.
    :type src: np.ndarray
    :param dst: The destination keypoints.
    :type dst: np.ndarray
    :param transform: The model transform that is used to determine the inliers from the
    outliers, defaults to EuclideanTransform.
    :type transform: ProjectiveTransform
    :param min_samples: The minimum number of data points to fit a model to, defaults to 10.
    :type min_samples: int
    :param residual_threshold: Maximum distance for a data point to be classified as an inlier,
    defaults to 7.0.
    :type residual_threshold: float
    :return robust_transform: Best transform with largest consensus set.
    :rtype robust_transform: ProjectiveTransform
    :return good_indices: Boolean mask of inliers classified as ``True``.
    :rtype good_indices: np.ndarray
    """

    return ransac(
        data=(src, dst),
        model_class=transform,
        min_samples=min_samples,
        residual_threshold=residual_threshold,
        **kwargs,
    )


@available(AVAILABLE_FILTERS)
class LMedS:
    """Class that implements the filtering part of the feature matching process with use of
    the Least-Median of squares (LMedS) algorithm from OpenCV.
    """

    def __init__(self, **kwargs: Any) -> None:
        """LMedS constructor.

        Each method accepts specific kwargs that can be also provided. In particular with skRansac method, on
        should pass the required arguments from skimage.measure.ransac other than ``data``.
        """

        self.exec_time: Optional[float] = None

        self.__filter = partial(cv2.findHomography, method=cv2.LMEDS)
        self.__filter_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__filter=}, {self.__filter_kwargs=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(
        self, src: np.ndarray, dst: np.ndarray, transform: ProjectiveTransform, **kwargs: Any
    ) -> Tuple[ProjectiveTransform, np.ndarray]:
        """Filter the matched keypoints according to a specific transform model.

        :param src: The source keypoints.
        :type src: np.ndarray
        :param dst: The destination keypoints.
        :type dst: np.ndarray
        :param transform: The model transform that is used to determine the inliers from the
        outliers. It should be derived from the scikit-image ProjectiveTransform.
        :type transform: ProjectiveTransform
        :return robust_transform: Best transform with largest consensus set.
        :rtype robust_transform: ProjectiveTransform
        :return good_indices: Boolean mask of inliers classified as ``True``.
        :rtype good_indices: np.ndarray
        """

        def _reshape_mask(mask: np.ndarray) -> np.ndarray:
            indices = np.where(mask.reshape(-1) == 1)[0]
            good_indices = np.zeros((src.shape[0],), bool)
            good_indices[indices] = True
            return good_indices

        _, mask = self.__filter(src, dst, **self.__filter_kwargs)

        good_indices = _reshape_mask(mask)

        robust_transform = transform
        robust_transform.estimate(src[good_indices, :], dst[good_indices, :])

        return robust_transform, good_indices


@available(AVAILABLE_FILTERS)
class VFC:
    """Class that implements the filtering part of the feature matching process with use of
    the VFC algorithm.
    """

    def __init__(self, **kwargs: Any) -> None:
        """VFC constructor.

        :raises KeyError: When an unavailable method is provided.

        The implementation is based on https://github.com/aelskens/pyvfc which is an adaptation of
        the original code from https://github.com/jiayi-ma/VFC. Therefore, the class constructor
        accepts the attributes of the latter C++ VFC class as kwargs.

        Allowed kwargs are:
        :kwarg method: The method for outlier removal. The options are ``normal``, ``fast`` or
        ``sparse``, defaults to ``sparse``.
        :type method: int, optional
        :kwarg maxIter: The maximum number of iterations, defaults to 500.
        :type maxIter: int, optional
        :kwarg gamma: Percentage of inliers in the samples. This is an initial value for EM iteration
        and is not important, defaults to 0.9.
        :type gamma: float, optional
        :kwarg beta: Parameter of Gaussian Kernel, k(x, y) = exp(-beta* || x - y || ^ 2), defaults
        to 0.1.
        :type beta: float, optional
        :kwarg lmbda: Represents the trade-off between the goodness of data fit and smoothness of the
        field, defaults to 3.0.
        :type lmbda: float, optional
        :kwarg theta: The threshold value that determines whether the sample is an inliner or not. An
        inliner has a posterior probability which is superior to theta, defaults to 0.75.
        :type theta: float, optional
        :kwarg a: Parameter of the uniform distribution. It is that the outliers obey a uniform
        distribution 1/a, defaults to 10.0.
        :type a: float, optional
        :kwarg ecr: The minimum limitation of the energy change rate in the iteration process, defaults
        to 1e-5.
        :type ecr: float, optional
        :kwarg minP: The posterior probability Matrix P may be singular for matrix inversion. The minimum
        value of P is set as minP, defaults to 1e-5.
        :type minP: float, optional
        """

        self.exec_time: Optional[float] = None

        self.__filter_kwargs = kwargs.copy()

        method = kwargs.get("method", None)

        if method:
            METHODS = {"normal": 1, "fast": 2, "sparse": 3}

            if m := METHODS.get(method, None):
                kwargs["method"] = m
            else:
                raise KeyError(
                    f"{method} is not one of the existing options for the VFC outlier removal algorithm. "
                    f"The available options are the following {','.join(METHODS.keys())}."
                )

        self.__filter = pyvfc.VFC(**kwargs)
        logger.debug(f"Method: {self.__filter.getMethod()}")
        logger.debug(f"MaxIter: {self.__filter.getMaxIter()}")
        logger.debug(f"Gamma: {self.__filter.getGamma()}")
        logger.debug(f"Beta: {self.__filter.getBeta()}")
        logger.debug(f"Lambda: {self.__filter.getLambda()}")
        logger.debug(f"Theta: {self.__filter.getTheta()}")
        logger.debug(f"A: {self.__filter.getA()}")
        logger.debug(f"Ecr: {self.__filter.getEcr()}")
        logger.debug(f"MinP: {self.__filter.getMinP()}")

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__filter=}, {self.__filter_kwargs=})"

    def get_filter_params(self) -> Dict[str, Any]:
        """Get the parameters of the VFC C++ class.

        :return: The parameters dictionary.
        :rtype: Dict[Any]
        """

        return {
            "method": self.__filter.getMethod(),
            "maxIter": self.__filter.getMaxIter(),
            "gamma": self.__filter.getGamma(),
            "beta": self.__filter.getBeta(),
            "lmbda": self.__filter.getLambda(),
            "theta": self.__filter.getTheta(),
            "a": self.__filter.getA(),
            "ecr": self.__filter.getEcr(),
            "minP": self.__filter.getMinP(),
        }

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(
        self, src: np.ndarray, dst: np.ndarray, transform: ProjectiveTransform, **kwargs: Any
    ) -> Tuple[ProjectiveTransform, np.ndarray]:
        """Filter the matched keypoints according to a specific transform model.

        :param src: The source keypoints.
        :type src: np.ndarray
        :param dst: The destination keypoints.
        :type dst: np.ndarray
        :param transform: The model transform that is used to determine the inliers from the
        outliers. It should be derived from the scikit-image ProjectiveTransform.
        :type transform: ProjectiveTransform
        :return robust_transform: Best transform with largest consensus set.
        :rtype robust_transform: ProjectiveTransform
        :return good_indices: Boolean mask of inliers classified as ``True``.
        :rtype good_indices: np.ndarray
        """

        if not self.__filter.setData(src, dst):
            raise TypeError(
                "The input data was not successfully set, this may be caused by the format of src and/or dst which "
                "should be collections of points defined by their x and y coordinates (in this specific order!)."
            )

        logger.debug("Starting the optimization process.")
        self.__filter.optimize()
        logger.debug("Optimization process accomplished.")

        indices = self.__filter.obtainCorrectMatch()
        if not indices:
            params = [f"{key} = {value:.2e}" for key, value in self.get_filter_params().items()]

            message = "No matching keypoints between the two sets with the current parameters: "
            message += ", ".join(params)

            raise ValueError(message)

        good_indices = np.zeros((src.shape[0],), bool)
        good_indices[indices] = True

        robust_transform = transform
        robust_transform.estimate(src[good_indices, :], dst[good_indices, :])

        return robust_transform, good_indices


@available(AVAILABLE_FILTERS)
class GMS:
    """Class that implements the filtering part of the feature matching process with use of
    the GMS (Grid-based Motion Statistics) algorithm [1].

    References:
    [1] JiaWang Bian, Wen-Yan Lin, Yasuyuki Matsushita, Sai-Kit Yeung,
    Tan Dat Nguyen, and Ming-Ming Cheng. Gms: Grid-based motion statistics for
    fast, ultra-robust feature correspondence. In IEEE Conference on Computer
    Vision and Pattern Recognition, 2017
    """

    def __init__(self, **kwargs: Any) -> None:
        """GMS constructor.

        The implementation is based on cv2.xfeatures2d.matchGMS. Therefore, the class constructor
        accepts the parameters of the latter callable as kwargs.
        """

        self.exec_time: Optional[float] = None

        self.__filter = matchGMS

        # Infer GMS boolean arguments based on the nature of the given transform if either EuclideanTransform
        # SimilarityTransform or AffineTransform.
        filter_kwargs = {
            key: kwargs.get(key, default)
            for key, default in zip(("withRotation", "withScale", "thresholdFactor"), (True, False, 6.0))
        }

        self.__filter_kwargs = filter_kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__filter=}, {self.__filter_kwargs=})"

    def __update_kwargs(self, transform: ProjectiveTransform) -> None:
        if transform == EuclideanTransform:
            self.__filter_kwargs["withRotation"] = True
            self.__filter_kwargs["withScale"] = False
        elif transform == SimilarityTransform or transform == AffineTransform:
            self.__filter_kwargs["withRotation"] = True
            self.__filter_kwargs["withScale"] = True

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(
        self,
        src: np.ndarray,
        dst: np.ndarray,
        transform: ProjectiveTransform,
        distances: np.ndarray,
        src_img_shape: np.ndarray,
        dst_img_shape: np.ndarray,
        **kwargs: Any,
    ) -> Tuple[ProjectiveTransform, np.ndarray]:
        """Filter the matched keypoints according to a specific transform model. The process takes also the distances
        between the match into account in the computations.

        :param src: The source keypoints.
        :type src: np.ndarray
        :param dst: The destination keypoints.
        :type dst: np.ndarray
        :param transform: The model transform  that is used to determine the inliers from the
        outliers. It should be derived from the scikit-image ProjectiveTransform.
        :type transform: ProjectiveTransform
        :param distances: The distances in the latent space between the matches.
        :type distances: np.ndarray
        :param src_img_shape: The shape of the source image.
        :type src_img_shape: np.ndarray
        :param dst_img_shape: The shape of the destination image.
        :type dst_img_shape: np.ndarray
        :return robust_transform: Best transform with largest consensus set.
        :rtype robust_transform: ProjectiveTransform
        :return good_indices: Boolean mask of inliers classified as ``True``.
        :rtype good_indices: np.ndarray
        """

        self.__update_kwargs(transform)

        cv_src = cv2.KeyPoint_convert(src.tolist())
        cv_dst = cv2.KeyPoint_convert(dst.tolist())
        matches = [cv2.DMatch(_queryIdx=i, _trainIdx=i, _imgIdx=0, _distance=distances[i]) for i in range(len(src))]
        gms_matches = self.__filter(
            src_img_shape.tolist(), dst_img_shape.tolist(), cv_src, cv_dst, matches, **self.__filter_kwargs
        )
        if not gms_matches:
            raise ValueError(
                "No correspondences have been found. If the matching method pre-filters already "
                "the matches based on mutual proximity, then remove this pre-filter and try again."
            )

        indices = np.array([d.queryIdx for d in gms_matches])

        good_indices = np.zeros((src.shape[0],), bool)
        good_indices[indices] = True

        robust_transform = transform
        robust_transform.estimate(src[good_indices, :], dst[good_indices, :])

        return robust_transform, good_indices
