"""
In this module are implemented various features detectors.
"""

import copyreg
import inspect
from typing import Any, Callable, Optional, Tuple, Union

import cv2
import numpy as np
import torch
from cv2.xfeatures2d import SURF, StarDetector
from profiling.decorators import benchmark_method, method_with_logging
from skimage import feature
from utils.general_utils import available
from utils.profiling_utils import get_heading
from utils.registration_utils import MANUAL_PICKLE

ArrayLike = Union[np.ndarray, Tuple[cv2.KeyPoint, ...], torch.Tensor]
FeatureDetector = Callable[[np.ndarray], ArrayLike]


AVAILABLE_DETECTORS = {
    "SIFT": cv2.SIFT.create,
    "SURF": SURF.create,
    "BRISK": cv2.BRISK.create,
    "ORB": cv2.ORB.create,
    "FAST": cv2.FastFeatureDetector.create,
    "KAZE": cv2.KAZE.create,
    "AKAZE": cv2.AKAZE.create,
    "STAR": StarDetector.create,  # derived from CenSurE
}


def unpack_SIFT_octave(keypoint: cv2.KeyPoint) -> Tuple[int, int, float]:
    """Compute octave, layer, and scale from a SIFT keypoint.

    Adapted from: https://github.com/rmislam/PythonSIFT/blob/master/pysift.py#L324.

    :param keypoint: The keypoint from which the octave will be unpacked.
    :type keypoint: cv2.KeyPoint
    :return octave: The actual keypoint's octave.
    :rtype: int
    :return layer: The layer of that octave.
    :rtype: int
    :return scale: The scale.
    :rtype: float
    """

    octave = keypoint.octave & 255
    layer = (keypoint.octave >> 8) & 255

    if octave >= 128:
        octave = -128 | octave

    scale = 1 / float(1 << octave) if octave >= 0 else float(1 << -octave)

    return octave, layer, scale


class Detector:
    """Class to implement a features detector callable."""

    def __init__(self, method: Callable[..., Any], **kwargs: Any) -> None:
        """Constructor of Detector class.

        :param method: The method that will be used to detect the keypoints.
        :type method: Callable[..., Any]

        Additionally, kwargs specific to the method to use can be provided.
        """

        self.exec_time: Optional[float] = None

        self.method = method(**kwargs)
        self.method_kwargs = kwargs

        if method_cls := MANUAL_PICKLE.get(str(method)):

            def _pickle(obj):
                return method_cls, tuple(
                    getattr(obj, name)() for name, _ in inspect.getmembers(method_cls()) if name.startswith("get")
                )

            copyreg.pickle(method_cls().__class__, _pickle)

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.method=}, {self.method_kwargs=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(self, img: np.ndarray) -> np.ndarray:
        """Detect the image keypoints.

        :param img: The image from which the keypoints are derived.
        :type img: np.ndarray
        :return keypoints: The detected keypoints.
        :rtype: np.ndarray
        """

        keypoints = self.method.detect(img)
        return keypoints


@available(AVAILABLE_DETECTORS)
class skCENSURE:
    """A CENSURE feature detector from scikit image.

    This scikit-image feature detecotr can be used as an OpenCV feature detector.
    """

    def __init__(self, **kwargs):
        """Constructor of skCENSURE class.

        Allowed args are similar to the ones from skimage.feature.CENSURE.
        """

        self.__detector = feature.CENSURE(**kwargs)

    def detect(self, img: np.ndarray):
        """Detect keypoints in image using CENSURE.

        See https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.CENSURE.
        Uses keypoint info to create KeyPoint objects for OpenCV.

        :param img: Image from which keypoints will be detected.
        :type img: np.ndarray
        :return kp: List of OpenCV KeyPoint objects
        :rtype kp:
        """

        self.__detector.detect(img)

        # Transform (row, col) keypoints to (x, y) keypoints
        kp_xy = self.__detector.keypoints[:, ::-1].astype(float)
        kp = cv2.KeyPoint_convert(kp_xy.tolist())

        return kp
