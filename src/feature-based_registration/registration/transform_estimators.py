from skimage.transform import (
    AffineTransform,
    EuclideanTransform,
    PiecewiseAffineTransform,
    PolynomialTransform,
    SimilarityTransform,
)

AVAILABLE_TRANSFORMS = {
    "skEuclidean": EuclideanTransform,
    "skSimilarity": SimilarityTransform,
    "skAffine": AffineTransform,
    "skPiecewiseAffine": PiecewiseAffineTransform,
    "skPolynomial": PolynomialTransform,
}
