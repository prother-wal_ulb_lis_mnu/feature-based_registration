"""
In this module are implemented various matching methods.
"""

import copyreg
from typing import Any, Callable, Optional, Tuple

import cv2
import numpy as np
from profiling.decorators import benchmark_method, method_with_logging
from sklearn.metrics import pairwise_distances
from utils.general_utils import available
from utils.profiling_utils import get_heading
from utils.registration_utils import MANUAL_PICKLE

Matcher = Callable[[np.ndarray, np.ndarray], Tuple[np.ndarray, np.ndarray]]


AVAILABLE_MATCHERS = {}


@available(AVAILABLE_MATCHERS)
class PairwiseDistances:
    """Class that implements the matching of two sets of descriptors based on a distance
    metric.
    """

    def __init__(self, **kwargs: Any) -> None:
        """PairwiseDistance constructor.

        The implementation is based on sklearn pairwise_distance function. Therefore, the
        constructor of this class accepts the different parameters of the later function
        as kwargs.

        The following kwargs are prohibited: ``X``, ``Y``, ``src`` and ``dst``, those are
        removed if present.
        """

        self.exec_time: Optional[float] = None

        self.__matcher = pairwise_distances

        for k in kwargs.copy():
            if k not in ["X", "Y", "src", "dst"]:
                continue

            del kwargs[k]

        self.__matcher_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__matcher=}, {self.__matcher_kwargs=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(self, src: np.ndarray, dst: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """Retrieve the indices of the matched keypoints based on their descriptors.

        :param src: Source descriptors.
        :type src: np.ndarray
        :param dst: Destination descriptors.
        :type dst: np.ndarray
        :return matched_indices: The indices of the source (first column) and destination (second column)
        matched keypoints.
        :rtype matched_indices: np.ndarray
        :return distances: The distances between the pairs of matched keypoints.
        :rtype distances: np.ndarray
        """

        distances = self.__matcher(src, dst, **self.__matcher_kwargs)

        matched_indices = np.empty((src.shape[0], 2)).astype(int)
        matched_indices[..., 0] = np.arange(src.shape[0])
        matched_indices[..., 1] = np.argmin(distances, axis=1)

        return matched_indices, distances[matched_indices[..., 0], matched_indices[..., 1]]


@available(AVAILABLE_MATCHERS)
class BruteForce:
    """Class that implements a brute force matching method of two sets of descriptors based
    on a distance metric.
    """

    def __init__(self, distance: str = "norm_l2", cross_check: bool = False) -> None:
        """BruteForce constructor.

        :param distance: The distance to use for the matching part, defaults to ``norm_l2``.
        :type distance: str, optional
        :param cross_check: Define whether or not a cross check to remove the single direction
        matches (the i-th descriptor in set A has the j-th descriptor in set B as the best match
        while the inverse is not true) should used, defaults to ``False``.
        :type cross_check: bool, optional
        """

        self.exec_time: Optional[float] = None

        CV_DISTANCES = {
            "norm_l2": cv2.NORM_L2,
            "norm_l1": cv2.NORM_L1,
            "norm_hamming": cv2.NORM_HAMMING,
            "norm_hamming2": cv2.NORM_HAMMING2,
        }

        d = CV_DISTANCES.get(distance, None)
        if not d:
            raise KeyError(
                f"{distance} is not one of the existing options OpenCV BFMatcher. The available "
                f"options are the following {','.join(CV_DISTANCES.keys())}."
            )

        self.__matcher = cv2.BFMatcher(d, crossCheck=cross_check)
        self.__matcher_kwargs = {"distance": distance, "cross_check": cross_check}

        if method_cls := MANUAL_PICKLE.get(str(cv2.BFMatcher)):

            def _pickle():
                return method_cls, (d, cross_check)

            copyreg.pickle(method_cls().__class__, _pickle)

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__matcher=}, {self.__matcher_kwargs=})"

    @method_with_logging(heading=get_heading(__name__))
    @benchmark_method
    def __call__(self, src: np.ndarray, dst: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """Retrieve the indices of the matched keypoints based on their descriptors.

        :param src: Source descriptors.
        :type src: np.ndarray
        :param dst: Destination descriptors.
        :type dst: np.ndarray
        :return matched_indices: The indices of the source (first column) and destination (second column)
        matched keypoints.
        :rtype matched_indices: np.ndarray
        :return distances: The distances between the pairs of matched keypoints.
        :rtype distances: np.ndarray
        """

        matches = self.__matcher.match(src, dst)

        indices = np.array([[match.queryIdx, match.trainIdx, match.distance] for match in matches])

        matched_indices = indices[..., :2].astype(int)
        distances = indices[:, 2]

        return matched_indices, distances
