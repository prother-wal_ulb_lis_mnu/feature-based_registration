"""
This module defines the method used to achieve the feature extraction step of the feature-based registration
pipeline (combination of detection and description).
"""

from dataclasses import dataclass
from typing import Dict, Protocol, Tuple

import cv2
import numpy as np
from registration.descriptors import (
    AVAILABLE_DESCRIPTORS,
    Descriptor,
    FeatureDescriptor,
    RootSIFT,
)
from registration.detectors import (
    AVAILABLE_DETECTORS,
    Detector,
    FeatureDetector,
    unpack_SIFT_octave,
)

# Maximum number of image features that will be recorded. If the number
# of features exceeds this value, the MAX_FEATURES features with the
# highest response will be returned.
MAX_FEATURES = 20000


class Features(Protocol):
    img: np.ndarray
    detector: FeatureDetector
    descriptor: FeatureDescriptor
    keypoints: np.ndarray
    kp_descriptors: np.ndarray


@dataclass
class ModularDetectionDescription:
    """A generic class that links the detection and description steps from a feature matching pipeline.

    :param detector: The method used to detect the keypoints. Any callable that detects and
    returns the found keypoints based on an image, defaults to BRISK.
    :type detector: FeatureDetector
    :param descriptor: The descriptor technique used to extract the feature vector for each keypoints.
    Any callable that implements a descriptor algorithm based on a given image and its detected keypoints
    and which returns the keypoints' features vectors, defaults to BRIEF.
    :type descriptor: FeatureDescriptor
    """

    detector: FeatureDetector = Detector(method=AVAILABLE_DETECTORS["BRISK"])
    descriptor: FeatureDescriptor = Descriptor(method=AVAILABLE_DESCRIPTORS["BRIEF"])

    def __call__(self, img: np.ndarray) -> Tuple[Tuple[cv2.KeyPoint, ...], np.ndarray]:
        """Detect and describe the keypoints derived from the given image.

        :param img: The image from which the keypoints are detected and described.
        :type img: np.ndarray
        :return keypoints: The detected keypoints.
        :rtype keypoints: Tuple[cv2.KeyPoint, ...]
        :return kp_descriptors: The descriptors vectors.
        :rtype kp_descriptors: np.ndarray
        """

        tmp_keypoints = self.detector(img)
        
        assert isinstance(tmp_keypoints, tuple)
        tmp_keypoints = self.remove_excessive_kp(tmp_keypoints)

        if isinstance(self.detector.method, cv2.SIFT) and (
            not isinstance(self.descriptor.method, cv2.SIFT) and not isinstance(self.descriptor.method, RootSIFT)
        ):
            for k in tmp_keypoints:
                octave, _, _ = unpack_SIFT_octave(k)
                k.octave = octave

        keypoints, kp_descriptors = self.descriptor(img, tmp_keypoints)

        return keypoints, kp_descriptors

    def remove_excessive_kp(
        self, keypoints: Tuple[cv2.KeyPoint, ...], nb_max: int = MAX_FEATURES
    ) -> Tuple[cv2.KeyPoint, ...]:
        """Remove excessive keypoint.

        :param keypoints: The detected keypoints.
        :type keypoints: Tuple[cv2.KeyPoint, ...]
        :param nb_max: The maximal number of keypoints that are taken into account, defaults to MAX_FEATURES
        :type nb_max: int, optional
        :return: The nb_max best keypoints.
        :rtype: Tuple[cv2.KeyPoint, ...]
        """

        response = np.array([x.response for x in keypoints])
        keep_idx = np.argsort(response)[::-1][:nb_max]

        return tuple(keypoints[i] for i in keep_idx)

    @property
    def exec_times(self) -> Dict[str, float]:
        return {
            method_name: method.exec_time
            for method_name, method in zip(("detector", "descriptor"), (self.detector, self.descriptor))
        }
