from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, Optional, Tuple, Union

import cv2
import numpy as np
import torch
from processing.image import ProcessedImage
from registration.descriptors import (
    AVAILABLE_DESCRIPTORS,
    Descriptor,
    FeatureDescriptor,
)
from registration.detectors import AVAILABLE_DETECTORS, Detector, FeatureDetector
from registration.features_extraction import ModularDetectionDescription
from registration.filtering_methods import (
    AVAILABLE_FILTERS,
    RANSAC_FAMILY,
    Filter,
    RANSACFamily,
)
from registration.matching_methods import AVAILABLE_MATCHERS, Matcher
from registration.transform_estimators import AVAILABLE_TRANSFORMS
from skimage.transform import ProjectiveTransform
from utils.general_utils import available

from configs.config import ExperimentConfig

ArrayLike = Union[np.ndarray, Tuple[cv2.KeyPoint, ...], torch.Tensor]


AVAILABLE_PIPELINES = {}


class ModularFeatureMatching(ABC):
    """Abstract feature matching pipeline class."""

    def __init__(self) -> None:
        """ModularFeatureMatching's constructor."""

        self._labels: Optional[Dict[str, str]] = None
        self._src_keypoints: Optional[ArrayLike] = None
        self._dst_keypoints: Optional[ArrayLike] = None
        self._src_descriptors: Optional[ArrayLike] = None
        self._dst_descriptors: Optional[ArrayLike] = None
        self._matched_indices: Optional[np.ndarray] = None
        self._filtered_indices: Optional[np.ndarray] = None
        self._robust_estimation: Optional[ProjectiveTransform] = None
        self._exec_times: Dict[str, float] = {}

    @abstractmethod
    def __call__(self, src_img: ProcessedImage, dst_img: ProcessedImage, **kwargs: Any) -> None:
        pass

    @abstractmethod
    def __repr__(self) -> str:
        pass

    @abstractmethod
    def _to_numpy(self, arraylike: ArrayLike) -> np.ndarray:
        pass

    @classmethod
    @abstractmethod
    def from_config(cls, cfg: ExperimentConfig) -> ModularFeatureMatching:
        pass

    @property
    def labels(self) -> Optional[Dict[str, str]]:
        return self._labels

    @labels.setter
    def labels(self, values: Dict[str, str]) -> None:
        self._labels = values

    @property
    def src_keypoints_xy(self) -> np.ndarray:
        assert self._src_keypoints is not None

        return self._to_numpy(self._src_keypoints)

    @property
    def dst_keypoints_xy(self) -> np.ndarray:
        assert self._dst_keypoints is not None

        return self._to_numpy(self._dst_keypoints)

    @property
    def src_keypoints_desc(self) -> np.ndarray:
        assert self._src_descriptors is not None

        return self._to_numpy(self._src_descriptors)

    @property
    def dst_keypoints_desc(self) -> np.ndarray:
        assert self._dst_descriptors is not None

        return self._to_numpy(self._dst_descriptors)

    @property
    def matched_src_keypoints_xy(self) -> np.ndarray:
        assert self._matched_indices is not None

        return self.src_keypoints_xy[self._matched_indices[..., 0]]

    @property
    def matched_dst_keypoints_xy(self) -> np.ndarray:
        assert self._matched_indices is not None

        return self.dst_keypoints_xy[self._matched_indices[..., 1]]

    @property
    def matched_src_keypoints_desc(self) -> np.ndarray:
        assert self._matched_indices is not None

        return self.src_keypoints_desc[self._matched_indices[..., 0]]

    @property
    def matched_dst_keypoints_desc(self) -> np.ndarray:
        assert self._matched_indices is not None

        return self.dst_keypoints_desc[self._matched_indices[..., 1]]

    @property
    def matched_indices(self) -> np.ndarray:
        assert self._matched_indices is not None

        return self._matched_indices

    @property
    def filtered_matched_src_keypoints_xy(self) -> np.ndarray:
        return self.matched_src_keypoints_xy[self._filtered_indices]

    @property
    def filtered_matched_dst_keypoints_xy(self) -> np.ndarray:
        return self.matched_dst_keypoints_xy[self._filtered_indices]

    @property
    def filtered_matched_src_keypoints_desc(self) -> np.ndarray:
        return self.matched_src_keypoints_desc[self._filtered_indices]

    @property
    def filtered_matched_dst_keypoints_desc(self) -> np.ndarray:
        return self.matched_dst_keypoints_desc[self._filtered_indices]

    @property
    def filtered_indices(self) -> np.ndarray:
        assert self._filtered_indices is not None

        return self._filtered_indices

    @property
    def robust_transform(self) -> ProjectiveTransform:
        assert self._robust_estimation is not None

        return self._robust_estimation

    @property
    def exec_times(self) -> Dict[str, float]:
        return self._exec_times


@available(AVAILABLE_PIPELINES)
class Traditional(ModularFeatureMatching):
    """Traditional feature matching pipeline with a detector, a descriptor, a matcher and a filter."""

    def __init__(
        self,
        detector: FeatureDetector,
        descriptor: FeatureDescriptor,
        matcher: Matcher,
        filter_method: Filter,
        transform: ProjectiveTransform,
    ) -> None:
        """Traditional's constructor.

        :param detector: The method used to detect the keypoints. Any callable that detects and
        returns the found keypoints based on an image.
        :type detector: FeatureDetector
        :param descriptor: The descriptor technique used to extract the feature vector for each keypoints.
        Any callable that implements a descriptor algorithm based on a given image and its detected keypoints
        and which returns the keypoints' features vectors.
        :type descriptor: FeatureDescriptor
        :param matcher: The method that will be used to match the set of keypoints.
        :type matcher: Matcher
        :param filter_method: The method that will be used to filter out the robust correspondences from the
        outliers.
        :type filter_method: Filter
        :param transform: The method that will be used to estimate the transformation parameters.
        :type transform: ProjectiveTransform
        """

        self.detector = detector
        self.descriptor = descriptor
        self.matcher = matcher
        self.filter = filter_method
        self.transform = transform
        super().__init__()

    def __repr__(self) -> str:
        return (
            f"{__name__}.{self.__class__.__name__}(\n    {self.detector=},\n    {self.descriptor=},\n    "
            f"{self.matcher=},\n    {self.filter=},\n    {self.transform=}\n)"
        )

    def __call__(self, src_img: ProcessedImage, dst_img: ProcessedImage) -> None:
        """Perform the feature matching on the input images.

        Note: Utilize the version of the images without padding to perform keypoint detection and description.
        Adjust the detected keypoints based on the padding value and subsequently employ the "padded" version
        of the images for the filtering stage. The objective is to ensure that the detection and description
        processes remain independent of the specific padding value applied.

        :param src_img: The "source" image object.
        :type src_img: ProcessedImage
        :param dst_img: The "destination" image object.
        :type dst_img: ProcessedImage
        """

        # Detect and Describe
        detect_and_describe = ModularDetectionDescription(detector=self.detector, descriptor=self.descriptor)

        assert src_img.preprocessed is not None
        self._src_keypoints, self._src_descriptors = detect_and_describe(src_img.preprocessed)
        self._exec_times.update(
            {f"src_{method_name}": exec_time for method_name, exec_time in detect_and_describe.exec_times.items()}
        )
        assert dst_img.preprocessed is not None
        self._dst_keypoints, self._dst_descriptors = detect_and_describe(dst_img.preprocessed)
        self._exec_times.update(
            {f"dst_{method_name}": exec_time for method_name, exec_time in detect_and_describe.exec_times.items()}
        )

        # Match
        self._matched_indices, distances = self.matcher(self.src_keypoints_desc, self.dst_keypoints_desc)
        self._exec_times.update({"matcher": self.matcher.exec_time})

        # Filter
        self._robust_estimation, self._filtered_indices = self.filter(
            self.matched_src_keypoints_xy,
            self.matched_dst_keypoints_xy,
            self.transform,
            distances=distances,
            src_img_shape=src_img.preprocessed.shape,
            dst_img_shape=dst_img.preprocessed.shape,
        )
        self._exec_times.update({"filter": self.filter.exec_time})

    def _to_numpy(self, arraylike: ArrayLike) -> np.ndarray:
        if isinstance(arraylike, torch.Tensor):
            arraylike = np.asarray(arraylike.numpy())
        elif isinstance(arraylike, tuple):
            arraylike = np.array([elem.pt for elem in arraylike])

        return arraylike

    @classmethod
    def from_config(cls, cfg: ExperimentConfig) -> Traditional:
        """Factory method to instantiate an object based on an ExperimentConfig.

        :param config: The configuration object that contains all the experiment specifications.
        :type config: ExperimentConfig
        :return: The pipeline instantiated with the given configuration.
        :rtype: Traditional
        """

        detector_label, detector, detector_kwargs = ExperimentConfig.extract_label_method_N_kwargs(cfg.detector)
        descriptor_label, descriptor, descriptor_kwargs = ExperimentConfig.extract_label_method_N_kwargs(
            cfg.descriptor
        )
        matcher_label, matcher, matcher_kwargs = ExperimentConfig.extract_label_method_N_kwargs(cfg.matcher)
        filter_label, filter_method, filter_kwargs = ExperimentConfig.extract_label_method_N_kwargs(cfg.filter)
        transform_label, transform, transform_kwargs = ExperimentConfig.extract_label_method_N_kwargs(cfg.transform)

        # Extract the matching distance specified at the descriptor level
        matcher_kwargs.update({"distance": cfg.descriptor[descriptor_label]["matching_distance"]})

        obj = Traditional(
            detector=Detector(AVAILABLE_DETECTORS[detector], **detector_kwargs),
            descriptor=Descriptor(AVAILABLE_DESCRIPTORS[descriptor], **descriptor_kwargs),
            matcher=AVAILABLE_MATCHERS[matcher](**matcher_kwargs),
            filter_method=(
                AVAILABLE_FILTERS[filter_method](**filter_kwargs)
                if filter_method not in RANSAC_FAMILY
                else RANSACFamily(RANSAC_FAMILY[filter_method], **filter_kwargs)
            ),
            transform=AVAILABLE_TRANSFORMS[transform](**transform_kwargs),
        )

        obj.labels = {
            "detector": detector_label,
            "descriptor": descriptor_label,
            "matcher": matcher_label,
            "filter": filter_label,
            "transform": transform_label,
        }

        return obj


dummy = Traditional(
    detector=Detector(AVAILABLE_DETECTORS["FAST"]),
    descriptor=Descriptor(AVAILABLE_DESCRIPTORS["BRIEF"]),
    matcher=AVAILABLE_MATCHERS["BruteForce"],
    filter_method=RANSACFamily(RANSAC_FAMILY["affRANSAC"]),
    transform=AVAILABLE_TRANSFORMS["skAffine"],
)
