# Issues:
# - Spacial resolution in z axis = 4 microns -> not particularly the same as x and y axis

from __future__ import annotations

import os
import pickle
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Callable, Dict, List, Optional, Sequence, Tuple

import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt
from openwholeslide import IntVector, PaddingParameters, WholeSlide
from processing.channel_reduction import grayscale
from processing.transform import Warping2D
from skimage import img_as_ubyte
from skimage.exposure import rescale_intensity
from skimage.io import imsave
from skimage.transform import EuclideanTransform, ProjectiveTransform

from benchmark.benchmark_utils import get_all_experiments_list, get_pair_name


@dataclass
class Slide:
    ws: WholeSlide
    number: int
    thickness: float
    resolution: Optional[float] = field(default=None)
    transform: Warping2D = field(default=Warping2D(EuclideanTransform(), resolution=10))
    is_reference: bool = False

    _padder: Optional[PaddingParameters] = field(init=False, repr=False, default=None)
    _img: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    _padded_img: Optional[np.ndarray] = field(init=False, repr=False, default=None)

    @property
    def mpp(self) -> float:
        if not self.resolution:
            return self.thickness

        return self.resolution

    def _load_img(self, ws: WholeSlide) -> np.ndarray:
        return img_as_ubyte(ws.read_full(resolution=self.mpp).as_ndarray)

    @property
    def img(self) -> np.ndarray:
        if self._img is None:
            self._img = self._load_img(self.ws)

        return self._img

    def get_dim_at_resolution(self) -> IntVector:
        return IntVector.from_yx((self.img.shape[0], self.img.shape[1]))

    @property
    def padder(self) -> Optional[PaddingParameters]:
        return self._padder

    @property
    def offset(self) -> Optional[IntVector]:
        if self.padder is None:
            return None

        return self.padder.pad_before

    @padder.setter
    def padder(self, p) -> None:
        self._padder = p

    def get_padder(self, padding: IntVector, pad_value: float) -> PaddingParameters:
        if self.img.dtype == np.uint8:
            pad_value = (
                max(np.iinfo(self.img.dtype).max, int(pad_value))
                if int(pad_value) != np.iinfo(self.img.dtype).min
                else 0
            )

        pad_dimensions = padding - IntVector.from_yx((self.img.shape[0], self.img.shape[1]))
        pad_before = IntVector(x=pad_dimensions.x // 2, y=pad_dimensions.y // 2)

        padder = PaddingParameters(
            pad_before=pad_before,
            pad_after=IntVector(x=pad_dimensions.x - pad_before.x, y=pad_dimensions.y - pad_before.y),
            pad_value=pad_value,
        )

        return padder

    def _pad(self, img: np.ndarray) -> np.ndarray:
        assert self.padder is not None

        # Adapt the `pad_value` to the image's encoding type
        pad_value = self.padder.pad_value
        if img.dtype == np.uint8:
            pad_value = (
                max(np.iinfo(img.dtype).max, int(self.padder.pad_value))
                if int(self.padder.pad_value) != np.iinfo(img.dtype).min
                else 0
            )

        return np.pad(img, self.padder.pad_width[: len(img.shape)], constant_values=pad_value)

    @property
    def padded_img(self) -> np.ndarray:
        if self._padded_img is None:
            self._padded_img = self._pad(self.img)

        return self._padded_img

    def align_to_ref(
        self,
        img: np.ndarray,
        output_shape: np.ndarray,
        tf: Optional[Warping2D] = None,
    ) -> np.ndarray:
        if tf:
            return tf(img, cval=1, output_shape=output_shape, target_resolution=self.mpp)
        return self.transform(img, cval=1, output_shape=output_shape, target_resolution=self.mpp)


@dataclass
class SlideStack:
    slides: Sequence[Slide]

    @classmethod
    def from_experiments(
        cls,
        pairs_list_path: str,
        experiments_path: str,
        transform_cls: Callable,
        thickness: float,
        resolution: Optional[float] = None,
    ) -> SlideStack:
        def _get_pairs_list(path: str) -> List[str]:
            with open(path, "r", encoding="utf-8") as fp:
                pairs_list = fp.read().splitlines()

            return pairs_list

        def _get_id(filename: str) -> int:
            return int(filename.replace(".ndpi", "")[-3:])

        def _get_wsi(pairs_list_path: str) -> Tuple[Dict[int, WholeSlide], int]:
            path = os.path.dirname(pairs_list_path)

            pairs_list = _get_pairs_list(pairs_list_path)
            reference_index = (len(pairs_list) + 1) // 2
            reference_id = 0

            wsis = {}
            for i, pair in enumerate(pairs_list):
                # Handle reference
                if i == reference_index:
                    file = pair.split(",")[-1][1:]
                    _id = _get_id(file)
                    reference_id = _id
                    wsis[_id] = WholeSlide(os.path.join(path, file))

                file = pair.split(",")[0]
                _id = _get_id(file)

                wsis[_id] = WholeSlide(os.path.join(path, file))

            return wsis, reference_id

        def _pickle_load(path: str) -> Callable:
            with open(path, "rb") as fp:
                out = pickle.load(fp)

            return out

        def _get_transforms(experiments_path: str, transform_cls: Callable) -> Dict[int, ProjectiveTransform]:
            exps = get_all_experiments_list(experiments_path)

            transforms = {}
            for exp in exps:
                _id = _get_id(get_pair_name(exp, "|").split("|", maxsplit=1)[0])
                warp2D = _pickle_load(os.path.join(exp, "regparams", "raw_warp2D.pickle"))
                transforms[_id] = transform_cls(matrix=warp2D.transform.params)

            return transforms

        wsis, reference_id = _get_wsi(pairs_list_path)
        tfs = _get_transforms(experiments_path, EuclideanTransform)

        tmp = []
        compose = transform_cls()
        for _id, wsi in wsis.items():
            reference = False
            if _id == reference_id:
                reference = True
                compose = transform_cls()
            else:
                compose = transform_cls(matrix=compose.params @ tfs[_id].params)

            tmp.append(
                Slide(
                    wsi,
                    _id,
                    thickness,
                    resolution=resolution,
                    transform=Warping2D(compose, resolution=10),
                    is_reference=reference,
                )
            )

        return SlideStack(tmp)

    def set_up(self) -> None:
        max_dims = IntVector(x=0, y=0)
        for slide in self.slides:
            slide_dims = slide.get_dim_at_resolution()
            max_dims.x = max(max_dims.x, slide_dims.x)
            max_dims.y = max(max_dims.y, slide_dims.y)

        # Add a 10th of the max_dims to purposely overpad the images
        max_dims.x += max_dims.x // 10
        max_dims.y += max_dims.y // 10

        for slide in self.slides:
            slide.padder = slide.get_padder(max_dims, 1.0)

    @property
    def ordered_slides(self) -> Sequence[Slide]:
        return sorted(self.slides, key=lambda x: x.number)

    def get_reference_slide(self) -> Slide:
        for slide in self.slides:
            if slide.is_reference:
                return slide

        raise ValueError("There is no reference slide.")

    def get_volume(self, warped: bool = True, n_dims: int = 2) -> np.ndarray:
        def _dummy(img: np.ndarray) -> np.ndarray:
            return img

        dims = self.ordered_slides[0].padded_img.shape[:n_dims]
        volume = np.empty((len(self.ordered_slides), *dims))
        preproc = grayscale if n_dims == 2 else _dummy
        for i, slide in enumerate(self.ordered_slides):
            if warped:
                tf = deepcopy(slide.transform)
                tf.transform.params = (
                    EuclideanTransform(translation=self.get_reference_slide().offset.xy).params
                    @ slide.transform.transform.params
                )
                volume[i, ...] = img_as_ubyte(
                    slide.align_to_ref(preproc(slide.img), np.array(preproc(slide.padded_img).shape), tf=tf)
                )
            else:
                volume[i, ...] = preproc(slide.padded_img)

        return volume.astype(np.uint8)

    @classmethod
    def generate_volume_slices(cls, volume: np.ndarray, outdir: str, name: str, point=None) -> None:
        if point is None:
            point = np.array([volume.shape[0] // 2, volume.shape[1] // 2, volume.shape[2] // 2])

        cor_img = volume[point[0]]
        sag_img = volume[:, point[1], :]
        trans_img = volume[:, :, point[2]]

        imsave(os.path.join(outdir, f"trans_slice_{name}.png"), trans_img)
        imsave(os.path.join(outdir, f"cor_slice_{name}.png"), cor_img)
        imsave(os.path.join(outdir, f"sag_slice_{name}.png"), sag_img)

    @classmethod
    def generate_volume_projections(cls, volume: np.ndarray, outdir: str, name: str) -> None:

        cor_img = img_as_ubyte(rescale_intensity(volume.sum(axis=0), out_range=(0, 1)))
        sag_img = img_as_ubyte(rescale_intensity(volume.sum(axis=1), out_range=(0, 1)))
        trans_img = img_as_ubyte(rescale_intensity(volume.sum(axis=2), out_range=(0, 1)))

        imsave(os.path.join(outdir, f"trans_proj_{name}.png"), trans_img)
        imsave(os.path.join(outdir, f"cor_proj_{name}.png"), cor_img)
        imsave(os.path.join(outdir, f"sag_proj_{name}.png"), sag_img)

    @classmethod
    def generate_gif_from_volume(
        cls,
        volume: np.ndarray,
        outdir: str,
        name: str,
        slide_numbers: Optional[Sequence[int]] = None,
        interval: int = 400,
    ) -> None:
        # Create new figure for GIF
        fig, ax = plt.subplots()

        # Adjust figure so GIF does not have extra whitespace
        fig.subplots_adjust(left=0, bottom=0, right=1, top=1)
        ax.axis("off")
        ims = []

        for layer in range(volume.shape[0]):
            if len(volume[layer, ...].shape) > 2:
                im = ax.imshow(volume[layer, ...], animated=True)
            else:
                im = ax.imshow(volume[layer, ...], cmap="gray", animated=True)

            if slide_numbers:
                slide_number = ax.annotate(
                    str(slide_numbers[layer]),
                    xy=(1, 0),
                    xycoords="axes fraction",
                    fontsize=16,
                    xytext=(-5, 5),
                    textcoords="offset points",
                    ha="right",
                    va="bottom",
                )
                ims.append([im, slide_number])
            else:
                ims.append(im)

        ani = animation.ArtistAnimation(fig, ims, interval=interval)
        ani.save(os.path.join(outdir, f"{name}.gif"))


if __name__ == "__main__":
    import os

    from skimage.transform import EuclideanTransform

    path = "/data/cmmi/PTL01/Round_C1/3_1C1G1D"
    slides_stack = SlideStack.from_experiments(
        pairs_list_path=os.path.join(path, "dev_set.txt"),
        experiments_path="/results/feature_based/liver/experiments",
        transform_cls=EuclideanTransform,
        thickness=4.0,
        resolution=15.0,
    )

    slides_stack.set_up()

    # Aligned 3D volume
    volume = slides_stack.get_volume()

    outdir = "/results/feature_based/liver/experiments_analysis/3D_stack"
    slides_stack.generate_volume_slices(volume, outdir, "liver_aligned")
    slides_stack.generate_volume_projections(volume, outdir, "liver_aligned")
    slides_stack.generate_gif_from_volume(
        volume, outdir, "liver_aligned", slide_numbers=[s.number for s in slides_stack.ordered_slides]
    )
